@extends('layouts.masterblade')
@section('body')
	<div class="col-xs-12">
		<div class="row">
			<div class="col-xs-10">
				<h1>Figuur</h1>
			</div>
			<div class="col-xs-2">
				<a id="menu-toggle" href="#" class="btn btn-default"><i class="fa fa-bars"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<h3>Voeg een figuur toe</h3>
			</div>
			<div class="col-xs-12 blur-back">
				{{ Form::open(['route' => 'admin.image.store', 'files' => true, 'class' => 'form-horizontal']), PHP_EOL }}
					@if(Session::has('auth-error-message'))
					            <div class="alert-box radius" data-alert>
					                {{ Session::get('auth-error-message') }}
					            </div>
					@endif
					<div class="form-group">
						{{ Form::label('word', 'Woord', [
							'class' => 'col-sm-3 control-label'
							] ), PHP_EOL }}
						<div class="col-sm-9">
							{{ Form::text('word', '', [
					        'placeholder' 	=> 'kabouter',
					        'class' 		=> 'form-control'
					        ]), PHP_EOL }}
						    @if ($errors->has('word'))
					        	{{ $errors->first('word', '<small class="error">:message</small>') }}
					        @endif
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('phonetic', 'Fonetisch', [
							'class' => 'col-sm-3 control-label'
							] ), PHP_EOL }}
						<div class="col-sm-9">
							{{ Form::text('phonetic', '', [
					        'placeholder' 	=> 'kɑbautər',
					        'class' 		=> 'form-control phonetic mousetrap phonetics-only',
					        'autocomplete' 	=> 'off',
                            'disabled' => 'disabled'
					        ]), PHP_EOL }}
						    @if ($errors->has('phonetic'))
					        	{{ $errors->first('phonetic', '<small class="error">:message</small>') }}
					        @endif
                            <input type="hidden" id="phonetic-value" name="phonetic-value">
						</div>
					</div>

					<div class="form-group">
						{{ Form::label('url', 'Figuur', [
							'class' => 'col-sm-3 control-label'
							] ), PHP_EOL }}
						<div class="col-sm-9">
							{{ Form::file('url',[
								'id' => 'url'
							])}}
							<output id="imagepreview"></output>
							@if ($errors->has('url'))
					        	{{ $errors->first('url', '<small class="error">:message</small>') }}
					        @endif
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('wordurl', 'Woord', [
							'class' => 'col-sm-3 control-label'
							] ), PHP_EOL }}
						<div class="col-sm-9">
							{{ Form::file('wordurl',[
								'id' => 'wordurl'
							])}}
							<output id="wordpreview"></output>
							@if ($errors->has('wordurl'))
					        	{{ $errors->first('wordurl', '<small class="error">:message</small>') }}
					        @endif
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('sentenceurl', 'Zin', [
							'class' => 'col-sm-3 control-label'
							] ), PHP_EOL }}
						<div class="col-sm-9">
							{{ Form::file('sentenceurl',[
								'id' => 'sentenceurl'
							])}}
							<output id="sentencepreview"></output>
							@if ($errors->has('sentenceurl'))
					        	{{ $errors->first('sentenceurl', '<small class="error">:message</small>') }}
					        @endif
						</div>
					</div>


					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
					    {{ Form::submit('Toevoegen', [
					    	'class'	=> 'btn btn-default'
					    	]) , PHP_EOL}}
				    	</div>
					</div>

				{{ Form::close() }}
			</div>
		</div>
	</div>
@section('scripts')
	@parent
    {{ HTML::script("/components/jquery.caret.js") }}
    {{ HTML::script("/components/mousetrap.min.js") }}
	{{ HTML::script("/scripts/phonetic/PhoneticDropDown.js") }}
	<script>
        $(document).ready(function(){
            var phoneticDropDown = new PhoneticDropDown('../../');
        });
	</script>


	<script>
	function handleAudioWordSelect(evt) {
		document.getElementById('wordpreview').innerHTML = '';
	    var files = evt.target.files; // FileList object

	    // Loop through the FileList and render image files as thumbnails.
	    for (var i = 0, f; f = files[i]; i++) {

	      // Only process image files.
	      if (!f.type.match('audio.*')) {
	        continue;
	      }

	      var reader = new FileReader();

	      // Closure to capture the file information.
	      reader.onload = (function(theFile) {
	        return function(e) {
	          // Render thumbnail.
	          var span = document.createElement('span');
	          span.innerHTML = [
	          '<audio controls><source src="', e.target.result,'" type="audio/mpeg">Your browser does not support this audio format.</audio>'].join('');
	          document.getElementById('wordpreview').insertBefore(span, null);
	        };
	      })(f);

	      // Read in the image file as a data URL.
	      reader.readAsDataURL(f);
	    }
	  }

	  function handleAudioSentenceSelect(evt) {
	  	document.getElementById('sentencepreview').innerHTML = '';
	    var files = evt.target.files; // FileList object

	    // Loop through the FileList and render image files as thumbnails.
	    for (var i = 0, f; f = files[i]; i++) {

	      // Only process image files.
	      if (!f.type.match('audio.*')) {
	        continue;
	      }

	      var reader = new FileReader();

	      // Closure to capture the file information.
	      reader.onload = (function(theFile) {
	        return function(e) {
	          // Render thumbnail.
	          var span = document.createElement('span');
	          span.innerHTML = [
	          '<audio controls><source src="', e.target.result,'" type="audio/mpeg">Your browser does not support this audio format.</audio>'].join('');
	          document.getElementById('sentencepreview').insertBefore(span, null);
	        };
	      })(f);

	      // Read in the image file as a data URL.
	      reader.readAsDataURL(f);
	    }
	  }

	  function handleImageSelect(evt) {
	  	document.getElementById('imagepreview').innerHTML = '';
	    var files = evt.target.files; // FileList object

	    // Loop through the FileList and render image files as thumbnails.
	    for (var i = 0, f; f = files[i]; i++) {

	      // Only process image files.
	      if (!f.type.match('image.*')) {
	        continue;
	      }

	      var reader = new FileReader();

	      // Closure to capture the file information.
	      reader.onload = (function(theFile) {
	        return function(e) {
	          // Render thumbnail.
	          var span = document.createElement('span');
	          span.innerHTML = ['<img class="thumb" src="', e.target.result,
	                            '" title="', escape(theFile.name), '"/>'].join('');
	          document.getElementById('imagepreview').insertBefore(span, null);
	        };
	      })(f);

	      // Read in the image file as a data URL.
	      reader.readAsDataURL(f);
	    }
	  }

	  document.getElementById('url').addEventListener('change', handleImageSelect, false);
	  document.getElementById('wordurl').addEventListener('change', handleAudioWordSelect, false);
	  document.getElementById('sentenceurl').addEventListener('change', handleAudioSentenceSelect, false);
	</script>

@stop
@stop