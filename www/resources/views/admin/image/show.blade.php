@extends('layouts.masterblade')
@section('body')
	<div class="col-xs-12">
		<div class="row">
			<div class="col-xs-10">
				<h1>Figuur</h1>
			</div>
			<div class="col-xs-2">
				<a id="menu-toggle" href="#" class="btn btn-default"><i class="fa fa-bars"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<h3>{{ $image->word }}</h3>
			</div>
			<div class="col-xs-12 blur-back">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						{{ HTML::image('uploads/images/' . $image->url, $image->word, [
										'class' => 'image-show'
									])}}
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="row">
							<div class="col-xs-3 showlabel">Woord</div>
							<div class="col-xs-9 showvalue">{{ $image->word }}</div>
						</div>
						<div class="row">
							<div class="col-xs-3 showlabel">Fonetisch</div>
							<div class="col-xs-9 showvalue">{{ $phoneticString }}</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="row">
									<div class="col-xs-3 showlabel">Woord</div>
								</div>
								<div class="row">
									<div class="col-xs-12 showvalue">
										<audio controls>
										  <source src="{{url('uploads/words/'.$image->wordurl)}}" type="audio/mpeg">
										  Your browser does not support this audio format.
										</audio>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="row">
									<div class="col-xs-3 showlabel">Zin</div>
								</div>
								<div class="row">
									<div class="col-xs-12 showvalue">
										<audio controls>
										  <source src="{{url('uploads/sentences/'.$image->sentenceurl)}}" type="audio/mpeg">
										  Your browser does not support this audio format.
										</audio>
									</div>
								</div>
							</div>
						</div>
						<div class="row">

							{{ HTML::link('/admin/image', 'Terug naar de lijst', ['class' => 'btn btn-default col-xs-offset-3 btn-m']), PHP_EOL }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop