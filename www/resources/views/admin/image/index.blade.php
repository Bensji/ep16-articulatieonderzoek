@extends('layouts.masterblade')
@section('body')
	<div class="col-xs-12">
		<div class="row">
			<div class="col-xs-10">
				<h1>Figuren</h1>
			</div>
			<div class="col-xs-2">
				<a id="menu-toggle" href="#" class="btn btn-default"><i class="fa fa-bars"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				{{ HTML::link('admin/image/create', 'Maak een figuur aan', ['class' => 'btn btn-default btn-margin-bottom']), PHP_EOL }}
				{{ Form::open(['route' => 'admin.image.index', 'class' => 'form-horizontal', 'method' => 'get']), PHP_EOL }}
					{{ Form::text('search', '', [
					        'placeholder' 	=> 'Zoek figuur',
					        'class' 		=> 'form-control btn-margin-bottom',
					        'data-provide'	=> 'typeahead',
					        'data-source'	=> json_encode($all),
					        'data-items'	=> 5,
					        'autocomplete'	=> 'off'
					        ]), PHP_EOL }}
				{{ Form::close() }}
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 blur-back">
				<div class="table-responsive">
					<table class="table table-striped">
						<tr>
							<th>Figuur</th>
							<th>Woord</th>
							<th>Status</th>
							<th></th>
						</tr>
						@foreach ($images as $image)
							<tr>
								<td>
									{{ HTML::image('uploads/images/' . $image->url, $image->word, [
										'class' => 'image-thumb'
									])}}
								</td>
								<td>{{ $image->word }}</td>
								<td>
									@if ($image->deleted_at != null)
										Deleted
									@endif
								</td>
								<td>
									<a href="{{ route('admin.image.show',$image->id) }}">
										<i class="fa fa-info"></i>
									<a href="{{ route('admin.image.edit',$image->id) }}">
										<i class="fa fa-edit"></i>
									</a>
									@if ($image->deleted_at == null)
										{{ Form::open([
										'method' => 'delete', 
										'route' => ['admin.image.destroy', $image->id], 
										'class' =>'table-form' ]),
										 PHP_EOL }}
											<button type="submit" class='form-button'><i class="fa fa-trash-o"></i></button>
										{{ Form::close() }}
									@else
										{{ Form::open([
										'method' => 'put', 
										'route' => ['admin.image.restore', $image->id], 
										'class' =>'table-form' ]),
										 PHP_EOL }}
											<button type="submit" class='form-button'><i class="fa fa-undo"></i></button>
										{{ Form::close() }}
									@endif
								</td>
							</tr>
						@endforeach
						@if($images->isEmpty() )
							<tr><td colspan="3">Er zijn nog geen figuren toegevoegd</td></tr>
						@endif	
					</table>
				</div>
				<div class="pagination-wrapper">
					{{$images->appends(['search' => $search ])->links();}}
				</div>
			</div>
		</div>
	</div>
@stop