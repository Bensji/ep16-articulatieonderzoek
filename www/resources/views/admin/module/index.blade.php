@extends('layouts.master')
@section('content')
	<div class="col-xs-12">
		<div class="row">
			<div class="col-xs-10">
				<h1>Modules</h1>
			</div>
			<div class="col-xs-2">
				<a id="menu-toggle" href="#" class="btn btn-default"><i class="fa fa-bars"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				{{ HTML::link('admin/module/create', 'Voeg een module toe', ['class' => 'btn btn-default btn-margin-bottom']), PHP_EOL }}
				{{ Form::open(['route' => 'admin.module.index', 'class' => 'form-horizontal', 'method' => 'get']), PHP_EOL }}
					{{ Form::text('search', '', [
					        'placeholder' 	=> 'Zoeken op naam',
					        'class' 		=> 'form-control btn-margin-bottom',
					        'data-provide'	=> 'typeahead',
					        'data-source'	=> json_encode($all),
					        'data-items'	=> 5,
					        'autocomplete'	=> 'off'
					        ]), PHP_EOL }}
				{{ Form::close() }}
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 blur-back">
				<div class="table-responsive">
					<table class="table table-striped">
						<tr>
							<th>Naam</th>
							<th>Omschrijving</th>
							<th>Type</th>
							<th>Status</th>
							<th></th>
						</tr>

						@foreach ($modules as $module)
							<tr>
								<td>{{ $module->name }}</td>
								<td>{{ $module->description }}</td>
								<td>{{ $module->moduletype->name }}</td>
								<td>
									@if ($module->deleted_at != null)
										Deleted
									@endif
								</td>
								<td>
									<a href="{{ route('admin.module.show',$module->id) }}">
										<i class="fa fa-info"></i>
									<a href="{{ route('admin.module.edit',$module->id) }}">
										<i class="fa fa-edit"></i>
									</a>
									@if ($module->deleted_at == null)
										{{ Form::open([
										'method' => 'delete', 
										'route' => ['admin.module.destroy', $module->id], 
										'class' =>'table-form' ]),
										 PHP_EOL }}
											<button type="submit" class='form-button'><i class="fa fa-trash-o"></i></button>
										{{ Form::close() }}
									@else
										{{ Form::open([
										'method' => 'put', 
										'route' => ['admin.module.restore', $module->id], 
										'class' =>'table-form' ]),
										 PHP_EOL }}
											<button type="submit" class='form-button'><i class="fa fa-undo"></i></button>
										{{ Form::close() }}
									@endif
								</td>
							</tr>
						@endforeach
						@if($modules->isEmpty() )
							<tr><td colspan="3">Er zijn nog geen modules toegevoegd</td></tr>
						@endif
					</table>
				</div>

				<div class="pagination-wrapper">
					{{$modules->appends(['search' => $search ])->links();}}
				</div>

			</div>
		</div>
	</div>
@section('scripts')
	@parent
	{{ HTML::script("/scripts/bootstrap3-typeahead.js") }}
@stop
@stop