@extends('layouts.master')
@section('content')
	<div class="col-xs-12">
		<div class="row">
			<div class="col-xs-10">
				<h1>Module</h1>
			</div>
			<div class="col-xs-2">
				<a id="menu-toggle" href="#" class="btn btn-default"><i class="fa fa-bars"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<h3>{{ $module->name }}</h3>
			</div>
			<div class="col-xs-12 blur-back">
				<div class="row">
					<div class="col-xs-3 showlabel">Naam</div>
					<div class="col-xs-9 showvalue">{{ $module->name }}</div>
				</div>
				<div class="row">
					<div class="col-xs-3 showlabel">Omschrijving</div>
					<div class="col-xs-9 showvalue">{{ $module->description }}</div>
				</div>
				<div class="row">
					<div class="col-xs-3 showlabel">Type</div>
					<div class="col-xs-9 showvalue">{{ $module->moduletype->name }}</div>
				</div>
                <?php /*
				<div class="row">
					<div class="col-xs-3 showlabel">Figuren</div>
					<div class="col-xs-9 showvalue">
						<div class="row">
							@foreach ($module->moduleimages as $moduleimage)
							<div class="col-xs-12 col-sm-6 col-md-4 center">
								{{ HTML::image('uploads/images/' . $moduleimage->image->url, $moduleimage->image->word, [
										'class' => 'module-show-image'
									])}}
								<h5>{{ $moduleimage->image->word}}</h5>
							</div>
						@endforeach

						</div>
					</div>
				</div>
				 */ ?>
                <div class="row">
                    <div class="col-xs-12">
                        <h3>Figuren</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12" id="images">
                        @foreach ($module->moduleimages as $mi)
                        <div class="image" style="cursor: default; background-image: url('{{ $baseUrl }}uploads/images/{{ $mi->image->url }}')">
                            {{ HTML::image('css/images/aspect-1-1.png', '', [
                            'class' => 'aspect-1-1'
                            ])}}
                            <div class="abs">
                                <div class="image-name">{{ $mi->image->word }}</div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
				<div class="row">

					{{ HTML::link('/admin/module', 'Terug naar de lijst', ['class' => 'btn btn-default btn-m']), PHP_EOL }}
				</div>
			</div>
		</div>
	</div>
@stop
@section('styles')
<!-- CHOSEN CSS + BOOTSTRAP CHANGES-->
@parent
{{ HTML::style("/css/module.css") }}
@stop