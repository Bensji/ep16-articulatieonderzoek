@extends('layouts.master')
@section('content')
	<div class="col-xs-12">
		<div class="row">
			<div class="col-xs-10">
				<h1>Module</h1>
			</div>
			<div class="col-xs-2">
				<a id="menu-toggle" href="#" class="btn btn-default"><i class="fa fa-bars"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<h3>Pas module {{$module->name}} aan</h3>
			</div>
			<div class="col-xs-12 blur-back">
				{{ Form::model( $module, [ 'route' => [ 'admin.module.update', $module->id ],'method' => 'put', 'class' => 'form-horizontal']) ,PHP_EOL}}
					@if(Session::has('auth-error-message'))
					            <div class="alert-box radius" data-alert>
					                {{ Session::get('auth-error-message') }}
					            </div>
					@endif
					<div class="form-group">
						{{ Form::label('name', 'Naam', [
							'class' => 'col-sm-3 control-label'
							] ), PHP_EOL }}
						<div class="col-sm-9">
							{{ Form::text('name', $value=null, [
					        'placeholder' 	=> 'Jansens',
					        'class' 		=> 'form-control'
					        ]), PHP_EOL }}
						    @if ($errors->has('name'))
					        	{{ $errors->first('name', '<small class="error">:message</small>') }}
					        @endif
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('description', 'Omschrijving', [
							'class' => 'col-sm-3 control-label'
							] ), PHP_EOL }}
						<div class="col-sm-9">
							{{ Form::text('description', $value=null, [
					        'placeholder' 	=> 'Dit is een module...',
					        'class' 		=> 'form-control'
					        ]), PHP_EOL }}
						    @if ($errors->has('description'))
					        	{{ $errors->first('description', '<small class="error">:message</small>') }}
					        @endif
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('moduletype_id', 'Moduletype', [
							'class' => 'col-sm-3 control-label'
							]), PHP_EOL}}
							<div class="col-sm-9">
								{{ Form::select('moduletype_id', $moduletypes, null, [
									'class' => 'form-control userselectbox',
									'data-placeholder' => 'Kies een moduletype'
								]) , PHP_EOL}}
							@if ($errors->has('moduletype_id'))
					        	{{ $errors->first('moduletype_id', '<small class="error">:message</small>') }}
					        @endif
							</div>
					</div>

					<div class="form-group add-select-row">

                        <div class="col-sm-3"><h3>Figuren</h3></div>
					</div>
                <?php
                    echo '<div class="form-group">
										<div class="col-sm-3 selectbox">
											<select class="form-control userselectbox" id="image-select" disabled="disabled">';
                echo '<option value="0">Figuren toevoegen</option>';

                foreach ($images as $image)
                    {
                        //echo '+ \'<option '.( ($key == $selectvalue) ? 'selected="selected"': '').' value="'.$key.'">'.$value.'</option>\' ';
                        echo '<option value="' . $image->id . '">' . $image->word . '</option>';
                    }

                    echo 		'</select></div></div>';
                ?>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div class="row" id="images">
                                @foreach ($selectedimages as $si)
                                <div class="image" data-image-id="{{ $si->id }}" style="background-image: url('{{ $baseUrl }}/uploads/images/{{ $si->url }}')">
                                    {{ HTML::image('css/images/aspect-1-1.png', '', [
                                    'class' => 'aspect-1-1'
                                    ])}}
                                    <div class="abs">
                                        <div class="image-name">{{ $si->word }}</div>
                                    </div>
                                    <div class="remove">
                                        <i class="fa fa-times"></i>
                                    </div>
                                </div>
                                @endforeach
                                <?php
                                    //echo public_path();
                                ?>
                            </div>
                        </div>
                        <?php
                        $idString = '';
                        $counter = 0;
                        foreach ($selectedimages as $image){
                            if ($counter == 0){
                                $idString .= $image->id;
                            } else {
                                $idString .= '|' . $image->id;
                            }
                            $counter++;
                        }
                        ?>
                        <input type="hidden" id="image-ids" name="image-ids" value="{{ $idString }}">
                    </div>
					<div class="form-group">
						<div class="col-sm-9">
					    {{ Form::submit('Opslaan', [
					    	'class'	=> 'btn btn-default'
					    	]) , PHP_EOL}}
				    	</div>
					</div>

				{{ Form::close() }}
			</div>
		</div>
	</div>
@section('styles')
	<!-- CHOSEN CSS + BOOTSTRAP CHANGES-->
	@parent
	{{ HTML::style("/scripts/chosen_v1.1.0/chosen.css") }}
	{{ HTML::style("/scripts/datepicker/css/datepicker.css") }}
    {{ HTML::style("/css/module.css") }}
@stop
@section('scripts')
	@parent
	{{ HTML::script("/scripts/chosen_v1.1.0/chosen.jquery.js") }}
    {{ HTML::script("/components/jquery.disableSelect.js") }}
	<script type="text/javascript">
		/*$(".userselectbox").chosen({
		    disable_search_threshold: 5,
		    no_results_text: "Geen logopedisten gevonden.",
		    width: "100%"
		  });*/
	</script>
	{{ HTML::script("/scripts/datepicker/js/bootstrap-datepicker.js") }}
    {{ HTML::script("/scripts/ModuleImageOrder.js") }}
	<script>
		$('.datepicker').datepicker({
			format: 'dd-mm-yyyy'
		});
        $(document).ready(function(){
            var mio = new ModuleImageOrder('../../../');
        });
	</script>

	<script>
		var test = '<div class="form-group">'
					+'		<div class="col-sm-8 col-sm-offset-3">'
					+'			<select class="form-control userselectbox" data-placeholder="Kies de figuren" id="images[]" name="images[]">'
						<?php
							foreach ($images as $key => $value)
							{
								echo '+ \'<option value="'.$key.'">'.$value.'</option>\' ';
							};
						?>
					+'			</select>'
					+'		</div>'
					+'		<div class="col-sm-1">'
					+'			<a href="#" class="delete-select-row-button" ><i class="fa fa-minus"></i></a>'
					+'		</div>'
					+'	</div>';

		$('.add-select-row-button').click( function(e) {
			e.preventDefault();
			$(test).insertBefore('.add-select-row');
			// console.log( $( this ).parent().parent().prev().find('.userselectbox') );
			$( this ).parent().parent().prev().find('.userselectbox').chosen({
			    disable_search_threshold: 5,
			    no_results_text: "Geen logopedisten gevonden.",
			    width: "100%"
			  });

			$('.delete-select-row-button').click( function(e) {
				e.preventDefault();
				$( this ).parent().parent().remove();
			});
		});

		$('.delete-select-row-button').click( function(e) {
			e.preventDefault();
			$( this ).parent().parent().remove();
		});



	</script>

@stop
@stop