<?php
function cmp($a, $b)
{
    return strcmp($a["addition_order"], $b["addition_order"]);
}

$phonCats[0] = 'kies een categorie';
ksort($phonCats);

?>

@extends('layouts.masterblade')
@section('body')
	<div class="col-xs-12">
		<div class="row">
			<div class="col-xs-10">
				<h1>Analyseren</h1>
			</div>
			<div class="col-xs-2">
				<a id="menu-toggle" href="#" class="btn btn-default"><i class="fa fa-bars"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				{{ HTML::link('/test', 'Terug naar de lijst', ['class' => 'btn btn-default btn-margin-bottom']), PHP_EOL }}
				<a href="{{ route('test.analized.update',$testRecordings->first()->test->id) }}" class="btn btn-default btn-margin-bottom">
					Volledig geanalyseerd
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 ">
				@foreach ($testRecordings as $testRecording)
					<div class="row blur-back analyze-row" style='margin-bottom: 30px'>
						<div class="analyze-sending"><i class="fa fa-spinner"></i></div>
						<div class="col-xs-12">
							<h3>{{$testRecording->image->word}}</h3>
						</div>
						<div class="col-xs-4">
							{{ HTML::image('uploads/images/' . $testRecording->image->url, $testRecording->image->word, [
											'class' => 'analyze-image'
								])}}
							<audio controls id="word-sound">
				  				<source src="{{ url('uploads/recordings/'.$testRecording->filename)}}" type="audio/wav">
							</audio>
							@if ($testRecording->wordplay)
								<h5>Nagezegd</h5>
							@elseif ($testRecording->sentenceplay)
								<h5>Voorzegzin</h5>
							@else
								<h5>Benoemd</h5>
							@endif
						</div>
						<div class="col-xs-8">
                            <div class="row">
                                <div class="col col-xs-12">
                                    <h4>Fonetisch</h4>
                                </div>
                            </div>
							<?php
                                $wordLenght = 0;
                                $image = $testRecording->image;
                                $phonetics = $image->phonetics;
                                $transcriptions = $testRecording->transcriptions();
					    		$wordLenght = count($phonetics);
                                $empty = [];
					    		//for ($i=0; $i < $wordLenght; $i++) {
                                $i=0;
                            echo '<div class="row" data-index="0"><div class="col-xs-1 addrow-wrapper letter"><i class="fa fa-plus addrow"></i></div></div>';

                            foreach ($phonetics as $phonetic){
                                    $hasAddition = false;

                                    $sound = null;
                                    $distortion = null;
                                    $additions = [];
                                    foreach ($transcriptions as $transcription){
                                        if ($transcription->sound_index == $i && is_null($transcription->addition_order)){
                                            if (!is_null($transcription->sound_id)){
                                                $sound = Sound::where('id', '=', $transcription->sound_id)->first();
                                                $distortion = Distortion::where('id', '=', $transcription->distortion_id)->first();
                                            }
                                        } else if ($transcription->sound_index == $i){
                                            array_push($additions, $transcription);
                                        }
                                    }
                                    usort($additions, "cmp");
                                    foreach ($additions as $addition){
                                        $name = 'phonetic-' . $i . '-' . $addition->addition_order;
                                        $hiddenName = $name . '-value';
                                        echo '<div class="row row-eval-addition" data-index="'.$i.'" data-addition-index="' . $addition->addition_order . '">';
                                        echo '<div class="col-xs-1 letter"><i class="fa fa-minus deleterow"></i></div>';
                                        echo '<div class="col-xs-2 input-wrapper">';
                                        echo '<input class="form-control phonetic mousetrap phonetics-only" data-max-phonetics="1" disabled="disabled" name="' . $name . '" value="';
                                        echo $addition->sound->phonetic;

                                        echo '">';
                                        ?>
                                        <input type="hidden" id="{{ $hiddenName }}" name="{{ $hiddenName }}" value="{{ $addition->sound_id }}">
                                        <?php
                                        echo '</div>';//col-input
                                        echo '</div>';//row
                                        echo '<div class="row" data-index="' . $i . '"><div class="col-xs-1 addrow-wrapper letter"><i class="fa fa-plus addrow"></i></div></div>';

                                    }
					    			echo '<div class="row row-eval" data-index="'.$i.'">';
					    			echo '<div class="col-xs-1 letter" data-phonetic-id="' . $phonetic->id . '">'. $phonetic->phonetic .'</div>';
					    			echo '<div class="col-xs-2 input-wrapper">';
					    			//echo '<input class="phonetic form-control" data-index="'.$i.'" value="';
                                    ?>
                                    <?php
                                    //{{ Form::text('phonetic', $phoneticString, [
                                    $name = 'phonetic-' . $i;
                                    $hiddenName = $name . '-value';
                                    if (is_null($sound)){
                                        $sound = [
                                            'phonetic' => '',
                                            'id' => ''
                                        ];
                                    }
                                    ?>
                                    {{ Form::text($name, $sound['phonetic'], [
                                    'placeholder' 	=> '',
                                    'class' 		=> 'form-control phonetic mousetrap phonetics-only',
                                    'autocomplete'	=> 'off',
                                    'disabled'      => 'disabled',
                                    'data-index'    => $i,
                                    'data-max-phonetics' => 1
                                    ]), PHP_EOL }}
                                    @if ($errors->has('phonetic'))
                                    {{ $errors->first('phonetic', '<small class="error">:message</small>') }}
                                    @endif
                                    <input type="hidden" id="{{ $hiddenName }}" name="{{ $hiddenName }}" value="{{ $sound['id'] }}">
                                    <?php
					    			echo '</div>';//col-input
					    			echo '<div class="col-xs-9 select-wrapper">';

					    	 			$mistakeArray = [];
                                        foreach ($empty as $testmistake){
					    				//foreach ($testRecording->testmistakes as $testmistake) {
					    					if( $testmistake->letterindex === $i &&
					    						$testmistake->substitution == null &&
					    						$testmistake->beforeindex == null)
					    					{

					    						$mistakeArray[] = $testmistake->mistake_id;
					    					}
					    				};
                                    $distortions = $testRecording->image->phonetics[$i]->distortions->lists('name', 'id');
                                    $distortions[0] = 'distorsie (geen)';
                                    ksort($distortions);
                                    if ($distortion != null){
                                        $val = $distortion->id;
                                    } else {
                                        $val = 0;
                                    }
                                    if (!$phonetic->vowel && !$sound['vowel']){
                                    //if ($phonetic->id == $sound['id']){
                                        echo Form::select('distorsie', $distortions, $val, [
                                                'class' => 'form-control mistakeselectbox',
                                                'data-placeholder' => 'Distorsie (geen)'
                                            ]) , PHP_EOL;
                                    } else {
                                        echo Form::select('distorsie', $distortions, '0', [
                                            'class' => 'form-control mistakeselectbox',
                                            'data-placeholder' => 'Distorsie (geen)',
                                            'style' => 'display: none;'
                                        ]) , PHP_EOL;
                                    }
					    			echo '</div>';//col-dropdown
					    			echo '</div>';//row
					    			echo '<div class="row" data-index="' . ($i+1) . '"><div class="col-xs-1 addrow-wrapper letter"><i class="fa fa-plus addrow"></i></div></div>';
					    			if($hasAddition)
					    			{
					    				$j = 0;
					    				foreach ($testRecording->testmistakes as $testmistake)
					    				{
					    					if(	$testmistake->letterindex == null &&
					    						$testmistake->beforeindex === $i &&
					    						$testmistake->substitution != null)
					    					{
						    					echo '<div class="row row-eval-addition" data-index="'.$i.'">';
						    					echo '<div class="col-xs-1 letter"><i class="fa fa-minus deleterow"></i></div>';
						    					echo '<div class="col-xs-2 input-wrapper">';
						    					echo '<input class="phonetic form-control" data-index="'.$i.'" value="';
							    				echo $testmistake->substitution;

							    				echo '">';
						    					echo '</div>';//col-input
						    					echo '<div class="col-xs-9 select-wrapper">';
						    					$mistakeArray = [];
							    				foreach ($testRecording->testmistakes as $testmistake) {
							    					if( $testmistake->letterindex == null &&
							    						$testmistake->substitution == null &&
							    						$testmistake->beforeindex === $i &&
							    						$testmistake->additionindex === $j)
							    					{
							    						$mistakeArray[] = $testmistake->mistake_id;
							    					};
							    				};
								    			echo Form::select('mistakes[]', [], $mistakeArray, [
								    			//echo Form::select('mistakes[]', $mistakes, $mistakeArray, [
														'class' => 'form-control mistakeselectbox',
														'data-placeholder' => 'Kies een fout',
														'multiple' => true
													]) , PHP_EOL;
						    					echo '</div>';//col-dropdown
						    					echo '</div>';//row
						    					echo '<div class="row"><div class="col-xs-1 addrow-wrapper letter"><i class="fa fa-plus addrow"></i></div></div>';
						    					$j++;
					    					}
					    				}
					    			}
                                    $i++;?>
                            <?php   }
                            $additions = [];
                            foreach ($transcriptions as $transcription){
                                if ($transcription->sound_index == count($phonetics)){
                                    array_push($additions, $transcription);
                                }
                            }
                            usort($additions, "cmp");
                            foreach ($additions as $addition){
                                $name = 'phonetic-' . $i . '-' . $addition->addition_order;
                                $hiddenName = $name . '-value';
                                echo '<div class="row row-eval-addition" data-index="'.$i.'" data-addition-index="' . $addition->addition_order . '">';
                                echo '<div class="col-xs-1 letter"><i class="fa fa-minus deleterow"></i></div>';
                                echo '<div class="col-xs-2 input-wrapper">';
                                echo '<input class="form-control phonetic mousetrap phonetics-only" data-max-phonetics="1" disabled="disabled" name="' . $name . '" value="';
                                echo $addition->sound->phonetic;

                                echo '">';
                                ?>
                                <input type="hidden" id="{{ $hiddenName }}" name="{{ $hiddenName }}" value="{{ $addition->sound_id }}">
                                <?php
                                echo '</div>';//col-input
                                echo '</div>';//row
                                echo '<div class="row" data-index="' . $i . '"><div class="col-xs-1 addrow-wrapper letter"><i class="fa fa-plus addrow"></i></div></div>';

                            }

					    	?>
                            <div class="row">
                                <div class="col col-xs-12">
                                    <h4>Fonologisch</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-xs-4">
                                    {{ Form::select('phonologicalError', $phonCats, '0', [
                                    'class' => 'form-control phon-cats',
                                    'data-placeholder' => 'categorie (geen)',
                                    'disabled' => 'disabled'
                                    ]) , PHP_EOL; }}
                                </div>
                                <div class="col col-xs-8">
                                    {{ Form::select('distorsie', [], '0', [
                                    'class' => 'form-control phon-errors',
                                    'data-placeholder' => 'proces (geen)',
                                    'disabled' => 'disabled'
                                    ]) , PHP_EOL; }}
                                </div>
                            </div>
                            <div class="row">
                                <h4 class="col col-xs-12 phon-error-list">
                                    @foreach ($testRecording->phonological_errors as $error)
                                    <span class="label label-default" data-phon-error-id="{{ $error->id }}" style="margin-right: 3px;">
                                        {{ $error->name }}&nbsp;&nbsp;
                                        <i class="fa fa-times-circle white remove-phon-err"></i>
                                    </span>
                                    @endforeach
                                </h4>
                            </div>
					    	<button type="button" data-imageid="{{$testRecording->image->id}}" data-testrecordingid="{{$testRecording->id}}" data-testid="{{$testRecording->test->id}}" class="btn btn-default sendmistakes">Opslaan</button>
						</div>
					</div>
				@endforeach
			</div>
			<div class="pagination-wrapper">
                <?php
					//{{$testRecordings->links();}}
                ?>
			</div>
		</div>
	</div>
	<div id="rowaddition">
        <div class="row"><div class="col-xs-1 addrow-wrapper letter"><i class="fa fa-plus addrow"></i></div></div>
		<div class="row row-eval-addition">
			<div class="col-xs-1 letter"><i class="fa fa-minus deleterow"></i></div>
			<div class="col-xs-2 input-wrapper">
    			<input class="form-control phonetic mousetrap phonetics-only" type="text" data-index="" value="" data-max-phonetics="1">
                <input type="hidden" id="" name="" value="">
			</div>
			<div class="col-xs-9 select-wrapper">

    			<?php
                //{{ Form::select('mistakes[]', $mistakes, null, [

    			/*{{ Form::select('mistakes[]', [], null, [
						'class' => 'form-control mistakeselectboxadded',
						'data-placeholder' => 'Kies een fout',
						'multiple' => true
					]) , PHP_EOL;}}*/
                ?>
			</div>
		</div>
	</div>
@section('styles')
	<!-- CHOSEN CSS + BOOTSTRAP CHANGES-->
	@parent
	{{ HTML::style("/scripts/chosen_v1.1.0/chosen.css") }}
    {{ HTML::style('/css/phoneticdropdown.css') }}
@stop
@section('scripts')
    {{ HTML::script("http://ajax.aspnetcdn.com/ajax/jquery/jquery-2.0.3.min.js") }}
    {{ HTML::script("scripts/TestAnalysis.js") }}

@parent
<script>
	/*(function(){
		$(".sendmistakes").click(function(){
			var testId = $(this).attr("data-testid");
			var imageId = $(this).attr("data-imageid");
			var testrecordingId = $(this).attr("data-testrecordingid");
			var mistakes = [];

			$(this).siblings('.row-eval').each(function( index ){
	    		if( $(this).children(".letter").html() != $(this).children(".input-wrapper").children(".phonetic").val() )
	    		{
	    			if( $(this).children(".input-wrapper").children(".phonetic").val() == '.' )
		    		{
		    			var mistake = {};
		    			mistake["letterindex"] = index;
		    			mistake["imageId"] = imageId;
		    			mistake["mistakeId"] = 4;
		    			mistake["substitution"] = '.';
		    			mistake["beforeindex"] = null;
	    				mistake["additionindex"] = null;
		    			mistake["testrecordingId"] = testrecordingId;
		    			mistakes.push(mistake);
		    		}
		    		else{
		    			var mistake = {};
		    			mistake["letterindex"] = index;
		    			mistake["imageId"] = imageId;
		    			mistake["mistakeId"] = 5;
		    			mistake["substitution"] = $(this).children(".input-wrapper").children(".phonetic").val();
		    			mistake["beforeindex"] = null;
	    				mistake["additionindex"] = null;
		    			mistake["testrecordingId"] = testrecordingId;
		    			mistakes.push(mistake);
		    		}
	    		}
	    		$(this).children(".select-wrapper").children(".mistakeselectbox").children("option:selected").each(function(){
	    			var mistake = {};
	    			mistake["letterindex"] = index;
	    			mistake["imageId"] = imageId;
	    			mistake["mistakeId"] = $(this).val();
	    			mistake["substitution"] = null;
	    			mistake["beforeindex"] = null;
	    			mistake["additionindex"] = null;
	    			mistake["testrecordingId"] = testrecordingId;
	    			mistakes.push(mistake);
	    		});

	    	});
			$(this).siblings('.row-eval-addition').each(function( index ){
				var beforeindex = $(this).prev().prev().attr('data-index');
				console.log('beforeindex: ', beforeindex);
				var additionindex = $(this).prevAll('.row-eval-addition[data-index="'+beforeindex+'"]').length;
				if($(this).children(".input-wrapper").children(".phonetic").val() != ""){
					var mistake = {};
	    			mistake["beforeindex"] = beforeindex;
	    			mistake["additionindex"] = additionindex;
	    			mistake["imageId"] = imageId;
	    			mistake["mistakeId"] = "6";
	    			//HIER EIGENLIJK GEEN SUBSTITUTIE MAAR INGEGEVEN LETTER WORDT BIJGEHOUDEN
	    			mistake["substitution"] = $(this).children(".input-wrapper").children(".phonetic").val();
	    			mistake["letterindex"] = null;
	    			mistake["testrecordingId"] = testrecordingId;
	    			mistakes.push(mistake);
				}
				$(this).children(".select-wrapper").children(".mistakeselectboxadded").children("option:selected").each(function(){
	    			var mistake = {};
	    			mistake["beforeindex"] = beforeindex;
	    			mistake["additionindex"] = additionindex;
	    			mistake["imageId"] = imageId;
	    			mistake["mistakeId"] = $(this).val();
	    			mistake["substitution"] = null;
	    			mistake["letterindex"] = null;
	    			mistake["testrecordingId"] = testrecordingId;
	    			mistakes.push(mistake);
	    		});
	    		$(this).children(".select-wrapper").children(".mistakeselectbox").children("option:selected").each(function(){
	    			var mistake = {};
	    			mistake["beforeindex"] = beforeindex;
	    			mistake["additionindex"] = additionindex;
	    			mistake["imageId"] = imageId;
	    			mistake["mistakeId"] = $(this).val();
	    			mistake["substitution"] = null;
	    			mistake["letterindex"] = null;
	    			mistake["testrecordingId"] = testrecordingId;
	    			mistakes.push(mistake);
	    		});
			});
			var data = new FormData();
			data.append('imageId', imageId );
			data.append('testrecordingId', testrecordingId );
			serializedMistakes = JSON.stringify(mistakes);
			data.append('mistakes', serializedMistakes);
			console.log(serializedMistakes);

			var url = '{{ Request::root()}}/test/analyze/' + testId;
			$.ajax({
			  type: "POST",
			  url: url,
			  processData: false,
			  contentType: false,
			  data: data,
			  beforesend: function(){
			  	$('.analyze-sending').show();
			  },
			  success: function(){
			  	console.log("gelukte post");
			  	$('.analyze-sending').hide();

			  },
			  error: function (xhr, status, error){
			  	var err = eval("(" + xhr.responseText + ")");
  				console.log(err.Message);
			  	console.log(error);
			  }
			});
		});
		$('.analyze-sending').hide();
		$(document).on("click", ".eval-button", function(){
	        var phonetic = $(this).html();
	        //console.log(phonetic);

	        $(this).parent().siblings(".input-wrapper").children(".phonetic").val(phonetic);
    	});
    	$(document).on("click", ".fa-plus", function(){
	    	var previousIndex = $(this).parent().parent().prev().attr('data-index');
	    	rowaddition = $('#rowaddition').html();
	        $(this).parent().parent().after(rowaddition);
	        $(this).parent().parent().next().attr('data-index',previousIndex);
	        setPhoneticFields();
	        $(this).parent().parent().next(".row-eval-addition").children(".select-wrapper").children(".mistakeselectboxadded").chosen({
			    disable_search_threshold: 5,
			    no_results_text: "Geen logopedisten gevonden.",
			    width: "100%"
			  });
	    });

	    $(document).on("click", ".fa-minus", function(){
	    	$(this).parent().parent().next().remove();
	        $(this).parent().parent().remove();
	    });
	}(window))*/
</script>
<!-- PHONETIC -->
{{ HTML::script("/components/jquery.caret.js") }}
{{ HTML::script("/components/mousetrap.min.js") }}
{{ HTML::script("/scripts/phonetic/PhoneticDropDown.js") }}
<script>
    $(document).ready(function(){
        var phoneticDropDown = new PhoneticDropDown('../../');
        var analyze = new TestAnalysis('../../');
        $('.analyze-sending').hide();
    });
</script>
{{ HTML::script("/scripts/chosen_v1.1.0/chosen.jquery.js") }}
	<script type="text/javascript">
		/*$(".mistakeselectbox").chosen({
		    disable_search_threshold: 5,
		    no_results_text: "Geen logopedisten gevonden.",
		    width: "100%"
		  });*/
	</script>

@stop
@stop