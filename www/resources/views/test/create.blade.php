@extends('layouts.masterblade')
@section('body')

    <div class="col-xs-12">
    <div class="row">
    <div class="col-xs-10">
    <h1>Testen</h1>
    </div>
    <div class="col-xs-2">
    <a id="menu-toggle" href="#" class="btn btn-default"><i class="fa fa-bars"></i></a>
    </div>
    </div>
    <div class="row">
    <div class="col-xs-12">
    <h3>Start een test</h3>
    </div>
    <div class="col-xs-12 blur-back">
    {{ Form::open(['route' => 'test.store', 'class' => 'form-horizontal']), PHP_EOL }}
    @if(Session::has('auth-error-message'))
        <div class="alert-box radius" data-alert>
            {{ Session::get('auth-error-message') }}
        </div>
    @endif

    <div class="form-group">
        {{ Form::label('patient_id', 'Kind', [
            'class' => 'col-sm-3 control-label'
            ]), PHP_EOL}}
            <div class="col-sm-9">
                {{ Form::select('patient_id', $children, null, [
                    'class' => 'form-control userselectbox',
                    'placeholder' => 'Kies een kind'
                ]) , PHP_EOL}}
            @if ($errors->has('patient_id'))
                {{ $errors->first('patient_id', '<small class="error">:message</small>') }}
            @endif
            </div>
    </div>
    <div class="form-group">
        {{ Form::label('module_id', 'Module', [
            'class' => 'col-sm-3 control-label'
            ]), PHP_EOL}}
            <div class="col-sm-9">
                {{ Form::select('module_id', $modules, null, [
                    'class' => 'form-control userselectbox',
                    'placeholder' => 'Kies een module'
                ]) , PHP_EOL}}
            @if ($errors->has('module_id'))
                {{ $errors->first('module_id', '<small class="error">:message</small>') }}
            @endif
            </div>
    </div>
    <div class="form-group">
        {{ Form::label('normering', 'Normering', [
            'class' => 'col-sm-3 control-label'
            ] ), PHP_EOL }}
        <div class="col-sm-9">
            {{ Form::radio('forstats', true, false) , PHP_EOL}}
            {{ Form::label('forstats', 'Voor normering') , PHP_EOL}}</br>
            {{ Form::radio('forstats', false, true) , PHP_EOL}}
            {{ Form::label('forstats', 'Niet voor normering') , PHP_EOL}}</br>
            @if ($errors->has('forstats'))
                {{ $errors->first('forstats', '<small class="error">:message</small>') }}
            @endif
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-9 col-sm-offset-3">
        {{ Form::submit('Start test', [
            'class'	=> 'btn btn-default'
            ]) , PHP_EOL}}
        </div>
    </div>

    {{ Form::close() }}
    </div>
    </div>
    </div>
@stop