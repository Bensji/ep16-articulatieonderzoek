@extends('layouts.masterblade')
@section('body')
	<div class="col-xs-12">
		<div class="row">
			<div class="col-xs-10">
				<h1>Rapport</h1>
			</div>
			<div class="col-xs-2">
				<a id="menu-toggle" href="#" class="btn btn-default"><i class="fa fa-bars"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<h3>{{ $test->patient->name }} {{ $test->patient->givenname }}</h3>
			</div>
			<div class="col-xs-12 blur-back">
				<div class="row">
					<div class="col-xs-2 showlabel">Test gestart</div>
                    <?php
                        $start = new DateTime($test->created_at);
                    ?>
					<div class="col-xs-10 showvalue">{{ $start->format('j-n-Y G:i') }}</div>
				</div>
				<div class="row">
					<div class="col-xs-2 showlabel">Test beëindigd</div>
                    <?php
                        $end = new DateTime($test->ended);
                    ?>
					<div class="col-xs-10 showvalue">{{ $end->format('j-n-Y G:i') }}</div>
				</div>
				<div class="row">
					<div class="col-xs-2 showlabel">Module</div>
					<div class="col-xs-10 showvalue">{{ $test->module->name }}</div>
				</div>
                <div class="row">
                    <div class="col-xs-2 showlabel">Correct</div>
                    <div class="col-xs-10 showvalue">{{ $correctCount }}</div>
                </div>
                <div class="row">
                    <div class="col-xs-2 showlabel">Foutief</div>
                    <div class="col-xs-10 showvalue">{{ $wrongCount }}</div>
                </div>
                <div class="row">
                    <div class="col-xs-2 showlabel">Totaal</div>
                    <div class="col-xs-10 showvalue">{{ $totalCount }}</div>
                </div>
				<?php /*<div class="row">
					<div class="col-xs-3 showlabel">Totaal aantal fouten</div>
					<div class="col-xs-9 showvalue"><?php //{{ $test->testmistakes->count() }} ?></div>
				</div>
				<div class="row">
					<div class="col-xs-3 showlabel">Fouten</div>
					<div class="col-xs-9 showvalue">
						@foreach ($test->testmistakes as $testmistake)
							{{$testmistake->mistake->name}} 
							@if ( $testmistake->mistake->name == "substitutie" )
								: 
								{{ substr($testmistake->image->word, $testmistake->letterindex, 1)}} 
								->
								{{ $testmistake->substitution }}
							@else
								: {{ substr($testmistake->image->word, $testmistake->letterindex, 1) }}
							@endif
						</br>
						@endforeach
					</div>
				</div>
                        */ ?>
			</div>
		</div>
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12">
                        <h2>Fonetisch</h2>
                    </div>
                </div>
            </div>
        </div>
        @if (count($omissions['sound_class']) > 0)
        <div class="row">
            <div class="col-xs-12 blur-back">
                <div class="row">
                    <div class="col-xs-12">
                        <h3>Omissies <span class="count">{{ count($omissions['sound_class']) }}</span></h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <h4>Klankklasse</h4>
                        <ul class="error-list">
                            @foreach ($omissions['sound_class'] as $sc => $count)
                            <li><span class="count">{{ $count }}</span><span>{{ $sc }}</span></li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-xs-4">
                        <h4>Articulatieplaats</h4>
                        <ul class="error-list">
                            @foreach ($omissions['articulation_place'] as $ap => $count)
                            <li><span class="count">{{ $count }}</span><span>{{ $ap }}</span></li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-xs-4">
                        <h4>Stemhebbend/-loos</h4>
                        <ul class="error-list">
                            @foreach ($omissions['voiced_voiceless'] as $vv => $count)
                            <li><span class="count">{{ $count }}</span><span>{{ $vv }}</span></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @if (count($substitutions['sound_class']) > 0)
        <div class="row">
            <div class="col-xs-12 blur-back">
                <div class="row">
                    <div class="col-xs-12">
                        <h3>Substituties <span class="count">{{ count($substitutions['sound_class']) }}</span></h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <h4>Klankklasse</h4>
                        <ul class="error-list">
                            @foreach ($substitutions['sound_class'] as $sc => $count)
                            <li><span class="count">{{ $count }}</span><span>{{ str_replace("_", "&nbsp;&RightArrow;&nbsp;", $sc) }}</span></li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-xs-4">
                        <h4>Articulatieplaats</h4>
                        <ul class="error-list">
                            @foreach ($substitutions['articulation_place'] as $ap => $count)
                            <li><span class="count">{{ $count }}</span><span>{{ str_replace("_", "&nbsp;&RightArrow;&nbsp;", $ap) }}</span></li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-xs-4">
                        <h4>Stemhebbend/-loos</h4>
                        <ul class="error-list">
                            @foreach ($substitutions['voiced_voiceless'] as $vv => $count)
                            <li><span class="count">{{ $count }}</span><span>{{ str_replace("_", "&nbsp;&RightArrow;&nbsp;", $vv) }}</span></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @if (count($distortions) > 0)
        <div class="row">
            <div class="col-xs-12 blur-back">
                <div class="row">
                    <div class="col-xs-12">
                        <h3>Distorsies <span class="count">{{ count($distortions) }}</span></h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <ul class="error-list">
                            @foreach ($distortions as $dt => $count)
                            <li><span class="count">{{ $count }}</span><span>{{ $dt }}</span></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @if ($additions > 0)
        <div class="row">
            <div class="col-xs-12 blur-back">
                <div class="row">
                    <div class="col-xs-12">
                        <h3>Addities <span class="count">{{ $additions }}</span></h3>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12">
                        <h2>Fonologisch</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 blur-back">
                <div class="row">
                    <?php
                        //var_dump($debug);
                    ?>
                    @foreach ($phonological as $name => $items)
                    <div class="col-xs-3">
                        <h4>{{ ucfirst($name) }}</h4>
                        <ul class="error-list">
                            @foreach ($items as $itemname => $itemcount)
                            <li><span class="count">{{ $itemcount }}</span><span>{{ $itemname }}</span></li>
                            @endforeach
                        </ul>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
            {{ HTML::link('/test', 'Terug naar het overzicht', ['class' => 'btn btn-default btn-m']), PHP_EOL }}
            </div>
        </div>
	</div>
@stop