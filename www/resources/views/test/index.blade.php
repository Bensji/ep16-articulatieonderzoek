@extends('layouts.masterblade')
@section('body')
	<div class="col-xs-12">
		<div class="row">
			<div class="col-xs-10">
				<h1>Testen</h1>
			</div>
			<div class="col-xs-2">
				<a id="menu-toggle" href="#" class="btn btn-default"><i class="fa fa-bars"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				{{ HTML::link('/test/create', 'Start een test', ['class' => 'btn btn-default btn-margin-bottom']), PHP_EOL }}
				{{ Form::open(['route' => 'test.index', 'class' => 'form-horizontal', 'method' => 'get']), PHP_EOL }}
					{{ Form::text('search', '', [
					        'placeholder' 	=> 'Zoeken op naam',
					        'class' 		=> 'form-control btn-margin-bottom',
					        'data-provide'	=> 'typeahead',
					        'data-source'	=> json_encode($children),
					        'data-items'	=> 5,
					        'autocomplete'	=> 'off'
					        ]), PHP_EOL }}
				{{ Form::close() }}
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 blur-back">
				<div class="table-responsive">
					<table class="table table-striped">
						<tr>
							<th>Naam</th>
							<th>Module</th>
							<th>Status</th>
							<th></th>
						</tr>
						@foreach ($tests as $test)
							<tr>
								<td>{{ $test->patient->name }}</td>
								<td>{{ $test->module->name }}</td>
								<td>
									@if ( $test->analyzed )
										Geanalyseerd
									@elseif ( $test->ended != null)
										Te analyseren
									@else
										Onafgewerkt
									@endif
								</td>
								<td>
									@if ( $test->analyzed )
										<a href="{{ route('test.test',$test->id) }}" class="btn btn-xs btn-default btn-margin-bottom">Rapport</a>

									</a>
									@elseif ( $test->ended != null)
										<a href="{{ route('test.test',$test->id) }}" class="btn btn-xs btn-default btn-margin-bottom">Analyseren</a>
									@else
										<a href="{{ route('test.test',$test->id) }}" class="btn btn-xs btn-default btn-margin-bottom">Afwerken</a>
									@endif
								</td>
							</tr>
						@endforeach
						@if(empty($tests) )
							<tr><td colspan="3">Er zijn nog geen testen toegevoegd</td></tr>
						@endif
					</table>
				</div>
			</div>
		</div>
	</div>
@stop