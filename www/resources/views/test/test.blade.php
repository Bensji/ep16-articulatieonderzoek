<?php
/**
 * Created by PhpStorm.
 * User: Bart
 * Date: 10/12/14
 * Time: 19:03
 */
    if (is_null($test->indexend)){
        $index = 1;
    } else {
        $index = $test->indexend;
    }
?>

<!doctype html>
<!-- [if lt IE 7]><html dir="ltr" lang="nl-be" class="no-js lt-ie7"><![endif]-->
<!-- [if lt IE 8]><html dir="ltr" lang="nl-be" class="no-js lt-ie8"><![endif]-->
<!-- [if lt IE 9]><html dir="ltr" lang="nl-be" class="no-js lt-ie9"><![endif]-->
<!-- [if lt IE 10]><html dir="ltr" lang="nl-be" class="no-js lt-ie10"><![endif]-->
<!-- [if (gt IE 9)|!(IE)]><!--><html dir="ltr" lang="nl-be" class="no-js" prefix="og: http://ogp.me/ns#"><!--<![endif]-->
<head>
    <!-- CHARACTER ENCODING -->
    <meta charset="UTF-8">
    <!-- LATEST VERSION OF RENDERING ENGINE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!-- MOST IMPORTANT SEO FRIENDLY TAG -->
    <title>AO testmodule</title>
    <!-- OTHER IMPORTANT SEO TAGS -->
    <meta name="description" content="ArticulatieOnderzoek.">
    <meta name="keywords" content="ArticulatieOnderzoek">
    <meta name="author" content="Arteveldehogeschool | Bachelor in de Grafische en Digitale Media | Benjamin Lierman">
    <meta name="copyright" content="Copyright 2003-14 Arteveldehogeschool. Alle rechten voorbehouden.">
    <!-- HUMANS = AUTHORS TEXT FILE -->
    <link type="text/plain" rel="author" href="humans.txt">
    <!-- MOBILE VIEWPORT -->
    <meta name="viewport" content="width=device-width">
    <!-- Bootstrap -->
    {{ HTML::style("//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.2/css/bootstrap.css") }}
    {{ HTML::style("//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.2/css/bootstrap-theme.css") }}
    {{ HTML::style("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css") }}
            <!-- MODERNIZR: NEW HTML5 ELEMENTS + FEATURE DETECTION -->
    {{ HTML::script("scripts/components/modernizr/modernizr.custom.js") }}

            <!-- stylesheet -->
    {{ HTML::style("styles/css/main.css") }}
</head>
<body data-test-id="{{ $test->id }}">
<?php

//var_dump($moduleImage);

?>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
<!-- -->
<div id="sidebar-content"></div>
<div id="main-content">
    <div id="disable-interaction" style="position: absolute; width: 100%; height: 100%; z-index: 10; display: none;"></div>
    <div class="wrapper" id="display-wrapper">
        <div class="container">
            {{ HTML::image('uploads/images/'.$image->url, 'test', [
            'class' => 'image',
            'data-index' => $moduleImage->index,
            'data-image-id' => $image->id
            ])}}
        </div>
    </div>
    <div class="wrapper" id="toolbar-wrapper">
        <div class="container" id="toolbar-container">
            <div class="toolbar cs-col col-sc1-12">
                <div class="toolbar-left clearfix">
                    <div class="cs-btn disabled" id="btn-playback" title="afspelen [A]">
                        {{ HTML::image('/images/aspect-1-1.png', '', [
                        'class' => 'aspect-1-1'
                        ])}}
                        <div class="abs">
                            <canvas id="playback-progress" class="fragment-progress" width="0" height="0"></canvas>
                            <div class="outer-positioning">
                                <div class="inner-positioning">
                                    <i class="fa fa-lg fa-volume-up"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cs-btn disabled" id="btn-nazeggen" title="nazeggen [W]">
                        {{ HTML::image('/images/aspect-1-1.png', '', [
                        'class' => 'aspect-1-1'
                        ])}}
                        <div class="abs">
                            <canvas id="nazeggen-progress" class="fragment-progress" width="0" height="0"></canvas>
                            <div class="outer-positioning">
                                <div class="inner-positioning">
                                    <i class="fa fa-lg fa-comments"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cs-btn" id="btn-aanvulzin" title="aanvulzin [Z]">
                        {{ HTML::image('/images/aspect-1-1.png', '', [
                        'class' => 'aspect-1-1'
                        ])}}
                        <div class="abs">
                            <canvas id="aanvulzin-progress" class="fragment-progress" width="0" height="0"></canvas>
                            <div class="outer-positioning">
                                <div class="inner-positioning">
                                    <i class="fa fa-lg fa-comment"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cs-btn not-clickable" id="progress" title="voortgang">
                        {{ HTML::image('/images/aspect-1-1.png', '', [
                        'class' => 'aspect-1-1'
                        ])}}
                        <div class="abs">
                            <div class="outer-positioning">
                                <div class="inner-positioning">
                                    <div id="progress-counter">
                                            <span class="current">
                                                {{ $moduleImage->index }}
                                            </span>
                                            <span class="total">
                                                /{{ $totalImages }}
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cs-btn" id="btn-fs" title="fullscreen aan/uit [F]">
                        {{ HTML::image('/images/aspect-1-1.png', '', [
                        'class' => 'aspect-1-1'
                        ])}}
                        <div class="abs">
                            <div class="outer-positioning">
                                <div class="inner-positioning">
                                    <i class="fa fa-lg fa-expand"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cs-btn" id="btn-quit" title="stoppen [S]">
                        {{ HTML::image('/images/aspect-1-1.png', '', [
                        'class' => 'aspect-1-1'
                        ])}}
                        <a href="/#/test"><div class="abs">
                            <div class="outer-positioning">
                                <div class="inner-positioning">
                                    <i class="fa fa-lg fa-stop"></i>
                                </div>
                            </div>
                        </div>
                        </a>
                    </div>
                </div>
                <div class="toolbar-center">
                    <div class="cs-btn disabled" id="btn-record" title="opnemen/stoppen [SPATIE]">
                        {{ HTML::image('/images/aspect-1-1.png', '', [
                        'class' => 'aspect-1-1'
                        ])}}
                        <div id="audio-viz">
                            {{ HTML::image('/images/aspect-1-1.png', '', [
                            'class' => 'aspect-1-1'
                            ])}}
                            <div class="abs">
                                <canvas id="canvas-audio-viz"></canvas>
                            </div>
                        </div>
                        <div class="abs">
                            {{ HTML::image('/images/gradient-spinner.png', '', [
                                'class' => 'gradient-spinner spinning'
                            ])}}
                            <div class="outer-positioning">
                                <div class="inner-positioning">
                                    <i class="fa fa-2x fa-microphone bg-color"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="toolbar-right clearfix">
                    <div class="cs-btn disabled" id="btn-backward" title="vorige [LINKS]">
                        {{ HTML::image('/images/aspect-1-1.png', '', [
                        'class' => 'aspect-1-1'
                        ])}}
                        <div class="abs">
                            <div class="outer-positioning">
                                <div class="inner-positioning">
                                    <i class="fa fa-lg fa-backward"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cs-btn disabled" id="btn-wrong" title="fout [OMLAAG]">
                        {{ HTML::image('/images/aspect-1-1.png', '', [
                        'class' => 'aspect-1-1'
                        ])}}
                        <div class="abs">
                            <div class="outer-positioning">
                                <div class="inner-positioning">
                                    <i class="fa fa-lg fa-chevron-down"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cs-btn disabled" id="btn-unanswered" title="woord niet gezegd">
                        {{ HTML::image('/images/aspect-1-1.png', '', [
                        'class' => 'aspect-1-1'
                        ])}}
                        <div class="abs">
                            <div class="outer-positioning">
                                <div class="inner-positioning">
                                    <i class="fa fa-lg fa-trash-o"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cs-btn disabled" id="btn-correct" title="goed [OMHOOG]">
                        {{ HTML::image('/images/aspect-1-1.png', '', [
                        'class' => 'aspect-1-1'
                        ])}}
                        <div class="abs">
                            <div class="outer-positioning">
                                <div class="inner-positioning">
                                    <i class="fa fa-lg fa-chevron-up"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cs-btn disabled" id="btn-note" title="snelle analyse [TBD]">
                        {{ HTML::image('/images/aspect-1-1.png', '', [
                        'class' => 'aspect-1-1'
                        ])}}
                        <div class="abs">
                            <div class="outer-positioning">
                                <div class="inner-positioning">
                                    <i class="fa fa-lg fa-edit"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cs-btn disabled" id="btn-forward" title="volgende [RECHTS]">
                        {{ HTML::image('/images/aspect-1-1.png', '', [
                        'class' => 'aspect-1-1'
                        ])}}
                        <div class="abs">
                            <div class="outer-positioning">
                                <div class="inner-positioning">
                                    @if($moduleImage->index == $totalImages)
                                    <i class="fa fa-lg fa-step-forward"></i>
                                    @else
                                    <i class="fa fa-lg fa-forward"></i>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="notify-unanswered-wrapper">
    <div class="outer-positioning">
        <div class="inner-positioning">
            <div class="nu-container">
                <div class="nu-notification">3 afbeeldingen zijn niet benoemd.<br>Wenst u die te hernemen?</div>
                <div class="nu-buttons">
                    <div id="nu-yes">Ja</div>
                    <div id="nu-no">Nee</div>
                </div>
            </div>
        </div>
    </div>
</div>
<audio id="audio-playback">
    <source src="" type="audio/wav">
</audio>
<audio id="audio-voorzeggen">
    <source src="{{ url('uploads/words/' . $image->wordurl)}}" type="audio/wav">
</audio>
<audio id="audio-aanvulzin">
    <source src="{{ url('uploads/sentences/' . $image->sentenceurl)}}" type="audio/wav">
</audio>
<div id="error">
    <div id="error-table">
        <div id="error-icon">
            <i class="fa fa-times-circle"></i>
        </div>
        <div id="error-message">
            error
        </div>
    </div>
</div>

<!-- BOTTOM SCRIPTS -->
<!-- jQuery -->
{{ HTML::script("//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.js") }}
        <!--Bootstrap-->
{{ HTML::script("//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js") }}
{{ HTML::script("scripts/components/jquery-hotkeys.js") }}
{{ HTML::script("scripts/components/jquery.caret.js") }}
{{ HTML::script("scripts/components/mousetrap.min.js") }}
<!-- CUSTOM JAVASCRIPT FILES -->
{{ HTML::script("scripts/components/recorderjs/recorder.js") }}
{{ HTML::script("/scripts/phonetic/PhoneticDropDown.js") }}
{{ HTML::script("/scripts/other/modelTestImage.js") }}
{{ HTML::script("/scripts/other/RecordRTC.js") }}
{{ HTML::script("/scripts/other/ErrorData.js") }}
{{ HTML::script("/scripts/other/test-imagecontroller.js") }}
{{ HTML::script("/scripts/other/inTestAnalysisRendering.js") }}
{{ HTML::script("/scripts/other/InTestAnalysis.js") }}
{{ HTML::script("/scripts/other/test.js") }}
</body>
</html>