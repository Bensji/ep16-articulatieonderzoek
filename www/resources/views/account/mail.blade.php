<!DOCTYPE html>
<html lang="{{ Config::get('app.locale') }}" dir="ltr">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>Uw paswoord voor ao-r.be is gereset</h2>

    <div>Dit is uw nieuw paswoord: {{ $pass }}</div>

  </body>
</html>