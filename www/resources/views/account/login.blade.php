
@extends('layouts.master')

@section('body')

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-3 col-lg-offset-4 ">
                <div class="row">
                    <div class="col-xs-12">
                        {{ Form::open(['route' => 'login', 'class' => 'form-signin']), PHP_EOL }}
                        <h2 class="form-signin-heading"><span>Articulatie</span>Onderzoek</h2>

                        @if(Session::has('auth-error-message'))
                            <small class="error">
                                {{ Session::get('auth-error-message') }}
                            </small>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            {{ Form::email('email', '', [
                                'placeholder' 	=> 'Emailadres',
                                'class' 		=> 'form-control'
                                ]), PHP_EOL }}
                            @if (count($errors) > 0)
                                @if ($errors->has('email'))
                                    {{ $errors->first('email', '<small class="error">:message</small>') }}
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            {{ Form::password('password', [
                                'placeholder' 	=> 'Passwoord',
                                'class' 		=> 'form-control'
                                ]), PHP_EOL }}
                            @if (count($errors) > 0)
                                @if ($errors->has('password'))
                                    {{ $errors->first('password', '<small class="error">:message</small>') }}
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            {{ Form::checkbox('switch-auth', 'remember', false) }}
                            {{ Form::label( 'switch-auth' , 'Onthouden', ['class'  => 'remember']) }}
                            {{ HTML::link('/reset', 'Paswoord vergeten?', ['class' => 'right']), PHP_EOL }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            {{ Form::submit('Login', [
                                'class'	=> 'btn btn-default'
                                ]) , PHP_EOL}}
                            {{ HTML::link('/register', 'Registreren', ['class' => 'btn btn-default']), PHP_EOL }}
                        </div>
                    </div>
                    {{ Form::close() }}

                </div>
            </div>
        </div>

    </div>

@stop
