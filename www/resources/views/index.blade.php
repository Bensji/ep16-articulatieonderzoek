
@extends('layouts.master')

    @section('body')
        <body ng-app="aoApp" class="flat-blue">
        <div class="se-pre-con"></div>
        <div class="app-container">
            <div class="row content-container">
                <div ui-view>
                </div>
            <!-- jQuery -->
            {{ HTML::script("//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.js") }}
                    <!--Bootstrap-->
            {{ HTML::script("//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js") }}
                    <!-- Angular -->
            {{ HTML::script("scripts/lib/angular.js") }}
                    <!-- Lodash -->
            {{ HTML::script("scripts/lib/lodash.min.js") }}
                    <!-- App -->
            {{ HTML::script("scripts/js/app.js") }}

            <script>
                $("#menu-toggle").click(function(e) {
                    e.preventDefault();
                    $("#page-wrapper").toggleClass("active");
                });

                $(window).load(function() {
                    // Animate loader off screen
                    $(".se-pre-con").fadeOut("slow");
                });

            </script>
                </div>
            </div>
        </body>
@stop