<!doctype html>
<html lang="{{ Config::get('app.locale') }}" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ArticulatieOnderzoek</title>

    @section('head-styles')
            <!-- Bootstrap -->
    {{ HTML::style("//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.2/css/bootstrap.css") }}
    {{ HTML::style("//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.2/css/bootstrap-theme.css") }}
    {{ HTML::style("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css") }}

            <!-- stylesheet -->
    {{ HTML::style("styles/css/main.css") }}
    @show

</head>
<body>
<div id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li class="sidebar-head"><a href="#/home"><h3>ArticulatieOnderzoek</h3></a></a>
        </li>
    </ul>
</div>

<div id="page-wrapper">
    <div id="page-content-wrapper">
        <div class="page-content inset">
            <div class="row">
                @yield('body')
            </div>
        </div>
    </div>
</div>

@section('bottom-scripts')
            <!-- jQuery -->
    {{ HTML::script("//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.js") }}
            <!--Bootstrap-->
    {{ HTML::script("//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js") }}
            <!-- Angular -->
    {{ HTML::script("scripts/lib/angular.js") }}
            <!-- Lodash -->
    {{ HTML::script("scripts/lib/lodash.min.js") }}
            <!-- App -->
    {{ HTML::script("scripts/js/app.js") }}
    @show

</body>
</html>
