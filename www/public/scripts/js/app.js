/*
*
*   Auteur: Benjamin Lierman
*   Datum: 29/02/2026
*   contact: benjamin@keidigitaal.be
*
*/
// <reference path="_referencepaths.ts"/>
(function () {
    "use strict";
    angular.module("aoApp", [
        "ui.router",
        "ngRoute",
        "ngSanitize",
        "ngResource",
        "aoControllers",
        "aoServices",
        'angularUtils.directives.dirPagination'
    ]);
    angular.module("aoControllers", []);
    angular.module("aoServices", []);
})();

/*
 *
 *   Auteur: Benjamin Lierman
 *   Datum: 29/02/2026
 *   contact: benjamin@keidigitaal.be
 *
 */
// <reference path="_referencepaths.ts"/>
(function () {
    angular
        .module('aoApp')
        .config(configureMainRoutes);
    configureMainRoutes.$inject = [
        '$stateProvider',
        '$urlRouterProvider'
    ];
    /* @ngInject */
    function configureMainRoutes($stateProvider, $urlRouterProvider) {
        // When nothing is specified, go to the login url
        $urlRouterProvider.when('', '/auth');
        // When trying to go to a route that does not exists show a 404
        // Why is this going crazy?!
        //$urlRouterProvider.otherwise('/404');
        // Public routes
        $stateProvider
            .state('auth', {
            url: '/auth',
            templateUrl: '../../templates/auth/auth.view.html',
            controller: 'AuthController as auth'
        })
            .state('register', {
            url: '/register',
            templateUrl: '../../templates/auth/register.view.html',
            controller: 'AuthController as auth'
        })
            .state('reset', {
            url: '/reset',
            templateUrl: '../../templates/auth/reset.view.html',
            controller: 'AuthController as auth'
        })
            .state('404', {
            url: '/404',
            templateUrl: '../../templates/common/404.view.html',
            controller: 'AuthController as auth'
        })
            .state('home', {
            url: '/home',
            templateUrl: '../../templates/home/home.view.html',
            controller: 'HomeController as home',
            resolve: {
                'auth': function (AuthService) {
                    return AuthService.authenticate();
                }
            }
        })
            .state('test', {
            url: '/test',
            templateUrl: '../../templates/test/test-index.view.html',
            controller: 'indexTestController as test',
            resolve: {
                'auth': function (AuthService) {
                    return AuthService.authenticate();
                }
            }
        })
            .state('child', {
            url: '/child',
            templateUrl: '../../templates/child/child-index.view.html',
            controller: 'indexChildController as child',
            resolve: {
                'auth': function (AuthService) {
                    return AuthService.authenticate();
                }
            }
        })
            .state('child/create', {
            url: '/child/create',
            templateUrl: '../../templates/child/child-create.view.html',
            controller: 'createChildController as child',
            resolve: {
                'auth': function (AuthService) {
                    return AuthService.authenticate();
                }
            }
        })
            .state('child/edit', {
            url: '/child/edit/{id}',
            templateUrl: '../../templates/child/child-edit.view.html',
            controller: 'editChildController as child',
            resolve: {
                'auth': function (AuthService) {
                    return AuthService.authenticate();
                }
            }
        })
            .state('child/show', {
            url: '/child/show/{id}',
            templateUrl: '../../templates/child/child-show.view.html',
            controller: 'showChildController as child',
            resolve: {
                'auth': function (AuthService) {
                    return AuthService.authenticate();
                }
            }
        })
            .state('admin', {
            url: '/admin',
            templateUrl: '../../templates/admin/admin.view.html',
            controller: 'AdminController as admin',
            resolve: {
                'admin': function (AuthService) {
                    return AuthService.authenticateAdmin();
                }
            }
        })
            .state('admin/user/show', {
            url: '/admin/user/show/{id}',
            templateUrl: '../../templates/admin/user/user-show.view.html',
            controller: 'showUserAdminController as admin',
            resolve: {
                'admin': function (AuthService) {
                    return AuthService.authenticateAdmin();
                }
            }
        })
            .state('admin/user/edit', {
            url: '/admin/user/edit/{id}',
            templateUrl: '../../templates/admin/user/user-edit.view.html',
            controller: 'editUserAdminController as admin',
            resolve: {
                'admin': function (AuthService) {
                    return AuthService.authenticateAdmin();
                }
            }
        })
            .state('admin/user', {
            url: '/admin/user',
            templateUrl: '../../templates/admin/user/user-index.view.html',
            controller: 'indexUserAdminController as admin',
            resolve: {
                'admin': function (AuthService) {
                    return AuthService.authenticateAdmin();
                }
            }
        })
            .state('admin/user/create', {
            url: '/admin/user/create',
            templateUrl: '../../templates/admin/user/user-create.view.html',
            controller: 'createUserAdminController as admin',
            resolve: {
                'admin': function (AuthService) {
                    return AuthService.authenticateAdmin();
                }
            }
        })
            .state('admin/child', {
            url: '/admin/child',
            templateUrl: '../../templates/admin/child/child-index.view.html',
            controller: 'indexChildAdminController as admin',
            resolve: {
                'admin': function (AuthService) {
                    return AuthService.authenticateAdmin();
                }
            }
        })
            .state('admin/child/show', {
            url: '/admin/child/show/{id}',
            templateUrl: '../../templates/admin/child/child-show.view.html',
            controller: 'showChildAdminController as admin',
            resolve: {
                'admin': function (AuthService) {
                    return AuthService.authenticateAdmin();
                }
            }
        })
            .state('admin/child/edit', {
            url: '/admin/child/edit/{id}',
            templateUrl: '../../templates/admin/child/child-edit.view.html',
            controller: 'editChildAdminController as admin',
            resolve: {
                'admin': function (AuthService) {
                    return AuthService.authenticateAdmin();
                }
            }
        })
            .state('admin/child/create', {
            url: '/admin/child/create',
            templateUrl: '../../templates/admin/child/child-create.view.html',
            controller: 'createChildAdminController as admin',
            resolve: {
                'admin': function (AuthService) {
                    return AuthService.authenticateAdmin();
                }
            }
        });
    }
})();

// <reference path="../_referencepaths.ts"/>
var aoControllers;
(function (aoControllers) {
    'use strict';
    var AdminController = (function () {
        function AdminController(adminUserService, $location) {
            this.adminUserService = adminUserService;
            this.$location = $location;
            this.indexAdminUserCtrl();
        }
        AdminController.prototype.indexAdminUserCtrl = function () {
            var _this = this;
            this.adminUserService.indexUserDataServ()
                .then(function (data) {
                if (data["error"] != null) {
                    _this.errorMessage = data["error"];
                }
                else {
                    console.log(data);
                    _this.newUsersData = data;
                }
            });
        };
        AdminController.prototype.deleteUserCtrl = function (id) {
            var _this = this;
            this.adminUserService.deleteUserData(id)
                .then(function (data) {
                if (data["data"]["error"] != null) {
                    console.log("neuwp");
                    _this.errorMessage = data["data"]["error"];
                }
                else {
                    console.log(data);
                    _this.succesMessage = data["data"]["succes"];
                    _this.indexAdminUserCtrl();
                }
            });
        };
        AdminController.prototype.lockUserCtrl = function (id) {
            var _this = this;
            this.adminUserService.lockUserData(id)
                .then(function (data) {
                if (data["data"]["error"] != null) {
                    _this.errorMessage = data["data"]["error"];
                }
                else {
                    console.log(data);
                    _this.indexAdminUserCtrl();
                    _this.succesMessage = data["data"]["succes"];
                }
            });
        };
        AdminController.prototype.unlockUserCtrl = function (id) {
            var _this = this;
            this.adminUserService.unlockUserData(id)
                .then(function (data) {
                if (data["data"]["error"] != null) {
                    _this.errorMessage = data["data"]["error"];
                }
                else {
                    _this.indexAdminUserCtrl();
                    _this.succesMessage = data["data"]["succes"];
                }
            });
        };
        AdminController.$inject = [
            'adminUserService',
            '$location'
        ];
        return AdminController;
    }());
    aoControllers.AdminController = AdminController;
    angular.module('aoControllers')
        .controller('AdminController', AdminController);
})(aoControllers || (aoControllers = {}));

// <reference path="../_referencepaths.ts"/>
var aoServices;
(function (aoServices) {
    'use strict';
    var AdminService = (function () {
        function AdminService($http) {
            this.$http = $http;
        }
        AdminService.prototype.getNewUsersServ = function () {
            return this.$http.get('/admin/');
        };
        AdminService.$inject = [
            '$http'
        ];
        return AdminService;
    }());
    aoServices.AdminService = AdminService;
    angular.module('aoServices')
        .service('AdminService', AdminService);
})(aoServices || (aoServices = {}));

// <reference path="../_referencepaths.ts"/>
var aoServices;
(function (aoServices) {
    'use strict';
    var adminChildService = (function () {
        function adminChildService($http) {
            this.$http = $http;
        }
        adminChildService.prototype.getUsers = function () {
            return this.$http.get('/admin/user/');
        };
        adminChildService.prototype.getChildDataServ = function () {
            return this.$http.get('/admin/child/');
        };
        adminChildService.prototype.showChildServ = function (id) {
            return this.$http.get('/admin/child/' + id);
        };
        adminChildService.prototype.editChildDataServ = function (id, editChildData) {
            console.log(editChildData);
            return this.$http({
                method: 'PATCH',
                url: '/admin/child/' + id,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param(editChildData)
            });
        };
        adminChildService.prototype.deleteChildServ = function (id) {
            return this.$http.delete('/admin/child/' + id);
        };
        adminChildService.prototype.createChildServ = function (createChildData) {
            console.log(createChildData);
            return this.$http({
                method: 'POST',
                url: '/admin/child/',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param(createChildData)
            });
        };
        adminChildService.$inject = [
            '$http'
        ];
        return adminChildService;
    }());
    aoServices.adminChildService = adminChildService;
    angular.module('aoServices')
        .service('adminChildService', adminChildService);
})(aoServices || (aoServices = {}));

// <reference path="../../_referencepaths.ts"/>
var aoControllers;
(function (aoControllers) {
    'use strict';
    var createChildAdminController = (function () {
        function createChildAdminController(adminChildService, $location) {
            this.adminChildService = adminChildService;
            this.$location = $location;
            this.getUsersCtrl();
        }
        createChildAdminController.prototype.getUsersCtrl = function () {
            var _this = this;
            this.adminChildService.getUsers()
                .then(function (data) {
                console.log(data);
                _this.users = data["data"]["users"];
                _this.defaultUser = { id: data["data"]["users"][0]["id"], value: data["data"]["users"][0]["name"] };
            });
        };
        createChildAdminController.prototype.createChildCtrl = function () {
            var _this = this;
            this.createChildData = {
                name: this.name,
                givenname: this.givenname,
                email: this.email,
                street: this.street,
                streetnumber: this.streetnumber,
                city: this.city,
                postalcode: this.postalcode,
                initials: this.initials,
                birthdate: this.birthdate,
                sex: this.sex,
                user_id: this.defaultUser.id
            };
            this.adminChildService.createChildServ(this.createChildData)
                .success(function (data) {
                if (data["succes"] != null) {
                    _this.succesMessage = data["succes"];
                    _this.$location.path('/admin/child');
                }
                else {
                    _this.errorMessage = data;
                }
            })
                .error(function (data) {
                _this.errorMessage = data;
                console.log(data);
            });
        };
        createChildAdminController.$inject = [
            'adminChildService',
            '$location'
        ];
        return createChildAdminController;
    }());
    aoControllers.createChildAdminController = createChildAdminController;
    angular.module('aoControllers')
        .controller('createChildAdminController', createChildAdminController);
})(aoControllers || (aoControllers = {}));

// <reference path="../../_referencepaths.ts"/>
var aoControllers;
(function (aoControllers) {
    'use strict';
    var editChildAdminController = (function () {
        function editChildAdminController(adminChildService, $location, $stateParams) {
            this.adminChildService = adminChildService;
            this.$location = $location;
            this.$stateParams = $stateParams;
            this.bindChildCtrl($stateParams["id"]);
        }
        editChildAdminController.prototype.bindChildCtrl = function (id) {
            var _this = this;
            this.adminChildService.showChildServ(id)
                .success(function (data) {
                console.log(data);
                _this.name = data["child"]["name"];
                _this.givenname = data["child"]["givenname"];
                _this.email = data["child"]["email"];
                _this.street = data["child"]["street"];
                _this.streetnumber = parseFloat(data["child"]["streetnumber"]);
                _this.city = data["child"]["city"];
                _this.postalcode = parseFloat(data["child"]["postalcode"]);
                _this.initials = data["child"]["initials"];
                _this.birthdate = data["child"]["birthdate"];
                _this.sex = data["child"]["sex"];
                _this.dropdownuser = data["users"];
                _this.thisuser = { id: data["child"]["user"]["id"], value: data["child"]["user"]["givenname"] };
            })
                .error(function (data) {
                _this.errorMessage = data;
            });
        };
        editChildAdminController.prototype.editChildCtrl = function (id) {
            var _this = this;
            this.editChildData = {
                name: this.name,
                givenname: this.givenname,
                email: this.email,
                street: this.street,
                streetnumber: this.streetnumber,
                city: this.city,
                postalcode: this.postalcode,
                initials: this.initials,
                birthdate: this.birthdate,
                sex: this.sex,
                user_id: this.thisuser.id
            };
            this.adminChildService.editChildDataServ(this.$stateParams["id"], this.editChildData)
                .success(function (data) {
                console.log(data);
                _this.succesMessage = data["succes"];
                _this.$location.path('/admin/child');
            })
                .error(function (data) {
                console.log(data);
                console.log("error");
                _this.errorMessage = data;
            });
        };
        editChildAdminController.$inject = [
            'adminChildService',
            '$location',
            '$stateParams'
        ];
        return editChildAdminController;
    }());
    aoControllers.editChildAdminController = editChildAdminController;
    angular.module('aoControllers')
        .controller('editChildAdminController', editChildAdminController);
})(aoControllers || (aoControllers = {}));

// <reference path="../../_referencepaths.ts"/>
var aoControllers;
(function (aoControllers) {
    'use strict';
    var indexChildAdminController = (function () {
        function indexChildAdminController(adminChildService, $location) {
            this.adminChildService = adminChildService;
            this.$location = $location;
            this.currentPage = 1;
            this.pageSize = 10;
            this.getChildDataCtrl();
        }
        indexChildAdminController.prototype.getChildDataCtrl = function () {
            var _this = this;
            this.adminChildService.getChildDataServ()
                .then(function (data) {
                if (data["error"] != null) {
                    _this.errorMessage = data["error"];
                }
                else {
                    _this.childData = data;
                }
            });
        };
        indexChildAdminController.prototype.deleteChildCtrl = function (id) {
            var _this = this;
            this.adminChildService.deleteChildServ(id)
                .then(function (data) {
                if (data["error"] != null) {
                    _this.errorMessage = "Kind is niet verwijderd.";
                }
                else {
                    _this.succesMessage = "Kind is verwijderd.";
                    _this.getChildDataCtrl();
                }
            });
        };
        indexChildAdminController.$inject = [
            'adminChildService',
            '$location'
        ];
        return indexChildAdminController;
    }());
    aoControllers.indexChildAdminController = indexChildAdminController;
    angular.module('aoControllers')
        .controller('indexChildAdminController', indexChildAdminController);
})(aoControllers || (aoControllers = {}));

// <reference path="../../_referencepaths.ts"/>
var aoControllers;
(function (aoControllers) {
    'use strict';
    var showChildAdminController = (function () {
        function showChildAdminController(adminChildService, $location, $stateParams) {
            this.adminChildService = adminChildService;
            this.$location = $location;
            this.$stateParams = $stateParams;
            this.showChildCtrl($stateParams["id"]);
        }
        showChildAdminController.prototype.showChildCtrl = function (id) {
            var _this = this;
            this.adminChildService.showChildServ(id)
                .then(function (data) {
                if (data["error"] != null) {
                    _this.errorMessage = data["error"];
                }
                else {
                    _this.singleChildData = data;
                }
            });
        };
        showChildAdminController.$inject = [
            'adminChildService',
            '$location',
            '$stateParams'
        ];
        return showChildAdminController;
    }());
    aoControllers.showChildAdminController = showChildAdminController;
    angular.module('aoControllers')
        .controller('showChildAdminController', showChildAdminController);
})(aoControllers || (aoControllers = {}));

// <reference path="../_referencepaths.ts"/>
var aoServices;
(function (aoServices) {
    'use strict';
    var adminUserService = (function () {
        function adminUserService($http) {
            this.$http = $http;
        }
        adminUserService.prototype.indexUserDataServ = function () {
            return this.$http.get('/admin/');
        };
        adminUserService.prototype.indexAllUserDataServ = function () {
            return this.$http.get('/admin/user/');
        };
        adminUserService.prototype.showUserDataServ = function (id) {
            return this.$http.get('/admin/user/' + id);
        };
        adminUserService.prototype.createUserDataServ = function (userCreateData) {
            console.log(userCreateData);
            return this.$http({
                method: 'POST',
                url: '/admin/user/',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param(userCreateData)
            });
        };
        adminUserService.prototype.editUserDataServ = function (id, userEditData) {
            return this.$http({
                method: 'PATCH',
                url: '/admin/user/' + id,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param(userEditData)
            });
        };
        adminUserService.prototype.deleteUserData = function (id) {
            return this.$http.delete('/admin/user/' + id);
        };
        adminUserService.prototype.lockUserData = function (id) {
            return this.$http({
                method: 'PUT',
                url: '/admin/user/lock/' + id,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            });
            //return this.$http.put('/admin/user/lock/' + id);
        };
        adminUserService.prototype.unlockUserData = function (id) {
            return this.$http({
                method: 'PUT',
                url: '/admin/user/unlock/' + id,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            });
        };
        adminUserService.prototype.getUserRoles = function () {
            return this.$http.get('/admin/getroles');
        };
        adminUserService.$inject = [
            '$http'
        ];
        return adminUserService;
    }());
    aoServices.adminUserService = adminUserService;
    angular.module('aoServices')
        .service('adminUserService', adminUserService);
})(aoServices || (aoServices = {}));

// <reference path="../../_referencepaths.ts"/>
var aoControllers;
(function (aoControllers) {
    'use strict';
    var createUserAdminController = (function () {
        function createUserAdminController(adminUserService, $location) {
            this.adminUserService = adminUserService;
            this.$location = $location;
            this.getUserRoles();
        }
        createUserAdminController.prototype.getUserRoles = function () {
            var _this = this;
            this.adminUserService.getUserRoles()
                .then(function (data) {
                _this.userRoles = data["data"]["roles"];
                _this.defaultUserRole = { id: data["data"]["roles"][0]["id"], value: data["data"]["roles"][0]["name"] };
            });
        };
        createUserAdminController.prototype.createUserCtrl = function () {
            var _this = this;
            this.createUserData = {
                name: this.name,
                givenname: this.givenname,
                email: this.email,
                street: this.street,
                streetnumber: this.streetnumber,
                city: this.city,
                postalcode: this.postalcode,
                password: this.password,
                password_repeat: this.password_repeat,
                role_id: this.defaultUserRole.id
            };
            this.adminUserService.createUserDataServ(this.createUserData)
                .success(function (data) {
                if (data["succes"] != null) {
                    _this.succesMessage = data["succes"];
                    _this.$location.path('/admin/user');
                }
                else {
                    _this.errorMessage = data;
                }
            })
                .error(function (data) {
                _this.errorMessage = data;
            });
        };
        createUserAdminController.$inject = [
            'adminUserService',
            '$location'
        ];
        return createUserAdminController;
    }());
    aoControllers.createUserAdminController = createUserAdminController;
    angular.module('aoControllers')
        .controller('createUserAdminController', createUserAdminController);
})(aoControllers || (aoControllers = {}));

// <reference path="../../_referencepaths.ts"/>
var aoControllers;
(function (aoControllers) {
    'use strict';
    var editUserAdminController = (function () {
        function editUserAdminController(adminUserService, $location, $stateParams) {
            this.adminUserService = adminUserService;
            this.$location = $location;
            this.$stateParams = $stateParams;
            this.bindUserCtrl($stateParams["id"]);
        }
        editUserAdminController.prototype.bindUserCtrl = function (id) {
            var _this = this;
            this.adminUserService.showUserDataServ(id)
                .success(function (data) {
                console.log(data);
                _this.name = data["user"]["name"];
                _this.givenname = data["user"]["givenname"];
                _this.email = data["user"]["email"];
                _this.street = data["user"]["street"];
                _this.streetnumber = parseFloat(data["user"]["streetnumber"]);
                _this.city = data["user"]["city"];
                _this.postalcode = parseFloat(data["user"]["postalcode"]);
                _this.userrole = { id: data["user"]["role"]["id"], name: data["user"]["role"]["name"] };
                _this.dropdown = data["role"];
            })
                .error(function (data) {
                _this.errorMessage = data;
            });
        };
        editUserAdminController.prototype.editUserCtrl = function (id) {
            var _this = this;
            this.editUserData = {
                name: this.name,
                givenname: this.givenname,
                email: this.email,
                street: this.street,
                streetnumber: this.streetnumber,
                city: this.city,
                postalcode: this.postalcode,
                role_id: this.userrole.id
            };
            this.adminUserService.editUserDataServ(this.$stateParams["id"], this.editUserData)
                .success(function (data) {
                _this.succesMessage = data["succes"];
                _this.$location.path('/admin');
            })
                .error(function (data) {
                _this.errorMessage = data;
            });
        };
        editUserAdminController.$inject = [
            'adminUserService',
            '$location',
            '$stateParams'
        ];
        return editUserAdminController;
    }());
    aoControllers.editUserAdminController = editUserAdminController;
    angular.module('aoControllers')
        .controller('editUserAdminController', editUserAdminController);
})(aoControllers || (aoControllers = {}));

// <reference path="../../_referencepaths.ts"/>
var aoControllers;
(function (aoControllers) {
    'use strict';
    var indexUserAdminController = (function () {
        function indexUserAdminController(adminUserService, $location, $stateParams) {
            this.adminUserService = adminUserService;
            this.$location = $location;
            this.$stateParams = $stateParams;
            this.currentPage = 1;
            this.pageSize = 10;
            this.indexAdminUserCtrl();
        }
        indexUserAdminController.prototype.indexAdminUserCtrl = function () {
            var _this = this;
            this.adminUserService.indexAllUserDataServ()
                .then(function (data) {
                if (data["data"]["error"] != null) {
                    _this.errorMessage = data["data"]["error"];
                }
                else {
                    console.log(data);
                    _this.newUsersData = data;
                }
            });
        };
        indexUserAdminController.prototype.deleteUserCtrl = function (id) {
            var _this = this;
            this.adminUserService.deleteUserData(id)
                .then(function (data) {
                if (data["data"]["error"] != null) {
                    console.log("neuwp");
                    _this.errorMessage = data["data"]["error"];
                }
                else {
                    console.log(data);
                    _this.succesMessage = data["data"]["succes"];
                    _this.indexAdminUserCtrl();
                }
            });
        };
        indexUserAdminController.prototype.lockUserCtrl = function (id) {
            var _this = this;
            this.adminUserService.lockUserData(id)
                .then(function (data) {
                if (data["data"]["error"] != null) {
                    _this.errorMessage = data["data"]["error"];
                }
                else {
                    console.log(data);
                    _this.indexAdminUserCtrl();
                    _this.succesMessage = data["data"]["succes"];
                }
            });
        };
        indexUserAdminController.prototype.unlockUserCtrl = function (id) {
            var _this = this;
            this.adminUserService.unlockUserData(id)
                .then(function (data) {
                if (data["data"]["error"] != null) {
                    _this.errorMessage = data["data"]["error"];
                }
                else {
                    _this.indexAdminUserCtrl();
                    _this.succesMessage = data["data"]["succes"];
                }
            });
        };
        indexUserAdminController.$inject = [
            'adminUserService',
            '$location',
            '$stateParams'
        ];
        return indexUserAdminController;
    }());
    aoControllers.indexUserAdminController = indexUserAdminController;
    angular.module('aoControllers')
        .controller('indexUserAdminController', indexUserAdminController);
})(aoControllers || (aoControllers = {}));

// <reference path="../../_referencepaths.ts"/>
var aoControllers;
(function (aoControllers) {
    'use strict';
    var showUserAdminController = (function () {
        function showUserAdminController(adminUserService, $location, $stateParams) {
            this.adminUserService = adminUserService;
            this.$location = $location;
            this.$stateParams = $stateParams;
            this.showAdminUserCtrl($stateParams["id"]);
        }
        showUserAdminController.prototype.showAdminUserCtrl = function (id) {
            var _this = this;
            this.adminUserService.showUserDataServ(id)
                .then(function (data) {
                if (data["error"] != null) {
                    _this.errorMessage = data["error"];
                }
                else {
                    console.log(data);
                    _this.singleUserData = data;
                }
            });
        };
        showUserAdminController.$inject = [
            'adminUserService',
            '$location',
            '$stateParams'
        ];
        return showUserAdminController;
    }());
    aoControllers.showUserAdminController = showUserAdminController;
    angular.module('aoControllers')
        .controller('showUserAdminController', showUserAdminController);
})(aoControllers || (aoControllers = {}));

// <reference path="../_referencepaths.ts"/>
var aoServices;
(function (aoServices) {
    'use strict';
    var ChildService = (function () {
        function ChildService($http) {
            this.$http = $http;
        }
        ChildService.prototype.getChildDataServ = function () {
            return this.$http.get('/child/');
        };
        ChildService.prototype.createChildServ = function (childData) {
            return this.$http({
                method: 'POST',
                url: '/child/',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param(childData)
            });
        };
        ChildService.prototype.editChildServ = function (id, childEditData) {
            return this.$http({
                method: 'PATCH',
                url: '/child/' + id,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param(childEditData)
            });
        };
        ChildService.prototype.showChildServ = function (id) {
            return this.$http.get('/child/' + id);
        };
        ChildService.prototype.deleteChildServ = function (id) {
            return this.$http.delete('/child/' + id);
        };
        ChildService.$inject = [
            '$http'
        ];
        return ChildService;
    }());
    aoServices.ChildService = ChildService;
    angular.module('aoServices')
        .service('ChildService', ChildService);
})(aoServices || (aoServices = {}));

// <reference path="../../_referencepaths.ts"/>
var aoControllers;
(function (aoControllers) {
    'use strict';
    var createChildController = (function () {
        function createChildController(ChildService, $location) {
            this.ChildService = ChildService;
            this.$location = $location;
        }
        createChildController.prototype.createChildCtrl = function () {
            var _this = this;
            this.createChildData = {
                name: this.name,
                givenname: this.givenname,
                email: this.email,
                street: this.street,
                streetnumber: this.streetnumber,
                city: this.city,
                postalcode: this.postalcode,
                initials: this.initials,
                birthdate: this.birthdate,
                sex: this.sex
            };
            this.ChildService.createChildServ(this.createChildData)
                .success(function (data) {
                if (data["succes"] != null) {
                    _this.succesMessage = data["succes"];
                    _this.$location.path('/child');
                }
                else {
                    _this.errorMessage = data;
                }
            })
                .error(function (data) {
                _this.errorMessage = data;
                console.log(data);
            });
        };
        createChildController.$inject = [
            'ChildService',
            '$location'
        ];
        return createChildController;
    }());
    aoControllers.createChildController = createChildController;
    angular.module('aoControllers')
        .controller('createChildController', createChildController);
})(aoControllers || (aoControllers = {}));

// <reference path="../../_referencepaths.ts"/>
var aoControllers;
(function (aoControllers) {
    'use strict';
    var editChildController = (function () {
        function editChildController(ChildService, $location, $stateParams) {
            this.ChildService = ChildService;
            this.$location = $location;
            this.$stateParams = $stateParams;
            this.bindChildCtrl($stateParams["id"]);
        }
        editChildController.prototype.bindChildCtrl = function (id) {
            var _this = this;
            this.ChildService.showChildServ(id)
                .success(function (data) {
                _this.name = data["child"]["name"];
                _this.givenname = data["child"]["givenname"];
                _this.email = data["child"]["email"];
                _this.street = data["child"]["street"];
                _this.streetnumber = parseFloat(data["child"]["streetnumber"]);
                _this.city = data["child"]["city"];
                _this.postalcode = parseFloat(data["child"]["postalcode"]);
                _this.initials = data["child"]["initials"];
                _this.birthdate = data["child"]["birthdate"];
                _this.sex = data["child"]["sex"];
            })
                .error(function (data) {
                _this.errorMessage = data;
            });
        };
        editChildController.prototype.editChildCtrl = function (id) {
            var _this = this;
            this.editChildData = {
                name: this.name,
                givenname: this.givenname,
                email: this.email,
                street: this.street,
                streetnumber: this.streetnumber,
                city: this.city,
                postalcode: this.postalcode,
                initials: this.initials,
                birthdate: this.birthdate,
                sex: this.sex
            };
            this.ChildService.editChildServ(this.$stateParams["id"], this.editChildData)
                .success(function (data) {
                _this.succesMessage = data["succes"];
                _this.$location.path('/child');
            })
                .error(function (data) {
                _this.errorMessage = data;
            });
        };
        editChildController.$inject = [
            'ChildService',
            '$location',
            '$stateParams'
        ];
        return editChildController;
    }());
    aoControllers.editChildController = editChildController;
    angular.module('aoControllers')
        .controller('editChildController', editChildController);
})(aoControllers || (aoControllers = {}));

// <reference path="../../_referencepaths.ts"/>
var aoControllers;
(function (aoControllers) {
    'use strict';
    var indexChildController = (function () {
        function indexChildController(ChildService, $location) {
            this.ChildService = ChildService;
            this.$location = $location;
            this.currentPage = 1;
            this.pageSize = 10;
            this.getChildDataCtrl();
        }
        indexChildController.prototype.getChildDataCtrl = function () {
            var _this = this;
            this.ChildService.getChildDataServ()
                .then(function (data) {
                if (data["error"] != null) {
                    _this.errorMessage = data["error"];
                }
                else {
                    _this.childData = data;
                }
            });
        };
        indexChildController.prototype.deleteChildCtrl = function (id) {
            var _this = this;
            this.ChildService.deleteChildServ(id)
                .then(function (data) {
                if (data["error"] != null) {
                    _this.errorMessage = "Kind is niet verwijderd.";
                }
                else {
                    _this.succesMessage = "Kind is verwijderd.";
                    _this.getChildDataCtrl();
                }
            });
        };
        indexChildController.$inject = [
            'ChildService',
            '$location'
        ];
        return indexChildController;
    }());
    aoControllers.indexChildController = indexChildController;
    angular.module('aoControllers')
        .controller('indexChildController', indexChildController);
})(aoControllers || (aoControllers = {}));

// <reference path="../../_referencepaths.ts"/>
var aoControllers;
(function (aoControllers) {
    'use strict';
    var showChildController = (function () {
        function showChildController(ChildService, $location, $stateParams) {
            this.ChildService = ChildService;
            this.$location = $location;
            this.$stateParams = $stateParams;
            this.showChildCtrl($stateParams["id"]);
        }
        showChildController.prototype.showChildCtrl = function (id) {
            var _this = this;
            this.ChildService.showChildServ(id)
                .then(function (data) {
                if (data["error"] != null) {
                    _this.errorMessage = data["error"];
                }
                else {
                    _this.singleChildData = data;
                }
            });
        };
        showChildController.$inject = [
            'ChildService',
            '$location',
            '$stateParams'
        ];
        return showChildController;
    }());
    aoControllers.showChildController = showChildController;
    angular.module('aoControllers')
        .controller('showChildController', showChildController);
})(aoControllers || (aoControllers = {}));

// <reference path="../_referencepaths.ts"/>
var aoControllers;
(function (aoControllers) {
    'use strict';
    var AppController = (function () {
        function AppController(AuthService, $location) {
            this.AuthService = AuthService;
            this.$location = $location;
            this.loggedInUser();
        }
        AppController.prototype.loggedInUser = function () {
            var _this = this;
            this.AuthService.authenticate()
                .then(function (data) {
                _this.loggedInUserData = data;
                console.log(data);
            });
        };
        AppController.prototype.logout = function () {
            var _this = this;
            this.AuthService.logoutUser()
                .success(function (data) {
                if (data["succes"] != null) {
                    _this.$location.path('/auth');
                }
                else {
                    console.log(data);
                }
            });
        };
        AppController.$inject = [
            'AuthService',
            '$location'
        ];
        return AppController;
    }());
    aoControllers.AppController = AppController;
    angular.module('aoControllers')
        .controller('AppController', AppController);
})(aoControllers || (aoControllers = {}));

// <reference path="../_referencepaths.ts"/>
var aoServices;
(function (aoServices) {
    'use strict';
    var HomeService = (function () {
        function HomeService($http) {
            this.$http = $http;
        }
        HomeService.prototype.getHomeDataServ = function () {
            return this.$http.get('/home/');
        };
        HomeService.$inject = [
            '$http'
        ];
        return HomeService;
    }());
    aoServices.HomeService = HomeService;
    angular.module('aoServices')
        .service('HomeService', HomeService);
})(aoServices || (aoServices = {}));

// <reference path="../_referencepaths.ts"/>
var aoControllers;
(function (aoControllers) {
    'use strict';
    var HomeController = (function () {
        function HomeController(HomeService) {
            this.HomeService = HomeService;
            this.getHomeDataCtrl();
        }
        HomeController.prototype.getHomeDataCtrl = function () {
            var _this = this;
            this.HomeService.getHomeDataServ()
                .then(function (data) {
                _this.homeData = data;
                console.log(_this.homeData);
            });
        };
        HomeController.$inject = [
            'HomeService'
        ];
        return HomeController;
    }());
    aoControllers.HomeController = HomeController;
    angular.module('aoControllers')
        .controller('HomeController', HomeController);
})(aoControllers || (aoControllers = {}));

// <reference path="../_referencepaths.ts"/>
var aoServices;
(function (aoServices) {
    'use strict';
    var TestService = (function () {
        function TestService($http) {
            this.$http = $http;
        }
        TestService.prototype.getTestDataServ = function () {
            return this.$http.get('/test/');
        };
        TestService.prototype.createTestServ = function (childData) {
            return this.$http({
                method: 'POST',
                url: '/test/',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param(childData)
            });
        };
        TestService.prototype.editTestServ = function (id, childEditData) {
            return this.$http({
                method: 'PATCH',
                url: '/test/' + id,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param(childEditData)
            });
        };
        TestService.prototype.showTestServ = function (id) {
            return this.$http.get('/test/' + id);
        };
        TestService.prototype.deleteTestServ = function (id) {
            return this.$http.delete('/test/' + id);
        };
        TestService.$inject = [
            '$http'
        ];
        return TestService;
    }());
    aoServices.TestService = TestService;
    angular.module('aoServices')
        .service('TestService', TestService);
})(aoServices || (aoServices = {}));

// <reference path="../../_referencepaths.ts"/>
var aoControllers;
(function (aoControllers) {
    'use strict';
    var indexTestController = (function () {
        function indexTestController(TestService, $location) {
            this.TestService = TestService;
            this.$location = $location;
            this.currentPage = 1;
            this.pageSize = 10;
            this.getTestDataCtrl();
        }
        indexTestController.prototype.getTestDataCtrl = function () {
            var _this = this;
            this.TestService.getTestDataServ()
                .then(function (data) {
                if (data["error"] != null) {
                    console.log(data);
                    _this.errorMessage = data["error"];
                }
                else {
                    console.log(data);
                    _this.testData = data;
                }
            });
        };
        indexTestController.prototype.deleteTestCtrl = function (id) {
            var _this = this;
            this.TestService.deleteTestServ(id)
                .then(function (data) {
                if (data["error"] != null) {
                    _this.errorMessage = "Test is niet verwijderd.";
                }
                else {
                    _this.succesMessage = "Test is verwijderd.";
                    _this.getTestDataCtrl();
                }
            });
        };
        indexTestController.prototype.pageChangeHandler = function (number) {
            console.log(number);
        };
        indexTestController.$inject = [
            'TestService',
            '$location'
        ];
        return indexTestController;
    }());
    aoControllers.indexTestController = indexTestController;
    angular.module('aoControllers')
        .controller('indexTestController', indexTestController);
})(aoControllers || (aoControllers = {}));

//Libs
/// <reference path='../typescript/definitely-typed-angular/angular.d.ts' />
/// <reference path='../typescript/definitely-typed-angular/angular-route.d.ts' />
/// <reference path='../typescript/definitely-typed-angular/angular-ui-router.d.ts' />
/// <reference path='../typescript/definitely-typed-jquery/jquery.d.ts' />
//Application
/// <reference path='app.ts' />
/// <reference path='routes.ts' />
//Admin
/// <reference path='admin/AdminController.ts' />
/// <reference path='admin/AdminService.ts' />
//Admin/Child
/// <reference path='admin/child/adminChildService.ts' />
/// <reference path='admin/child/createChildAdminController.ts' />
/// <reference path='admin/child/editChildAdminController.ts' />
/// <reference path='admin/child/indexChildAdminController.ts' />
/// <reference path='admin/child/showChildAdminController.ts' />
//Admin/User
/// <reference path='admin/user/adminUserService.ts' />
/// <reference path='admin/user/createUserAdminController.ts' />
/// <reference path='admin/user/editUserAdminController.ts' />
/// <reference path='admin/user/indexUserAdminController.ts' />
/// <reference path='admin/user/showUserAdminController.ts' />
//Child
/// <reference path='child/ChildService.ts' />
/// <reference path='child/createChildController.ts' />
/// <reference path='child/editChildController.ts' />
/// <reference path='child/indexChildController.ts' />
/// <reference path='child/showChildController.ts' />
//Common
/// <reference path='common/AppController.ts' />
//Home
/// <reference path='home/HomeService.ts' />
/// <reference path='home/HomeController.ts' />
//Test
/// <reference path='test/TestService.ts' />
/// <reference path='test/IndexTestController.ts' />

// <reference path="_referencepaths.ts"/>
(function () {
    angular
        .module('aoApp')
        .config(configure);
    /* @ngInject */
    function configure($locationProvider, $compileProvider, $logProvider, $provide, $httpProvider) {
        // Intercept http calls to add auth info
        $httpProvider.interceptors.push(httpInterceptor);
        // https://code.angularjs.org/1.4.0/docs/api/ng/provider/$httpProvider
        $httpProvider.useApplyAsync(true);
        // Catch all exceptions and handle them in a function
        $provide.decorator('$exceptionHandler', extendExceptionHandler);
        // Catch all log calls and handle them in the client.
        $provide.decorator('$log', logHandler);
        // Change to disable #-urls
        $locationProvider.html5Mode({
            enabled: false,
            requireBase: false
        });
        // Turn on / off extra debug information (disable in production)
        $compileProvider.debugInfoEnabled(true);
        // Turn on / off debug console logging (disable in production)
        $logProvider.debugEnabled(true);
    }
    /* @ngInject */
    function extendExceptionHandler($delegate, $window) {
        return function (exception, cause) {
            // Add any exception tracking here - for example https://trackjs.com/
            /*
             if ($window.trackJs) {
             $window.trackJs.track(exception);
             } else if (appConfig.debug) {
             $delegate(exception, cause);
             }
             */
            if (true) {
                $delegate(exception, cause);
            }
        };
    }
    /* @ngInject */
    function httpInterceptor($q, $injector, $location) {
        var service = {
            request: request,
            responseError: responseError
        };
        return service;
        function request(config) {
            /*
             add any headers needed here, for example:
             config.headers['x-auth-token'] = 'token';
             */
            return config;
        }
        function responseError(rejection) {
            /*
             handle rejections (ie. 401) and perform logouts, loggin etc.
             if (rejection.status === 401) {
             if (rejection.config.url.indexOf('login') !== -1) {
             return $q.reject(rejection);
             } else {
             ...perform logout
             }
             } else {
             return $q.reject(rejection);
             }
             */
            /*console.log("responseError: config.ts");
            $location.path('/auth');*/
            if (rejection.status === 400) {
                //$location.path('/auth');
                console.log('error');
            }
            return $q.reject(rejection);
        }
    }
    /* @ngInject */
    function logHandler($delegate, $injector, $window) {
        var errorFn = $delegate.error;
        $delegate.error = function () {
            var args = [].slice.call(arguments);
            /*
             // Handle $log.error by notifying the user of the error.
             let toastr: any = $injector.get('toastr');
             toastr.error('System error, please contact the author');
             // log the error to trackJs
             if ($window.trackJs) {
             $window.trackJs.console.error(arguments);
             }
             */
            errorFn.apply(null, args);
        };
        return $delegate;
    }
})();

/*
 *
 *   Auteur: Benjamin Lierman
 *   Datum: 29/02/2026
 *   contact: benjamin@keidigitaal.be
 *
 */

// <reference path="../_referencepaths.ts"/>
var aoControllers;
(function (aoControllers) {
    'use strict';
    var AuthController = (function () {
        function AuthController(AuthService, $scope, $location) {
            this.AuthService = AuthService;
            this.$scope = $scope;
            this.$location = $location;
        }
        // function to handle submitting the form
        AuthController.prototype.login = function () {
            var _this = this;
            this.userData = {
                email: this.email,
                password: this.password,
                switchAuth: this.switchAuth
            };
            this.AuthService.loginUser(this.userData)
                .success(function (data) {
                if (data["succes"] != null) {
                    _this.succesMessage = data["succes"];
                    _this.$location.path('/home');
                }
                else {
                    _this.errorMessage = data["error"];
                }
            })
                .error(function () {
                _this.errorMessage = "Er is iets misgegaan, probeer het opnieuw.";
            });
        };
        AuthController.prototype.registerUser = function () {
            var _this = this;
            this.registerData = {
                name: this.name,
                givenname: this.givenname,
                street: this.street,
                streetnumber: this.streetnumber,
                postalcode: this.postalcode,
                city: this.city,
                email: this.email,
                password: this.password,
                password_repeat: this.password_repeat,
                role_id: 1
            };
            this.AuthService.registerUser(this.registerData)
                .success(function (data) {
                if (data["succes"] != null) {
                    _this.succesMessage = data["succes"];
                    _this.$location.path('/auth');
                }
                else {
                    console.log("Failed");
                    _this.errorMessage = data;
                }
            })
                .error(function (data) {
                console.log("Error");
                _this.errorMessage = data;
                console.log(data);
            });
        };
        AuthController.prototype.reset = function () {
            var _this = this;
            this.resetData = {
                email: this.email
            };
            this.AuthService.resetUser(this.resetData)
                .success(function (data) {
                if (data["succes"] != null) {
                    _this.succesMessage = data["succes"];
                    _this.$location.path('/auth');
                }
                else {
                    _this.errorMessage = data;
                }
            })
                .error(function (data) {
                console.log("Error");
                _this.errorMessage = data;
                console.log(data);
            });
        };
        AuthController.$inject = [
            'AuthService',
            '$scope',
            '$location'
        ];
        return AuthController;
    }());
    aoControllers.AuthController = AuthController;
    angular.module('aoControllers')
        .controller('AuthController', AuthController);
})(aoControllers || (aoControllers = {}));

// <reference path="../_referencepaths.ts"/>
var aoServices;
(function (aoServices) {
    'use strict';
    var AuthService = (function () {
        function AuthService($http, $q) {
            this.$http = $http;
            this.$q = $q;
        }
        AuthService.prototype.loginUser = function (userData) {
            return this.$http({
                method: 'POST',
                url: '/login/',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param(userData)
            });
            //return this.$http.get("http://jsonplaceholder.typicode.com/posts/1");
        };
        AuthService.prototype.registerUser = function (registerData) {
            return this.$http({
                method: 'POST',
                url: '/register/',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param(registerData)
            });
        };
        AuthService.prototype.resetUser = function (resetData) {
            return this.$http({
                method: 'POST',
                url: '/reset/',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param(resetData)
            });
        };
        AuthService.prototype.logoutUser = function () {
            return this.$http.get('/logout/');
        };
        AuthService.prototype.authenticate = function () {
            return this.$http.get('/check/');
        };
        AuthService.prototype.authenticateAdmin = function () {
            return this.$http.get('/admin/check/');
        };
        AuthService.$inject = [
            '$http',
            '$q'
        ];
        return AuthService;
    }());
    aoServices.AuthService = AuthService;
    angular.module('aoServices')
        .service('AuthService', AuthService);
})(aoServices || (aoServices = {}));

//# sourceMappingURL=app.js.map
