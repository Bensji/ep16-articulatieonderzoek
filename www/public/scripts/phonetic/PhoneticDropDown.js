/**
 * Created by Bart on 8/03/15.
 */

function PhoneticDropDown(path){

    this.phoneticData = null;
    this.keysWithMultipleOptions = null;
    this.allKeys = null;
    this.allKeyCodes = null;
    this.keysWithoutMultipleOptions = null;
    this.phoneticIndexSelected = null;

    this.focusedElt = null;
    this.phoneticIds = null;

    this.keysNotToBeDisabled = [
        '27', //escape
        '8', //backspace
        '46', //delete
        '37', //left
        '39', //right
        '13'  //return & enter
    ];

    this.keysNotToBeDisabledOnMenuOpen = [
        '49',
        '50',
        '51',
        '52',
        '53',
        '54',
        '55',
        '56',
        '57'
    ];

    this.init = function(path){
        var that = this;
        $.ajaxSetup({
            cache: false,
            //contentType: "application/json",
            dataType: 'json',
            timeout: 30 * 1000
        });
        $.ajax({
            type: 'get',
            url: path + 'api/phonetics/phonetics',
            url: path + 'api/phonetics/phonetics',
            complete: function(){

            },
            success: function(response){
                that.phoneticData = response;
                that.makeKeyArray();
                that.bindFocusListener();
                var inputs = $('.phonetics-only');
                for (var i=0; i<inputs.length; i++){
                    $(inputs[i]).removeAttr('disabled');
                }
            },
            error: function(){

            }
        });
    };

    this.makeKeyArray = function(){
        var that = this;
        var keyArray = [];
        var keyCodeArray = [];
        that.keysWithMultipleOptions = [];
        that.keysWithoutMultipleOptions = [];
        for (var i=0; i<that.phoneticData.length; i++){
            var alreadyInArray = false;

            var j=0;
            while (!alreadyInArray && j<keyArray.length){
                if (that.phoneticData[i]['key'].toLowerCase() == keyArray[j]){
                    alreadyInArray = true;
                }
                j++;
            }
            if (!alreadyInArray){
                keyArray.push(that.phoneticData[i]['key'].toLowerCase());
                keyCodeArray.push(that.phoneticData[i]['key_code']);
            }
        }
        that.allKeys = keyArray;
        that.allKeyCodes = keyCodeArray;
        for (var i=0; i<keyArray.length; i++){
            var occurences = 0;
            var j=0;
            while (j<that.phoneticData.length && occurences < 2){
                if (that.phoneticData[j]['key'].toLowerCase() == keyArray[i]){
                    occurences++;
                }
                j++;
            }
            if (occurences >= 2){
                that.keysWithMultipleOptions.push(keyArray[i]);
            } else {
                that.keysWithoutMultipleOptions.push(keyArray[i]);
            }
        }
    };

    this.bindFocusListener = function(){
        var that = this;
        var inputs = $('.phonetics-only');
        $('body').on('dblclick', '.phonetics-only', function(ev){
        //$(inputs).dblclick(function(ev){
            ev.preventDefault();
            var elt = ev.target;
            $(elt).focus();
            that.focusedElt.selectionStart = 0;
            that.focusedElt.selectionEnd = $(that.focusedElt).val().length;
        });
        $('body').on('focus', '.phonetics-only', function(ev){
        //$(inputs).focus(function(ev){
            var elt = ev.target;
            that.focusedElt = elt;
            var name = $(that.focusedElt).attr('name');
            var valElt = $('#' + name + '-value');
            console.log($(valElt).attr('id'));
            that.phoneticIds = that.parsePhoneticString($(valElt).val());
            that.bindKeys(elt);
            that.bindInputNavigation();
            that.bindInputClick();
        });
        $('body').on('blur', '.phonetics-only', function(){
            var name = $(that.focusedElt).attr('name');
            var valElt = $('#' + name + '-value');
            $(valElt).val(that.buildPhoneticStringRepresentation(that.phoneticIds));
            $(that.focusedElt).off('mouseup');
            that.focusedElt = null;
            Mousetrap.unbind(['left', 'right']);
            that.closeMenu();
        });
    };

    this.unbindFocusListener = function(){
        var that = this;
        var inputs = $('.phonetics-only');
        $(inputs).off('dblclick');
        $(inputs).off('focus');
        $(inputs).off('blur');
    };

    this.bindKeys = function(elt){
        var that = this;
        $(elt).keydown(function(ev){
            var keyCode = ev.keyCode;
            var char = String.fromCharCode(keyCode);
            if (!ev.shiftKey){
                char = char.toLowerCase();
            }
            keyCode = char.charCodeAt(0);
            var match = false;
            var i=0;
            while (!match && i<that.allKeyCodes.length){
                if (keyCode == that.allKeyCodes[i]){
                    match = true;
                }
                i++;
            }
            i = 0;
            while (!match && i<that.keysNotToBeDisabled.length){
                if (keyCode == that.keysNotToBeDisabled[i]){
                    match = true;
                }
                i++;
            }
            if (!match){
                var menu = $('.phonetic-chooser-container');
                if (menu.length > 0){
                    i = 0;
                    while (!match && i<that.keysNotToBeDisabledOnMenuOpen.length){
                        if (keyCode == that.keysNotToBeDisabledOnMenuOpen[i]){
                            match = true;
                        }
                        i++;
                    }
                }
            }
            if (!match){
                ev.preventDefault();
            }
        });
        for (var i=0; i<that.keysWithMultipleOptions.length; i++){
            Mousetrap.bind(that.keysWithMultipleOptions[i].toLowerCase(), function(ev){
                ev.preventDefault();
                var selectionStart = that.focusedElt.selectionStart;
                var selectionEnd = that.focusedElt.selectionEnd;
                if (that.phoneticIds != null && that.phoneticIds.length > 0){
                    if (selectionStart == selectionEnd && selectionStart < $(that.focusedElt).val().length){
                        var info = that.selectionIndexToPhoneticInfo(selectionStart);
                        if (info.phoneticOffset != info.length){
                            $(that.focusedElt).caretTo(selectionStart + (info.length-info.phoneticOffset));
                        }
                    } else {
                        var infoOnFirstPhonetic = that.selectionIndexToPhoneticInfo(selectionStart);
                        if (infoOnFirstPhonetic.phoneticOffset != infoOnFirstPhonetic.length){
                            that.focusedElt.selectionStart = selectionStart -
                                (infoOnFirstPhonetic.length - infoOnFirstPhonetic.phoneticOffset);
                        }
                        var infoOnLastPhonetic = that.selectionIndexToPhoneticInfo(selectionEnd);
                        if (infoOnLastPhonetic.phoneticOffset != infoOnLastPhonetic.length){
                            that.focusedElt.selectionEnd = selectionEnd +
                                (infoOnFirstPhonetic.length - infoOnLastPhonetic.phoneticOffset);
                        }
                    }
                }
                if (that.checkIfPhoneticMayBeInserted()){
                    that.showMenu(elt, String.fromCharCode(ev.keyCode));
                }
            });
        }
        for (var i=0; i<that.keysWithoutMultipleOptions.length; i++){
            Mousetrap.bind(that.keysWithoutMultipleOptions[i].toLowerCase(), function(ev){
                ev.preventDefault();
                var selectionStart = that.focusedElt.selectionStart;
                var selectionEnd = that.focusedElt.selectionEnd;
                if (that.phoneticIds != null && that.phoneticIds.length > 0){
                    if (selectionStart == selectionEnd && selectionStart < $(that.focusedElt).val().length){
                        var info = that.selectionIndexToPhoneticInfo(selectionStart);
                        if (info.phoneticOffset != info.length){
                            $(that.focusedElt).caretTo(selectionStart + (info.length-info.phoneticOffset));
                        }
                    } else {
                        var infoOnFirstPhonetic = that.selectionIndexToPhoneticInfo(selectionStart);
                        that.deleteSelectionOfPhonetics(selectionStart, selectionEnd);
                        $(that.focusedElt).caretTo(selectionStart-(infoOnFirstPhonetic.length-infoOnFirstPhonetic.phoneticOffset));
                    }
                }
                selectionStart = that.focusedElt.selectionStart;
                var key = String.fromCharCode(ev.keyCode);
                if (!ev.shiftKey){
                    key = key.toLowerCase();
                }
                var phonetic = that.getFirstPhoneticByKey(key);
                if (phonetic != null){
                    that.insertPhoneticAt(that.focusedElt.selectionStart, that.getIdByPhonetic(phonetic));
                    that.rerenderVisiblePhonetics();
                    $(that.focusedElt).caretTo(selectionStart+1);
                }
            });
        }
        Mousetrap.bind(['backspace', 'del'], function(ev){
            ev.preventDefault();
            var selectionStart = that.focusedElt.selectionStart;
            var selectionEnd = that.focusedElt.selectionEnd;
            var newCaret = 0;
            if (selectionStart == selectionEnd){
                if (ev.keyCode == '8' && selectionStart > 0){
                    //backspace
                    var infoOnPhonetic = that.selectionIndexToPhoneticInfo(selectionStart);
                    that.deletePhonetics(infoOnPhonetic.index, 1);
                    newCaret = selectionStart - infoOnPhonetic.phoneticOffset;
                } else if (ev.keyCode == '46' && selectionStart < $(that.focusedElt).val().length){
                    //delete
                    var infoOnPhonetic = that.selectionIndexToPhoneticInfo(selectionStart+1);
                    that.deletePhonetics(infoOnPhonetic.index, 1);
                    newCaret = selectionStart;
                }
            } else {
                // a selection was made
                var infoOnFirstPhonetic = that.selectionIndexToPhoneticInfo(selectionStart);
                that.deleteSelectionOfPhonetics(selectionStart, selectionEnd);
                newCaret = selectionStart-(infoOnFirstPhonetic.length-infoOnFirstPhonetic.phoneticOffset);
            }
            that.rerenderVisiblePhonetics();
            $(that.focusedElt).caretTo(newCaret);

        });
    };

    this.renderMenu = function(key){
        var that = this;
        var html = '<div class="phonetic-chooser-container clearfix">';
        var counter = 1;
        for (var i=0; i<that.phoneticData.length; i++){
            var sound = that.phoneticData[i];
            if (sound['key'].toLowerCase() == key){
                html += '<div class="phonetic-item-container" data-phonetic-id="' + sound.id + '">'+
                        '<div class="phonetic-item">';
                html += '<div class="phonetic-symbol">' + sound['phonetic'] + '</div>';
                html += '<div class="key">' + counter + '</div>';
                html += '</div>' +
                        '</div>';
                counter++;
            }
        }

        html += '</div>';

        return html;
    };

    this.showMenu = function(elt, key){
        var that = this;
        var menu = jQuery(that.renderMenu(key));
        $('body').append(menu);
        console.log($(elt).offset().top);
        $(menu).css({
            left: $(elt).offset().left,
            top: $(elt).offset().top - $(menu).outerHeight(true)
        });
        Mousetrap.bind(['escape', 'backspace'], function(ev){
            ev.preventDefault();
            that.closeMenu();
        });
        Mousetrap.unbind(['left', 'right']);
        that.bindMenuNavigation();
    };

    this.closeMenu = function(){
        //Mousetrap.reset();
        var that = this;
        that.phoneticIndexSelected = null;
        var menu = $('.phonetic-chooser-container');
        if (menu.length > 0){
            $(menu).detach();
        }
        Mousetrap.unbind(['left', 'right']);
        that.bindInputNavigation();
        $(that.focusedElt).unbind('keydown');
    };

    this.bindInputNavigation = function(){
        var that = this;
        Mousetrap.bind('left', function(ev){
            ev.preventDefault();
            var selectionStart = that.focusedElt.selectionStart;
            var selectionEnd = that.focusedElt.selectionEnd;
            if (selectionStart == selectionEnd){
                if (selectionStart > 0){
                    that.focusedElt.selectionStart = selectionStart-1;
                    that.focusedElt.selectionEnd = selectionStart-1;
                }
            } else {
                that.focusedElt.selectionStart = selectionStart;
                that.focusedElt.selectionEnd = selectionStart;
            }
            that.correctCaretPosition('left');
        });
        Mousetrap.bind('right', function(ev){
            ev.preventDefault();
            var selectionStart = that.focusedElt.selectionStart;
            var selectionEnd = that.focusedElt.selectionEnd;
            if (selectionStart == selectionEnd){
                if (selectionStart < $(that.focusedElt).val().length){
                    that.focusedElt.selectionStart = selectionStart+1;
                    that.focusedElt.selectionEnd = selectionStart+1;
                }
            } else {
                that.focusedElt.selectionStart = selectionEnd;
                that.focusedElt.selectionEnd = selectionEnd;
            }
            that.correctCaretPosition('right');
        });
    };

    this.bindInputClick = function(){
        var that = this;
        $(that.focusedElt).mouseup(function(ev){
            if ($(that.focusedElt).val().length > 0){
                that.correctCaretPosition('right');
            }
        });
    };

    this.correctCaretPosition = function(direction){
        var that = this;
        var selectionStart = that.focusedElt.selectionStart;
        var selectionEnd = that.focusedElt.selectionEnd;
        if (selectionStart == selectionEnd){
            var info = that.selectionIndexToPhoneticInfo(selectionStart);
            if (info.phoneticOffset != info.length){
                if (direction == 'left'){
                    that.focusedElt.selectionStart = selectionStart - (info.length - info.phoneticOffset);
                    that.focusedElt.selectionEnd = that.focusedElt.selectionStart;
                } else if (direction == 'right'){
                    that.focusedElt.selectionStart = selectionStart + (info.length - info.phoneticOffset);
                    that.focusedElt.selectionEnd = that.focusedElt.selectionStart;
                }
            }
        } else {
            var infoOnFirstPhonetic = that.selectionIndexToPhoneticInfo(selectionStart);
            if (infoOnFirstPhonetic.phoneticOffset != infoOnFirstPhonetic.length){
                that.focusedElt.selectionStart = selectionStart -
                    (infoOnFirstPhonetic.length - infoOnFirstPhonetic.phoneticOffset);
            }
            var infoOnLastPhonetic = that.selectionIndexToPhoneticInfo(selectionEnd);
            if (infoOnLastPhonetic.phoneticOffset != infoOnLastPhonetic.length){
                that.focusedElt.selectionEnd = selectionEnd +
                    (infoOnLastPhonetic.length - infoOnLastPhonetic.phoneticOffset);
            }
        }

    };

    this.bindMenuNavigation = function(){
        var that = this;
        Mousetrap.bind('left', function(ev){
            ev.preventDefault();
            var phoneticsInMenu = $('.phonetic-item-container');
            var maxIndex = phoneticsInMenu.length-1;
            if (that.phoneticIndexSelected != null){
                if (that.phoneticIndexSelected > 0){
                    that.selectPhoneticInMenu(that.phoneticIndexSelected-1);
                } else {
                    that.selectPhoneticInMenu(maxIndex);
                }
            } else {
                that.selectPhoneticInMenu(0);
            }
        });
        Mousetrap.bind('right', function(ev){
            ev.preventDefault();
            var phoneticsInMenu = $('.phonetic-item-container');
            var maxIndex = phoneticsInMenu.length-1;
            if (that.phoneticIndexSelected != null){
                if (that.phoneticIndexSelected < maxIndex){
                    that.selectPhoneticInMenu(that.phoneticIndexSelected+1);
                } else {
                    that.selectPhoneticInMenu(0);
                }
            } else {

                that.selectPhoneticInMenu(maxIndex);
            }
        });
        Mousetrap.bind(['1', '2', '3', '4', '5', '6', '7', '8', '9'], function(ev){
            ev.preventDefault();
            var phoneticsInMenu = $('.phonetic-item-container');
            var maxIndex = phoneticsInMenu.length-1;
            var keyCode = ev.keyCode;
            var char = String.fromCharCode(keyCode);
            var index = parseInt(char)-1;
            if (index <= maxIndex){
                var id = $(phoneticsInMenu[index]).attr('data-phonetic-id');
                var selectionStart = that.focusedElt.selectionStart;
                var selectionEnd = that.focusedElt.selectionEnd;
                if (selectionStart != selectionEnd){
                    that.deleteSelectionOfPhonetics(selectionStart, selectionEnd);
                }
                if (selectionStart == 0){
                    that.insertPhoneticAt(0, id);
                } else {
                    var info = that.selectionIndexToPhoneticInfo(selectionStart);
                    that.insertPhoneticAt(info.index+1, id);
                }
                that.rerenderVisiblePhonetics();
                $(that.focusedElt).caretTo(selectionStart + that.getPhoneticById(id).length);
                that.closeMenu();
                $(that.focusedElt).blur().focus();
            }
        });
        Mousetrap.bind(['return', 'enter'], function(ev){
            ev.preventDefault();
            if (that.phoneticIndexSelected != null){
                var phoneticsInMenu = $('.phonetic-item-container');
                var id = $(phoneticsInMenu[that.phoneticIndexSelected]).attr('data-phonetic-id');
                var selectionStart = that.focusedElt.selectionStart;
                var selectionEnd = that.focusedElt.selectionEnd;
                if (selectionStart != selectionEnd){
                    that.deleteSelectionOfPhonetics(selectionStart, selectionEnd);
                }
                if (selectionStart == 0){
                    that.insertPhoneticAt(0, id);
                } else {
                    var info = that.selectionIndexToPhoneticInfo(selectionStart);
                    that.insertPhoneticAt(info.index+1, id);
                }
                that.rerenderVisiblePhonetics();
                $(that.focusedElt).caretTo(selectionStart + that.getPhoneticById(id).length);
            }
            that.closeMenu();
            $(that.focusedElt).blur().focus();
        });
        $('.phonetic-item-container').mousedown(function(ev){
            that.unbindFocusListener();
            ev.stopPropagation();
            var elt = ev.target;
            while (!$(elt).hasClass('phonetic-item-container')){
                elt = $(elt).parent();
            }
            var id = $(elt).attr('data-phonetic-id');
            var selectionStart = that.focusedElt.selectionStart;
            var selectionEnd = that.focusedElt.selectionEnd;
            if (selectionStart != selectionEnd){
                that.deleteSelectionOfPhonetics(selectionStart, selectionEnd);
            }
            if (selectionStart == 0){
                that.insertPhoneticAt(0, id);
            } else {
                var info = that.selectionIndexToPhoneticInfo(selectionStart);
                that.insertPhoneticAt(info.index+1, id);
            }
            that.rerenderVisiblePhonetics();
            $(that.focusedElt).caretTo(selectionStart + that.getPhoneticById(id).length);
            that.closeMenu();
            $(that.focusedElt).blur(function(){
                $(that.focusedElt).focus();
                $(that.focusedElt).off('blur');
                that.bindFocusListener();
            });
        });
        //that.rebindFocusListener();
    };

    this.getFirstPhoneticByKey = function(key){
        var that = this;
        var i = 0;
        var phonetic = null;
        while (phonetic == null && i<that.phoneticData.length){
            if (that.phoneticData[i]['key'].toLowerCase() == key){
                phonetic = that.phoneticData[i]['phonetic'];
            }
            i++;
        }
        return phonetic;
    };

    this.selectPhoneticInMenu = function(index){
        var that = this;
        var phoneticsInMenu = $('.phonetic-item-container');
        $(phoneticsInMenu).filter('.selected').removeClass('selected');
        $(phoneticsInMenu[index]).addClass('selected');
        that.phoneticIndexSelected = index;
    };

    this.checkIfPhoneticMayBeInserted = function(){
        var that = this;
        var maxPhonetics = $(that.focusedElt).attr('data-max-phonetics');
        if (maxPhonetics != undefined){
            maxPhonetics = parseInt(maxPhonetics);
        }
        var currentNumberOfPhonetics = that.phoneticIds.length;
        if (maxPhonetics === undefined || currentNumberOfPhonetics < maxPhonetics){
            return true;
        } else {
            return false;
        }

    };

    this.insertPhoneticAt = function(index, phoneticId){
        var that = this;
        if (that.checkIfPhoneticMayBeInserted()){
            if (phoneticId >= 0){
                that.phoneticIds.splice(index, 0, phoneticId);
            }
        }
    };

    this.deletePhonetics = function(start, length){
        var that = this;
        that.phoneticIds.splice(start, length);
    };

    this.deleteSelectionOfPhonetics = function(selectionStart, selectionEnd){
        var that = this;
        var indicesToBeDeleted = [];
        var currentPos = selectionStart+1;
        while (currentPos <= selectionEnd){
            var info = that.selectionIndexToPhoneticInfo(currentPos);
            indicesToBeDeleted.push(info.index);
            currentPos = currentPos + 1 + info.length - info.phoneticOffset;
        }
        for (var i=0; i<indicesToBeDeleted.length; i++){
            that.deletePhonetics(indicesToBeDeleted[0], 1);
        }
    };

    this.parsePhoneticString = function(phonetics){
        var split = phonetics.split('|');
        var array = [];
        for (var i = 0; i<split.length; i++){
            if (split[i] != ""){
                array.push(parseInt(split[i]));
            }
        }
        return array;
    };

    this.renderPhoneticString = function(phoneticsArray){
        var that = this;
        var string = '';
        for (var i=0; i<phoneticsArray.length; i++){
            var phonetic = null;
            var j = 0;
            while (phonetic == null && j < that.phoneticData.length){
                if (phoneticsArray[i] == that.phoneticData[j]['id']){
                    phonetic = that.phoneticData[j]['phonetic'];
                }
                j++
            }
            string += phonetic;
        }
        return string;
    };

    this.buildPhoneticStringRepresentation = function(phoneticsArray){
        var that = this;
        var string = '';
        if (phoneticsArray.length > 0){
            string += phoneticsArray[0];
        }
        for (var i=1; i<phoneticsArray.length; i++){
            string += '|' + phoneticsArray[i];
        }
        return string;
    };

    this.getPhoneticById = function(id){
        var that = this;
        var phonetic = null;
        var i = 0;
        while (phonetic == null && i < that.phoneticData.length){
            if (id == that.phoneticData[i]['id']){
                phonetic = that.phoneticData[i]['phonetic'];
            }
            i++
        }
        if (phonetic != null){
            return phonetic;
        } else {
            return undefined;
        }
    };

    this.getIdByPhonetic = function(phonetic){
        var that = this;
        var id = null;
        var i = 0;
        while (id == null && i < that.phoneticData.length){
            if (phonetic == that.phoneticData[i]['phonetic']){
                id = that.phoneticData[i]['id'];
            }
            i++;
        }
        if (id != null){
            return id;
        } else {
            return -1;
        }
    };

    this.selectionIndexToPhoneticInfo = function(selectionIndex){
        var that = this;
        //var phoneticStringLength = that.phoneticIds.length;
        var i = 0;
        var length = 0;
        if (selectionIndex == 0){
            i=1;
        } else {
            while (length<selectionIndex){
                length += that.getPhoneticById(that.phoneticIds[i]).length;
                i++;
            }
        }
        return {
            index: i-1,
            phoneticOffset: that.getPhoneticById(that.phoneticIds[i-1]).length - (length-selectionIndex),
            length: that.getPhoneticById(that.phoneticIds[i-1]).length
        };
    };

    this.rerenderVisiblePhonetics = function(){
        var that = this;
        $(that.focusedElt).val(that.renderPhoneticString(that.phoneticIds));
    };

    this.init(path);
}