/**
 * Created by Bart on 21/03/15.
 */


function ErrorData(data){

    this.updated = false;
    this.testId = null;
    this.imageId = null;
    //this.word = null;
    this.transcriptions = [];
    this.phonologicalErrors = [];
    this.trailingAdditions = [];

    this.createFromParam = function(data){
        var that = this;
        that.testId = data.testId;
        that.imageId = data.imageId;
        //that.word = data.word;
        that.phonologicalErrors = data.phonologicalErrors;
        for (var i=0; i<data.transcriptions.length; i++){
            var transcription = data.transcriptions[i];
            if (!transcription.hasOwnProperty('additions')){
                transcription.additions = [];
            }
            var transcriptionData = new TranscriptionData(
                transcription.sound_id,
                transcription.sound_index,
                transcription.additions,
                transcription.distortion_id
            );
            that.transcriptions.push(transcriptionData);
        }
        that.trailingAdditions = data.trailingAdditions;
    };

    if (data != null){
        this.createFromParam(data);
    }
}

function TranscriptionData(sound_id, sound_index, additions, distortion_id){
    this.sound_id = sound_id;
    this.sound_index = sound_index;
    this.additions = additions;
    this.distortion_id = distortion_id;
}