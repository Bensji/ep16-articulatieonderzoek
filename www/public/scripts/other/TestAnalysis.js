/**
 * Created by Bart on 20/03/15.
 */

function TestAnalysis(path){

    this.path = path;
    this.phonErrorData = null;
    this.soundData = null;

    this.init = function(){
        var that = this;
        $.ajaxSetup({
            cache: false,
            //contentType: "application/json",
            dataType: 'json',
            timeout: 30 * 1000
        });
        that.getSoundData();
        that.getPhonErrorData();
        that.bindEvents();
    };

    this.bindEvents = function(){
        var that = this;
        $('body').on('click', '.addrow', function(ev){
            var rowAddition = jQuery($('#rowaddition').html());
            var elt = ev.target;
            var row = elt;
            while (!$(row).hasClass('row')){
                row = $(row).parent();
            }
            var nextRow = $(row).next('.row');
            var soundIndex, additionIndex;
            var isLast = false;
            if (nextRow.length > 0){
                if ($(nextRow).hasClass('row-eval')){
                    // laatste additie van volgende klank
                    // tel de voorgaande addities
                    var dataRows = $(row).parent().find('.row-eval').add($(row).parent().find('.row-eval-addition'));
                    var nextRowIndex = $(dataRows).index($(nextRow));
                    var index = nextRowIndex-1;
                    var additions = [];
                    while (!$(dataRows[index]).hasClass('row-eval') && index >= 0){
                        additions.push(dataRows[index]);
                        index--;
                    }
                    soundIndex = parseInt($(nextRow).attr('data-index'));
                    additionIndex = additions.length;

                } else {
                    // niet-laatste additie van volgende klank (indien die bestaat)
                    // tel voorgaande addities; incrementeer additie index van volgende addities.
                    var prevRow = $(row).prev();
                    if (prevRow.length > 0){
                        if ($(prevRow).hasClass('row-eval')){
                            // nieuwe eerste additie.
                            soundIndex = parseInt($(prevRow).attr('data-index')) + 1;
                            additionIndex = 0;
                        } else {
                            // vorige is ook additie
                            var dataRows = $(row).parent().find('.row-eval').add($(row).parent().find('.row-eval-addition'));
                            var index = $(dataRows).index($(prevRow));
                            var additions = [];
                            while (!$(dataRows[index]).hasClass('row-eval') && index >= 0){
                                additions.push(dataRows[index]);
                                index--;
                            }
                            console.log(index);
                            if (index == -1){
                                soundIndex = parseInt($(nextRow).attr('data-index'));
                            } else {
                                soundIndex = parseInt($(dataRows[index]).attr('data-index')) + 1;
                            }
                            additionIndex = additions.length;
                        }
                    } else {
                        soundIndex = 0;
                        additionIndex = 0;
                    }
                }
            } else {
                // laatste additie van laatste klank
                // tel tot aan de voorgaande klank
                var dataRows = $(row).parent().find('.row-eval').add($(row).parent().find('.row-eval-addition'));
                var index = dataRows.length-1;
                var additions = [];
                while (!$(dataRows[index]).hasClass('row-eval') && index >= 0){
                    additions.push(dataRows[index]);
                    index--;
                }
                soundIndex = parseInt($(dataRows[index]).attr('data-index')) + 1;
                additionIndex = additions.length;
                isLast = true;
            }
            console.log(soundIndex + '-' + additionIndex);
            //$(rowAddition).attr('data-index', soundIndex).attr('data-addition-index', additionIndex);
            $(rowAddition).find('input[type="text"]').attr('name', 'phonetic-' + soundIndex + '-' + additionIndex);
            $(rowAddition).find('input[type="hidden"]')
                .attr('name', 'phonetic-' + soundIndex + '-' + additionIndex + '-value')
                .attr('id', 'phonetic-' + soundIndex + '-' + additionIndex + '-value')
            ;
            if (!isLast){
                $(row).before($(rowAddition));
            } else {
                var allRows = $(row).parent().children().filter('.row');
                $(allRows[allRows.length-1]).before($(rowAddition));
            }
        });
        $('body').on('click', '.deleterow', function(ev){
            var row = ev.target;
            while (!$(row).hasClass('row')){
                row = $(row).parent();
            }
            $(row).prev().detach();
            $(row).detach();
        });
        $('body').on('click', '.sendmistakes', function(ev){
            var elt = ev.target;
            while (!$(elt).hasClass('sendmistakes')){
                elt = $(elt).parent();
            }
            var container = elt;
            while (!$(container).hasClass('analyze-row')){
                container = $(container).parent();
            }
            $(container).find('.analyze-sending').show();
            var imageId = parseInt($(elt).attr('data-imageid'));
            var testRecordingId = parseInt($(elt).attr('data-testrecordingid'));
            var testId = parseInt($(elt).attr('data-testid'));

            var dataRows = $(container).find('.row-eval').add($(container).find('.row-eval-addition'));

            var soundIndex = 0;
            var transcriptions = [];
            var additions = [];
            for (var i = 0; i<dataRows.length; i++){

                if ($(dataRows[i]).hasClass('row-eval-addition')){
                    additions.push($(dataRows[i]).find('.phonetic').next().val())
                } else {
                    var transcription = {
                        id: soundIndex,
                        soundId: $(dataRows[i]).find('.phonetic').next().val(),
                        distortionId: $(dataRows[i]).find('.mistakeselectbox').val(),
                        additions: additions
                    };
                    if (transcription.soundId == ''){
                        transcription.soundId = null;
                    }
                    if (transcription.distortionId == 0){
                        transcription.distortionId = null;
                    }
                    additions = [];
                    transcriptions.push(transcription);
                    soundIndex++;
                }

            }
            var labels = $(container).find('.phon-error-list').children();
            var phonologicalErrors = [];
            for (var i = 0; i<labels.length; i++){
                phonologicalErrors.push($(labels[i]).attr('data-phon-error-id'));
            }
            var data = {
                testId: testId,
                testRecordingId: testRecordingId,
                imageId: imageId,
                transcriptions: transcriptions,
                phonologicalErrors: phonologicalErrors,
                trailingAdditions: additions
            };
            console.log(data);
            that.submitErrorData(data, function(){
                $(container).find('.analyze-sending').hide();
            });
        });
        $('.phon-cats').change(function(ev){
            var catId = $(this).val();
            var errors = null;
            for (var i=0; i<that.phonErrorData.length; i++){
                if (that.phonErrorData[i]['id'] == catId){
                    errors = that.phonErrorData[i]['errors'];
                }
            }
            var errorSelect = $(this).parent().next().find('select');
            if (errors != null){
                var html = '<option value="0" selected>kies een proces</option>';
                for (var i=0; i<errors.length; i++){
                    var error = errors[i];
                    html += '<option value="' + error.id + '">' + error.name + '</option>';
                }
                $(errorSelect).html(html).removeAttr('disabled');
            } else {
                $(errorSelect).html('').attr('disabled', 'disabled');
            }
        });
        $('.phon-errors').change(function(ev){
            var errorId = $(this).val();
            if (errorId > 0){
                var analyzeRow = this;
                while (!$(analyzeRow).hasClass('analyze-row')){
                    analyzeRow = $(analyzeRow).parent();
                }
                var labels = $(analyzeRow).find('.phon-error-list').children();
                var recurring = false;
                for (var i=0; i<labels.length; i++){
                    if ($(labels[i]).attr('data-phon-error-id') == errorId){
                        recurring = true;
                    }
                }
                if (!recurring){
                    var errorName = null;
                    for (var i=0; i<that.phonErrorData.length; i++){
                        var errors = that.phonErrorData[i]['errors'];
                        for (var j=0; j<errors.length; j++){
                            var error = errors[j];
                            if (error.id == errorId){
                                errorName = error.name;
                            }
                        }
                    }
                    var html = '<span class="label label-default" data-phon-error-id="' + errorId + '" style="margin-right: 3px;">' +
                        errorName +
                        '&nbsp;&nbsp;<i class="fa fa-times-circle white remove-phon-err"></i>'
                        '</span>';
                    $(analyzeRow).find('.phon-error-list').append(jQuery(html));
                }
            }
        });
        $('body').on('click', '.remove-phon-err', function(ev){
            $(this).parent().detach();
        });
        $('.phonetic').on('blur', function(){
            if ($(this).parent().parent().hasClass('row-eval')){
                var phonId = $(this).parent().parent().find('.letter').attr('data-phonetic-id');
                var transId = $(this).next().val();
                var phonIsVowel = false;
                var transIsVowel = false;
                for (var i=0; i<that.soundData.length; i++){
                    if (that.soundData[i]['id'] == phonId){
                        phonIsVowel = that.soundData[i]['vowel'];
                    }
                    if (that.soundData[i]['id'] == transId){
                        transIsVowel = that.soundData[i]['vowel'];
                    }
                }
                if (!phonIsVowel && !transIsVowel){
                    $(this).parent().next().find('select').show();
                } else {
                    $(this).parent().next().find('select').val("0").hide();
                }
                if (transId == ""){
                    $(this).parent().next().find('select').val("0").hide();
                }
            }
        });
    };

    this.submitErrorData = function(data, done){
        var that = this;
        $.ajax({
            before: function(){

            },
            type: 'post',
            data: {
                data: data
            },
            url:  that.path + 'testapi/test-analysis',
            complete: function(){
                done();
            },
            success: function(response){
                console.log(response);
            },
            error: function(){

            }
        });
    };

    this.getPhonErrorData = function(){
        var that = this;
        $.ajax({
            type: 'get',
            url:  that.path + 'api/analysis/phonetic-errors',
            complete: function(){

            },
            success: function(response){
                console.log(response);
                that.phonErrorData = response;
                $('.phon-cats').removeAttr('disabled');
            },
            error: function(){

            }
        });
    };

    this.getSoundData = function(){
        var that = this;
        $.ajax({
            type: 'get',
            url:  that.path + 'api/phonetics/phonetics',
            complete: function(){

            },
            success: function(response){
                that.soundData = response;
            },
            error: function(){

            }
        });
    };

    this.init();

}