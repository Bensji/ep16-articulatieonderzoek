$(document).ready(function(){

    $.ajaxSetup({
        cache: false,
        contentType: "application/json",
        dataType: 'json',
        timeout: 30 * 1000

    });

    var baseUrl = '../../';

    window.AudioContext = window.AudioContext || window.webkitAudioContext;

    var App = {

        audioContext: null,
        audioInput: null,
        realAudioInput: null,
        inputPoint: null,
        audioRecorder: null,
        rafID: null,
        analyserContext: null,
        canvasWidth: null,
        canvasHeight: null,
        recIndex: 0,
        keyStrokes: {
            // SPACEBAR:    record/stop recording


        },
        imageController: null,
        inTestAnalysis: null,

        init: function(){
            var that = this;
            that.initCanvas();
            that.initAudio();
            that.initFragmentProgViz();
            $(window).resize(function(){
                that.windowResizeListener();
            });
            that.bindListeners();
            that.bindKeyListeners();
            var img = $('.image');
            var index = $(img).attr('data-index');
            var imageId = $(img).attr('data-image-id');
            var testId = $('body').attr('data-test-id');
            var pdd = new PhoneticDropDown(baseUrl);
            that.inTestAnalysis = new InTestAnalysis(baseUrl, testId, imageId);
            that.imageController = new ImageController(parseInt(index), parseInt(imageId), parseInt(testId), baseUrl, that.inTestAnalysis);
            that.imageController.setImageId(parseInt(imageId));
            that.imageController.loadCurrAndNext(function(){
                that.checkBckwButtonState();
            });
        },

        initCanvas: function(){
            var that = this;
            var canvas = $('#canvas-audio-viz');
            var canvasParent = $(canvas).parent();
            that.analyserContext = null;
            $(canvas).attr('width', $(canvasParent).width()+'px');
            $(canvas).attr('height', $(canvasParent).height()+'px');
        },

        initFragmentProgViz: function(){
            var that=this;
            var canvasses = $('.fragment-progress');
            for (var i=0; i<canvasses.length; i++){
                var canvas = canvasses[i];
                var canvasParent = $(canvas).parent();
                $(canvas).attr('width', $(canvasParent).width() + 4 + 'px');
                $(canvas).attr('height', $(canvasParent).height() + 4 + 'px');
            }
        },

        gotBuffers: function(buffers) {
            var that = this;
            //var canvas = document.getElementById( "wavedisplay" );

            //drawBuffer( canvas.width, canvas.height, canvas.getContext('2d'), buffers[0] );

            // the ONLY time gotBuffers is called is right after a new recording is completed -
            // so here's where we should set up the download.
            audioRecorder.exportWAV( that.doneEncoding );
        },

        doneEncoding: function(blob){
            var that = this;
            var theBlob = blob;
            var reader = new FileReader();
            reader.addEventListener("loadend", function() {

                var result = reader.result;
                var orgBufferView = new Uint8Array(result);

                /*
                 *   header length = 44 bytes
                 *
                 *   NUMBER OF SAMPLES
                 *   sampling rate = 44100/s
                 *   3s => 132300 samples => 132300 bytes
                 *
                 *   [byte 40-43] data-chunk size = samples * 4 = 529200 hex numbers
                 *   529200 dec = 00081330 hex => 30 13 08 00
                 *
                 *   [byte 44-52944] actual data
                 *
                 *   total length = 529244 bytes
                 */


                var newBuffer = new ArrayBuffer(529244);
                var newBufferView = new Uint8Array(newBuffer);

                // COPY HEADER INFO TO BYTES 0-39
                for (var i=0; i<40; i++){
                    newBufferView[i] = orgBufferView[i];
                }

                // WRITE SIZE OF DATA-CHUNK TO BYTES 40-43
                newBufferView[40] = parseInt('30', 16);
                newBufferView[41] = parseInt('13', 16);
                newBufferView[42] = parseInt('08', 16);
                newBufferView[43] = parseInt('00', 16);

                var orgBufferLength = orgBufferView.length;

                // WRITE ACTUAL AUDIO DATA TO BYTES 44-529243
                for (var i=0; i<529200; i++){
                    if (orgBufferLength - i >= 44){
                        //newBufferView[i+44] = orgBufferView[orgBufferLength-i];
                        newBufferView[529244-1-i] = orgBufferView[orgBufferLength-1-i];
                    } else {
                        newBufferView[529244-1-i] = 0;
                    }
                }

                // HTML5 AUDIO KOPPELEN AAN OPGENOMEN FRAGMENT
                var newBlob = new Blob([newBuffer], {type: 'audio/wav'});
                setupPlayback(newBlob);
                enableFragmentPlayback(true);
            });
            reader.readAsArrayBuffer(theBlob);
        },

        gotStream: function(stream){
            var that = this;
            that.audioContext = new AudioContext();
            inputPoint = that.audioContext.createGain();

            // Create an AudioNode from the stream.
            realAudioInput = that.audioContext.createMediaStreamSource(stream);
            audioInput = realAudioInput;
            audioInput.connect(inputPoint);

            analyserNode = that.audioContext.createAnalyser();
            analyserNode.fftSize = 2048;
            inputPoint.connect( analyserNode );

            audioRecorder = new Recorder( inputPoint );

            zeroGain = that.audioContext.createGain();
            zeroGain.gain.value = 0.0;
            inputPoint.connect( zeroGain );
            zeroGain.connect( that.audioContext.destination );
            $('#btn-record').removeClass('disabled').click();
            updateAnalysers();
        },

        toggleRecording: function(save) {
            var that = this;
            //e.stopPropagation();
            /*var target = e.target;
             while ($(target).attr('id') != 'btn-record'){
             target = $(target).parent();
             }*/
            var target = $('#btn-record');
            if ($(target).hasClass('recording')){
                // stop recording
                audioRecorder.stop();
                $(target).removeClass('recording');
                //e.classList.remove("recording");
                //audioRecorder.getBuffers( gotBuffers );
                if (save){
                    audioRecorder.exportWAV( that.doneEncoding );
                }

                var buttons = $('#btn-wrong').add('#btn-correct').add('#btn-unanswered');
                for (var i=0; i<buttons.length; i++){
                    if ($(buttons[i]).hasClass('disabled')){
                        $(buttons[i]).removeClass('disabled');
                    }
                }
                $(buttons).click(function(e){that.rightOrWrong(e)});
            } else {
                that.disableFragmentPlayback();
                // start recording
                if (!audioRecorder){
                    console.log('error');
                    return;
                }
                $(target).addClass('recording');
                audioRecorder.clear();
                audioRecorder.record();
            }
        },

        initAudio: function() {
            var that = this;
            if (!navigator.getUserMedia)
                navigator.getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
            if (!navigator.cancelAnimationFrame)
                navigator.cancelAnimationFrame = navigator.webkitCancelAnimationFrame || navigator.mozCancelAnimationFrame;
            if (!navigator.requestAnimationFrame)
                navigator.requestAnimationFrame = navigator.webkitRequestAnimationFrame || navigator.mozRequestAnimationFrame;

            navigator.getUserMedia(
                {
                    "audio": {
                        "mandatory": {
                            "googEchoCancellation": "false",
                            "googAutoGainControl": "false",
                            "googNoiseSuppression": "false",
                            "googHighpassFilter": "false"
                        },
                        "optional": []
                    }
                }, that.gotStream, function(e) {
                    alert('Error getting audio');
                    console.log(e);
                });
        },

        windowResizeListener: function(){
            var that = this;
            var canvas = $('#canvas-audio-viz');
            $(canvas).attr('width', 0+'px');
            $(canvas).attr('height', 0+'px');
            that.initCanvas();
            that.initFragmentProgViz();
        },

        disableFragmentPlayback: function(){
            var that = this;
            var button = $('#btn-playback');
            $(button).unbind('click');
            if (!$(button).hasClass('disabled')){
                $(button).addClass('disabled');
            }
        },

        bindListeners: function(){
            var that = this;
            $('#btn-aanvulzin').click(function(){that.finishSentence()});
            $('#btn-record').click(function(){that.toggleRecording(true)});
            $('#btn-forward').click(function(){
                if (!$(this).hasClass('disabled')){
                    that.next()
                }
            });
            $('#btn-backward').click(function(){
                if (!$(this).hasClass('disabled')){
                    that.prev()
                }
            });
            $('#btn-fs').click(function(){
                toggleFullScreen();
                var fa = $(this).find('.fa');
                if ($(fa).hasClass('fa-expand')){
                    $(fa).removeClass('fa-expand').addClass('fa-compress');
                } else if ($(fa).hasClass('fa-compress')){
                    $(fa).removeClass('fa-compress').addClass('fa-expand');
                }
            });
            $('#btn-note').click(function(ev){
                ev.stopPropagation();
                if (!$(this).hasClass('disabled')){
                    $('#disable-interaction').css('display', 'block');
                    $('#main-content').animate({
                        'margin-left': -500
                    }, 350);
                }
                $('#disable-interaction').click(function(ev){
                    ev.stopPropagation();
                    $(this).css('display', 'none');
                    $('#main-content').animate({
                        'margin-left': 0
                    }, 350);
                    $(this).off('click');
                });
            });
            $('#btn-quit').click(function(ev){
                window.location = '../../test';
            });
        },

        bindKeyListeners: function(){
            $('body')
                .bind('keyup', 'esc', function(ev){
                    var fa = $('#btn-fs').find('.fa');
                    if ($(fa).hasClass('fa-compress')){
                        $(fa).removeClass('fa-compress').addClass('fa-expand');
                    }
                })
                .bind('keyup', 'f', function(ev){
                    $('#btn-fs').click();
                })
                .bind('keyup', 'space', function(){
                    $('#btn-record').click();
                })
                .bind('keyup', 'left', function(){
                    $('#btn-backward').click();
                })
                .bind('keyup', 'right', function(){
                    $('#btn-forward').click();
                })
                .bind('keyup', 'up', function(){
                    $('#btn-correct').click();
                })
                .bind('keyup', 'down', function(){
                    $('#btn-wrong').click();
                })
                .bind('keyup', 'z', function(){
                    $('#btn-aanvulzin').click();
                })
                .bind('keyup', 'w', function(){
                    $('#btn-nazeggen').click();
                })
                .bind('keyup', 'a', function(){
                    $('#btn-playback').click();
                })
                .bind('keyup', 's', function(){
                    $('#btn-quit').click();
                })
                .bind('keyup', 'e', function(){
                    $('#btn-note').click();
                })
            ;
        },

        unbindListeners: function(){

        },

        finishSentence: function(){
            var that = this;
            var audio = document.getElementById('audio-aanvulzin');
            that.imageController.sentenceWasPlayed();
            audio.play();
            updateAanvulzinProgress();
            var btnRepeatWord = $('#btn-nazeggen');
            if ($(btnRepeatWord).hasClass('disabled')){
                $(btnRepeatWord).click(function(){that.repeatWord()});
                $(btnRepeatWord).removeClass('disabled');
            }
        },

        rightOrWrong: function(e){
            var that = this;
            var buttons = $('#btn-wrong').add('#btn-correct').add('#btn-unanswered');
            var elt = e.target;
            while (!$(elt).hasClass('cs-btn')){
                elt = $(elt).parent();
            }
            for (var i = 0; i<buttons.length; i++){
                if ($(buttons[i]).hasClass('selected')){
                    $(buttons[i]).removeClass('selected');
                }
            }
            $(elt).addClass('selected');
            var forward = $('#btn-forward');
            if ($(forward).hasClass('disabled')){
                $(forward).removeClass('disabled');
            }
            var note = $('#btn-note');
            /*if ($(elt).attr('id') == 'btn-wrong' && $(note).hasClass('disabled')){
             $(note).removeClass('disabled');
             } else if ($(elt).attr('id') == 'btn-correct' && !$(note).hasClass('disabled')){
             $(note).addClass('disabled');
             }*/

            if ($(elt).attr('id') == 'btn-wrong'){
                console.log('wrong');
                that.imageController.hasMistake(true);
                console.log(that.imageController.localImageData.hasMistake);
                that.imageController.wasAnswered(true);
                if ($(note).hasClass('disabled')){
                    $(note).removeClass('disabled');
                }
            } else if ($(elt).attr('id') == 'btn-correct'){
                that.imageController.hasMistake(false);
                that.imageController.wasAnswered(true);
                if (!$(note).hasClass('disabled')){
                    $(note).addClass('disabled');
                }
            } else if ($(elt).attr('id') == 'btn-unanswered'){
                that.imageController.wasAnswered(false);
                if (!$(note).hasClass('disabled')){
                    $(note).addClass('disabled');
                }
            }

        },

        repeatWord: function(){
            var that = this;
            var audio = document.getElementById('audio-voorzeggen');
            that.imageController.wordWasPlayed();
            audio.play();
            updateNazeggenProgress();
        },

        next: function(){
            var that = this;
            if (that.imageController.hasNext()){
                if (!that.imageController.isWorking()){
                    var buttons = $('#btn-wrong').add('#btn-correct').add('#btn-unanswered');
                    for (var i=0; i<buttons.length; i++){
                        if (!$(buttons[i]).hasClass('disabled')){
                            $(buttons[i]).addClass('disabled');
                        }
                    }
                    $(buttons).unbind('click');
                    if (that.imageController.imageData['next']['testrecording'] == null){
                        $('#btn-record').click();
                    }
                    if (!that.imageController.hasNextNext()){
                        $('#btn-forward').find('.fa').removeClass('fa-forward').addClass('fa-step-forward');
                    }
                    that.imageController.renderNext();
                }
                // TODO handle case imageController IS working
                that.disableWordButton();
                that.enableBckwButton();
                if (that.imageController.imageData['next']['testrecording'] == null){
                    that.disableFwButton();
                    that.deselectRightWrongButton();
                    that.disableEditButton();
                } else {
                    that.enableFwButton();
                }
                that.imageController.loadCurrPlusTwo(true, function(){});
            } else {
                that.imageController.pushData(function(){});
                // end the test
            }
        },

        prev: function(){
            var that = this;

            // Check for new inputted data of current image TODO

            // Render new image
            if (!that.imageController.isWorking()){
                that.imageController.renderPrev();
                if ($('#btn-forward').find('.fa').hasClass('fa-step-forward')){
                    $('#btn-forward').find('.fa').removeClass('fa-step-forward').addClass('fa-forward');
                }
            }
            // TODO handle case imageController IS working
            if ($('#btn-record').hasClass('recording')){
                that.toggleRecording(false);
            }
            that.disableWordButton();
            that.disableBckwButton();
            that.enableFwButton();
            enableFragmentPlayback(true);
            that.imageController.loadCurrPlusTwo(false, function(){
                that.checkBckwButtonState();
            });
        },

        deselectRightWrongButton: function(){
            var buttons = $('#btn-correct').add('#btn-wrong').add('#btn-unanswered');
            for (var i=0; i<buttons.length; i++){
                if ($(buttons[i]).hasClass('selected')){
                    $(buttons[i]).removeClass('selected');
                }
            }
        },

        disableFwButton: function(){
            var that = this;
            var btnForward = $('#btn-forward');
            if (!$(btnForward).hasClass('disabled')){
                $(btnForward).addClass('disabled');
            }
        },

        enableFwButton: function(){
            var that = this;
            var btnForward = $('#btn-forward');
            if ($(btnForward).hasClass('disabled')){
                $(btnForward).removeClass('disabled');
            }
        },

        disableBckwButton: function(){
            var that = this;
            var btnBackward = $('#btn-backward');

            if (!$(btnBackward).hasClass('disabled')){
                $(btnBackward).addClass('disabled');
            }
        },

        enableBckwButton: function(){
            var that = this;
            var btnBackward = $('#btn-backward');

            if ($(btnBackward).hasClass('disabled')){
                $(btnBackward).removeClass('disabled');
            }
        },

        checkBckwButtonState: function(){
            var that = this;
            var btnBackward = $('#btn-backward');

            if (that.imageController.hasPrev() && $(btnBackward).hasClass('disabled')){
                $(btnBackward).removeClass('disabled');
            } else if (!that.imageController.hasPrev() && !$(btnBackward).hasClass('disabled')){
                $(btnBackward).addClass('disabled');
            }
        },

        disableWordButton: function(){
            var that = this;
            var btnWord = $('#btn-nazeggen');
            if (!$(btnWord).hasClass('disabled')){
                $(btnWord).addClass('disabled');
            }
        },

        disableEditButton: function(){
            var that = this;
            var btn = $('#btn-note');
            if (!$(btn).hasClass('disabled')){
                $(btn).addClass('disabled');
            }
        }



    };

    app = App;
    App.init();

});

var app;

function updateNazeggenProgress(time){
    var that=this;
    var audio = document.getElementById('audio-voorzeggen');
    var currentTime = audio.currentTime;
    var totalTime = audio.duration;
    var canvas = document.getElementById('nazeggen-progress');
    var width = $(canvas).width();
    var context = canvas.getContext('2d');
    context.clearRect(0, 0, width, width);
    context.strokeStyle = '#999';
    context.lineWidth = 3;
    context.lineCap = "round";
    context.beginPath();
    context.arc(Math.ceil(width/2), Math.ceil(width/2), width/2-2, 0 - Math.PI/2, (currentTime/totalTime) * 2 * Math.PI - Math.PI/2);
    context.stroke();
    if (currentTime != totalTime){
        window.requestAnimationFrame( updateNazeggenProgress );
    } else {
        context.clearRect(0, 0, width, width);
    }
}

function updateAanvulzinProgress(time){
    var that=this;
    var audio = document.getElementById('audio-aanvulzin');
    var currentTime = audio.currentTime;
    var totalTime = audio.duration;
    var canvas = document.getElementById('aanvulzin-progress');
    var width = $(canvas).width();
    var context = canvas.getContext('2d');
    context.clearRect(0, 0, width, width);
    context.strokeStyle = '#999';
    context.lineWidth = 3;
    context.lineCap = "round";
    context.beginPath();
    context.arc(Math.ceil(width/2), Math.ceil(width/2), width/2-2, 0 - Math.PI/2, (currentTime/totalTime) * 2 * Math.PI - Math.PI/2);
    context.stroke();
    if (currentTime != totalTime){
        window.requestAnimationFrame( updateAanvulzinProgress );
    } else {
        context.clearRect(0, 0, width, width);
    }
}

function updatePlaybackProgress(time){
    var that=this;
    var audio = document.getElementById('audio-playback');
    var currentTime = audio.currentTime;
    var totalTime = audio.duration;
    var canvas = document.getElementById('playback-progress');
    var width = $(canvas).width();
    var context = canvas.getContext('2d');
    context.clearRect(0, 0, width, width);
    context.strokeStyle = '#999';
    context.lineWidth = 3;
    context.lineCap = "round";
    context.beginPath();
    context.arc(Math.ceil(width/2), Math.ceil(width/2), width/2-2, 0 - Math.PI/2, (currentTime/totalTime) * 2 * Math.PI - Math.PI/2);
    context.stroke();
    if (currentTime != totalTime){
        window.requestAnimationFrame( updatePlaybackProgress );
    } else {
        context.clearRect(0, 0, width, width);
    }
}

function updateAnalysers(time) {
    //var that=this;
    if (!app.analyserContext) {
        var canvas = document.getElementById("canvas-audio-viz");
        canvasWidth = canvas.width;
        canvasHeight = canvas.height;
        analyserContext = canvas.getContext('2d');
    }

    // analyzer draw code here
    {
        var SPACING = 3;
        var BAR_WIDTH = 1;
        var numBars = Math.round(canvasWidth / SPACING);
        var freqByteData = new Uint8Array(analyserNode.frequencyBinCount);

        analyserNode.getByteFrequencyData(freqByteData);
        //console.log(analyserNode.getByteFrequencyData(freqByteData));

        analyserContext.clearRect(0, 0, canvasWidth, canvasHeight);
        analyserContext.fillStyle = '#F6D565';
        analyserContext.lineCap = 'round';
        var multiplier = analyserNode.frequencyBinCount / numBars;

        var total = 0;
        // Draw rectangle for each frequency bin.
        for (var i = 0; i < numBars; ++i) {
            var magnitude = 0;
            var offset = Math.floor( i * multiplier );
            // gotta sum/average the block, or we miss narrow-bandwidth spikes
            for (var j = 0; j< multiplier; j++)
                magnitude += freqByteData[offset + j];
            magnitude = magnitude / multiplier;
            var magnitude2 = freqByteData[i * multiplier];
            total += magnitude;
            //analyserContext.fillStyle = "hsl( " + Math.round((i*360)/numBars) + ", 100%, 50%)";
            //analyserContext.fillRect(i * SPACING, canvasHeight, BAR_WIDTH, -magnitude);
        }
        var percentage = (total / 100) / numBars;
        if (percentage > 1){
            percentage = 1;
        }
        //console.log(percentage);
        analyserContext.fillStyle = '#ccc';
        analyserContext.beginPath();
        analyserContext.arc(Math.floor(canvasWidth/2), Math.floor(canvasHeight/2), ((percentage*(3/8)) + (5/8)) * canvasWidth * 0.45, 0, 2*Math.PI, false);
        analyserContext.fill();
        //analyserContext = null;
    }

    rafID = window.requestAnimationFrame( updateAnalysers );
}

function enableFragmentPlayback(truefalse){
    var button = $('#btn-playback');
    if (truefalse){
        if ($(button).hasClass('disabled')){
            $(button).removeClass('disabled');
        }
        $(button).click(function(){
            var audio = document.getElementById('audio-playback');
            audio.play();
            updatePlaybackProgress();
        })
    } else {
        $(button).unbind('click');
        if (!$(button).hasClass('disabled')){
            $(button).addClass('disabled');
        }
    }
}

function setupPlayback(blob){
    app.imageController.setRecording(blob);

    var url = (window.URL || window.webkitURL).createObjectURL(blob);
    var link = document.getElementById("audio-playback");

    $(link).find('source').attr('src', url);
    $(link).load();
}

function toggleFullScreen() {
    if (!document.fullscreenElement &&    // alternative standard method
        !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
        if (document.documentElement.requestFullscreen) {
            document.documentElement.requestFullscreen();
        } else if (document.documentElement.msRequestFullscreen) {
            document.documentElement.msRequestFullscreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullscreen) {
            document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
    }
}






