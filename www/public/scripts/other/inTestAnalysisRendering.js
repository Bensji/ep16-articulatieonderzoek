/**
 * Created by Bart on 21/03/15.
 */

function renderSideBarContent(imageData, errorData, phoneticData, phonologicalErrorData){
    var html =  '<div class="col-xs-12 analyze-row">'+
                    '<div class="row">'+
                        '<div class="col col-xs-12">'+
                            '<h1>' + imageData.word.charAt(0).toUpperCase() + imageData.word.slice(1) + '</h1>'+
                        '</div>'+
                    '</div>'+
                    '<div class="row">'+
                        '<div class="col col-xs-12">'+
                            '<h4>Fonetisch</h4>'+
                        '</div>'+
                    '</div>';
    for (var i=0; i<imageData.phonetics.length; i++){
        var sound = imageData.phonetics[i];
        var transcription = errorData.transcriptions[i];
        var replacingSound = null;
        var transIsVowel = false;
        var j =0;
        while (replacingSound == null && j<phoneticData.length){
            if (phoneticData[j].id == transcription.sound_id){
                replacingSound = phoneticData[j].phonetic;
                transIsVowel = phoneticData[j].vowel;
            }
            j++;
        }
        for (var k=0; k<transcription.additions.length; k++){
            var addition = transcription.additions[k];
            var additionSound = null;
            var l = 0;
            while (additionSound == null && l < phoneticData.length){
                if (phoneticData[l].id == addition){
                    additionSound = phoneticData[l].phonetic;
                }
                l++;
            }
            html += renderAddAddition();
            html += renderAddition(i, k, addition, additionSound);
        }
        html += renderAddAddition();
        if (transcription.sound_id == null){
            transcription.sound_id = '';
            replacingSound = '';
        }
        html+= renderSound(sound.phonetic, sound.id, i, replacingSound, transcription.sound_id, sound.distortions, transcription.distortion_id, sound.vowel, transIsVowel);
    }
    for (var i=0; i<errorData.trailingAdditions.length; i++){
        var addition = errorData.trailingAdditions[i];
        var additionSound = null;
        var l = 0;
        while (additionSound == null && l < phoneticData.length){
            if (phoneticData[l].id == addition){
                additionSound = phoneticData[l].phonetic;
            }
            l++;
        }
        html += renderAddAddition();
        html += renderAddition(i, k, addition, additionSound);
    }

    html+= renderAddAddition();

    html += '<div class="row">'+
                '<div class="col col-xs-12">'+
                    '<h4>Fonologisch</h4>'+
                '</div>'+
            '</div>';

    html += renderPhonologicPicker(phonologicalErrorData);
    html += renderPhonologicalErrorList(errorData.phonologicalErrors);

    html +=     '</div>';
    return html;
}

function renderAddition(soundIndex, additionOrder, soundId, sound){
    return  '<div class="row row-eval-addition">'+
                '<div class="col-xs-1 letter"><i class="fa fa-minus deleterow"></i></div>'+
                '<div class="col-xs-2 input-wrapper">'+
                    '<input class="form-control phonetic mousetrap phonetics-only" type="text" data-index="" value="' + sound + '" data-max-phonetics="1" name="phonetic-' + soundIndex + '-' + additionOrder + '">'+
                    '<input type="hidden" id="phonetic-' + soundIndex + '-' + additionOrder + '-value" name="phonetic-' + soundIndex + '-' + additionOrder + '-value" value="' + soundId + '">'+
                '</div>'+
                '<div class="col-xs-9 select-wrapper">'+
                '</div>'+
            '</div>';
}

function renderSound(sound, soundId, soundIndex, replacingSound, replacingSoundId, distortions, distortionId, phonIsVowel, transIsVowel){
    var html=   '<div class="row row-eval">'+
                    '<div class="col-xs-1 letter" data-phonetic-id="' + soundId + '">' + sound + '</div>'+
                    '<div class="col-xs-2 input-wrapper">' +
                        '<input placeholder="" class="form-control phonetic mousetrap phonetics-only" autocomplete="off" data-index="2" data-max-phonetics="1" name="phonetic-' + soundIndex + '" type="text" value="' + replacingSound + '">'+
                        '<input type="hidden" id="phonetic-' + soundIndex + '-value" name="phonetic-2-value" value="' + replacingSoundId + '">'+
                    '</div>'+
                    '<div class="col-xs-9 select-wrapper">';
    if (!phonIsVowel && !transIsVowel){
        html+=          '<select class="form-control mistakeselectbox" data-placeholder="Distorsie (geen)" name="distorsie">';
    } else {
        html+=          '<select class="form-control mistakeselectbox" data-placeholder="Distorsie (geen)" name="distorsie" style="display: none;">';
    }
    if (distortionId == null){
        html+=              '<option value="0" selected="selected">distorsie (geen)</option>';
    } else {
        html+=              '<option value="0"">distorsie (geen)</option>';
    }
    for (var i=0; i<distortions.length; i++){
        var distortion = distortions[i];
        if (distortion.id == distortionId){
            html+=          '<option value="' + distortion.id + '" selected="selected">' + distortion.name + '</option>';
        } else {
            html+=          '<option value="' + distortion.id + '">' + distortion.name + '</option>';
        }
    }

    html+=              '</select>'+
                    '</div>'+
                '</div>';
    return html;
}

function renderAddAddition(){
    var html =  '<div class="row">'+
                    '<div class="col-xs-1 addrow-wrapper letter"><i class="fa fa-plus addrow"></i></div>'+
                '</div>';
    return html;
}

function renderPhonologicPicker(categories){
    var html =  '<div class="row">'+
                    '<div class="col col-xs-5">'+
                        '<select class="form-control phon-cats" data-placeholder="categorie (geen)" name="phonologicalError">' +
                            '<option value="0" selected="selected">kies een categorie</option>';
    for (var i=0; i<categories.length; i++){
        var category = categories[i];
        html +=             '<option value="' + category.id + '">' + category.name + '</option>';
    }
    html+=              '</select>'+
                    '</div>'+
                    '<div class="col col-xs-7">'+
                        '<select class="form-control phon-errors" data-placeholder="proces (geen)" disabled="disabled" name="distorsie">' +
                        '</select>'+
                    '</div>'+
                '</div>';
    return html;
}

function renderPhonologicalErrorList(errors){
    var html =  '<div class="row">'+
                    '<h4 class="col col-xs-12 phon-error-list clearfix">';

    for (var i=0; i<errors.length; i++){
        var error = errors[i];
        html += renderPhonologicalError(error.id, error.name);
    }

    html+=          '</h4>'+
                '</div>';
    return html;
}

function renderPhonologicalError(id, name){
    html = '<span class="label label-default" data-phon-error-id="' + id + '" style="margin-right: 3px; margin-bottom: 3px;">' + name + '&nbsp;&nbsp;<i class="fa fa-times-circle white remove-phon-err"></i></span>&ZeroWidthSpace;';
    return html;
}

function renderSelectOptions(){

}