/**
 * Created by Bart on 13/12/14.
 */
function ImageController(index, imageId, currentTestId, baseUrl, inTestAnalysis){

    this.current = index;
    this.imageData = null;
    this.baseUrl = baseUrl;
    this.working = false;
    this.testId = currentTestId;
    this.inTestAnalysis = inTestAnalysis;

    this.localImageData = new TestImage(this.testId, this.imageId);

    this.unansweredData = null;
    this.unansweredIndex = null;

    this.hasPrev = function(){
        var that = this;
        return that.current > 1;
    };

    this.hasNext = function(){
        var that = this;
        return that.imageData['next'] != null;
    };

    this.hasNextNext = function(){
        var that = this;
        return that.imageData['nextnext'] != null;
    };

    this.setImageId = function(id){
        var that = this;
        that.localImageData.imageId = id;
    };

    this.sentenceWasPlayed = function(){
        var that = this;
        that.localImageData.sentencePlay = true;
    };

    this.wordWasPlayed = function(){
        var that = this;
        that.localImageData.wordPlay = true;
    };

    this.setRecording = function(blob){
        var that = this;
        that.localImageData.newRecording = true;
        that.localImageData.recording = blob;
    };

    this.hasRecording = function(){
        var that = this;
        return that.localImageData.recording !== null;
    };

    this.hasMistake = function(truefalse){
        var that = this;
        that.localImageData.errorDataChanged = true;
        that.localImageData.hasMistake = truefalse;
    };

    this.wasAnswered = function(truefalse){
        var that = this;
        that.localImageData.errorDataChanged = true;
        that.localImageData.answered = truefalse;
    };

    this.loadCurrPlusTwo = function(forward, success){
        var that = this;
        var imageIndex = forward ? ++that.current : --that.current;
        $.ajax({
            before: function(){
                that.working = true;
            },
            type: "GET",
            url: '../../testapi/test-images',
            data: {
                'testId': that.testId,
                'imageIndex': imageIndex
            },
            success: function(response){
                that.imageData = response;
                success();
            },
            error: function(a,b,c){
                that.logErrors(a,b,c);
            },
            complete: function(response){
                that.working = false;
            }
        });
    };

    this.loadCurrAndNext = function(success){
        var that = this;
        $.ajax({
            before: function(){
                that.working = true;
            },
            type: "GET",
            url: '../../testapi/test-images',
            data: {
                'testId': that.testId,
                'imageIndex': that.current
            },
            success: function(response){
                that.imageData = response;
                success();
            },
            error: function(a,b,c){
                that.logErrors(a,b,c);
            },
            complete: function(response){
                that.working = false;
            }
        });
    };

    this.pushData = function(success){
        var that = this;
        var fd = new FormData();
        fd.append('testId', that.localImageData.testId);
        fd.append('imageId', that.localImageData.imageId);
        if (that.localImageData.newRecording == true){
            fd.append('recording', that.localImageData.recording);
        } else {
            fd.append('recording', null);
        }
        console.log(that.localImageData.hasMistake);
        fd.append('hasMistake', that.localImageData.hasMistake);
        fd.append('answered', that.localImageData.answered);
        fd.append('wordPlay', that.localImageData.wordPlay);
        fd.append('sentencePlay', that.localImageData.sentencePlay);
        fd.append('hasNewRecording', that.localImageData.newRecording);
        fd.append('hasNewErrorData', that.localImageData.errorDataChanged);
        fd.append('errorData',  JSON.stringify(that.inTestAnalysis.updateErrorData()));
        $.ajax({
            before: function(){
                //that.working = true;
            },
            type: "POST",
            url: '../../testapi/test-data',
            processData: false,
            contentType: false,
            //data: JSON.stringify(that.localImageData),
            //data: that.localImageData,
            data: fd,
            success: function(response){
                if (response['ended'] == true && response['skipped'] > 0 && that.unansweredData == null){
                    that.displayRetakeUnanswered(response['skipped']);
                } else if (response['ended'] == true){
                    $('#btn-quit').click();
                }
                success();
                console.log(response);
            },
            error: function(a,b,c){
                that.showError('Er is iets misgelopen. De data van de vorige afbeelding is niet verstuurd naar de server.');
            },
            complete: function(response){
                console.log("De data van de vorige wefbeelding is verstuurd");
                that.working = false;
            }
        });
    };

    this.displayRetakeUnanswered = function(count){
        var that = this;
        var nuw = $('#notify-unanswered-wrapper');
        $(nuw).find('.nu-notification')
            .html('Er is/zijn ' + count + ' onbenoemde afbeelding(en).<br>Wenst u die te hernemen?');
        $('#nu-yes').on('click', function(){
            that.unansweredIndex = 0;
            that.getUnanswered();
            $(this).off('click');
            $(nuw).hide();
        });
        $('#nu-no').on('click', function(){
            $('#btn-quit').click();
        });
        $(nuw).show();
    };

    this.isWorking = function(){
        var that = this;
        return that.working;
    };

    this.renderCurr = function(){
        var that = this;
        $('.image')
            .attr('src', that.baseUrl + "uploads/images/" + that.imageData['curr']['moduleImage']['image']['url'])
            .attr('data-image-id', that.imageData['curr']['moduleImage']['id'])
        ;
    };

    this.renderNext = function(){
        var that = this;
        if (that.localImageData.hasNewData() || that.inTestAnalysis.currentErrorData.updated){
            that.pushData(function(){});
        }
        that.render('next');
    };

    this.renderPrev = function(){
        var that = this;
        that.render('prev');
    };
    
    this.render = function(which){
        var that = this;
        that.inTestAnalysis.fetchImageData(that.imageData[which]['moduleImage']['id']);
        that.inTestAnalysis.analysisDataLoaded = false;
        that.inTestAnalysis.fetchAnalysisData(that.testId, that.imageData[which]['moduleImage']['id']);
        $('.image')
            .attr('src', that.baseUrl + "uploads/images/" + that.imageData[which]['moduleImage']['image']['url'])
            .attr('data-image-id', that.imageData[which]['moduleImage']['id'])
        ;
        var aa = $('#audio-aanvulzin');
        $(aa).find('source').attr('src', baseUrl + 'uploads/sentences/' + that.imageData[which]['moduleImage']['image']['sentenceurl']);
        $(aa).load();
        var av = $('#audio-voorzeggen');
        $(av).find('source').attr('src', baseUrl + 'uploads/words/' + that.imageData[which]['moduleImage']['image']['wordurl']);
        $(av).load();
        that.localImageData = new TestImage(that.testId, that.imageData[which]['moduleImage']['image']['id']);
        $('#progress-counter').find('.current').text(that.imageData[which]['moduleImage']['index']);

        if (that.imageData[which]['testrecording'] != null){
            that.localImageData.recording = that.imageData[which]['testrecording']['filename'];
            that.localImageData.sentencePlay = that.imageData[which]['testrecording']['sentenceplay'];
            that.localImageData.wordPlay = that.imageData[which]['testrecording']['wordplay'];
            var ap = $('#audio-playback');
            $(ap).find('source').attr('src', baseUrl + 'uploads/recordings/' + that.localImageData.recording);
            $(ap).load();
            var btnWrong = $('#btn-wrong');
            var btnCorrect = $('#btn-correct');
            var btnUnanswered = $('#btn-unanswered');
            var btnNote = $('#btn-note');
            var btnsWrongCorrect = $(btnWrong).add(btnCorrect).add(btnUnanswered);
            for (var i=0; i<btnsWrongCorrect.length; i++){
                if ($(btnsWrongCorrect[i]).hasClass('disabled')){
                    $(btnsWrongCorrect[i]).removeClass('disabled');
                }
            }
            //if (!that.imageData[which]['testrecording']['answered']){
                //TODO
            if (false){
                console.log("!that.imageData[which]['testrecording']['answered']");

            } else {
                if (that.imageData[which]['testrecording']['hasMistake']){
                    $(btnWrong).addClass('selected');
                    if ($(btnCorrect).hasClass('selected')){
                        $(btnCorrect).removeClass('selected');
                        console.log(that.localImageData);
                    }
                    if ($(btnNote).hasClass('disabled')){
                        $(btnNote).removeClass('disabled');
                    }
                    if ($(btnUnanswered).hasClass('selected')){
                        $(btnUnanswered).removeClass('selected');
                    }
                } else {
                    $(btnCorrect).addClass('selected');
                    console.log(that.localImageData);
                    if ($(btnWrong).hasClass('selected')){
                        $(btnWrong).removeClass('selected');
                    }
                    if (!$(btnNote).hasClass('disabled')){
                        $(btnNote).addClass('disabled');
                    }
                    if ($(btnUnanswered).hasClass('selected')){
                        $(btnUnanswered).removeClass('selected');
                    }
                }
            }
        }
    };

    this.getUnanswered = function(){
        var that = this;
        $.ajax({
            before: function(){
                that.working = true;
            },
            data: {
                'testId': that.testId
            },
            type: "GET",
            contentType: "application/json",
            dataType: 'json',
            url: '../../testapi/unanswered',
            success: function(response){
                that.unansweredData = response;
                that.unansweredIndex = 0;
                that.bindUnansweredEvents();
                that.renderUnanswered(that.unansweredIndex);
            },
            error: function(a,b,c){
                that.logErrors(a,b,c);
            },
            complete: function(response){
                that.working = false;
            }
        });
    };

    this.bindUnansweredEvents = function(){
        var that = this;
        $('#btn-forward').off('click').click(function(){
            if (!$(this).hasClass('disabled')){
                that.renderUnanswered(that.unansweredIndex+1);
            }
        });
        $('#btn-backward').off('click').click(function(){
            if (!$(this).hasClass('disabled')){
                that.renderUnanswered(that.unansweredIndex-1);
            }
        });
    };

    this.renderUnanswered = function(index){
        var that = this;
        that.unansweredIndex = index;
        if (index >= that.unansweredData.length){
            that.pushData(function(){
                $('#btn-quit').click();
            });
        } else {
            that.inTestAnalysis.fetchImageData(that.unansweredData[index]['image']['id']);
            that.inTestAnalysis.analysisDataLoaded = false;
            that.inTestAnalysis.fetchAnalysisData(that.testId, that.unansweredData[index]['image']['id']);
            $('.image')
                .attr('src', that.baseUrl + "uploads/images/" + that.unansweredData[index]['image']['url'])
                .attr('data-image-id', that.unansweredData[index]['image']['id'])
            ;
            var aa = $('#audio-aanvulzin');
            $(aa).find('source').attr('src', baseUrl + 'uploads/sentences/' + that.unansweredData[index]['image']['sentenceurl']);
            $(aa).load();
            var av = $('#audio-voorzeggen');
            $(av).find('source').attr('src', baseUrl + 'uploads/words/' + that.unansweredData[index]['image']['wordurl']);
            $(av).load();
            $('#progress-counter').find('.current').text(that.unansweredData[index]['index']);

            that.localImageData = new TestImage(that.testId, that.unansweredData[index]['image_id']);
            if (index == that.unansweredData.length-1){
                var btnForward = $('#btn-forward');
                $(btnForward).find('.fa').removeClass('fa-forward').addClass('fa-step-forward');
            }
            var btnBackward = $('#btn-backward');
            if (index == 0){

                if (!$(btnBackward).hasClass('disabled')){
                    $(btnBackward).addClass('disabled');
                }
            }
            if (index >= 1){
                if ($(btnBackward).hasClass('disabled')){
                    $(btnBackward).removeClass('disabled');
                }
            }
            var btnWord = $('#btn-nazeggen');
            if (!$(btnWord).hasClass('disabled')){
                $(btnWord).addClass('disabled');
            }
            var btn = $('#btn-note');
            if (!$(btn).hasClass('disabled')){
                $(btn).addClass('disabled');
            }
            var buttons = $('#btn-correct').add('#btn-wrong').add('#btn-unanswered');
            for (var i=0; i<buttons.length; i++){
                if ($(buttons[i]).hasClass('selected')){
                    $(buttons[i]).removeClass('selected');
                }
            }
            $('#btn-record').click();
        }

    };

    this.showError = function(error){
        $('#error-message').text(error);
        var errorTable = $('#error-table');
        $(errorTable).animate({
            top: $('#error').outerHeight(true)
        }, 400);
    };

}