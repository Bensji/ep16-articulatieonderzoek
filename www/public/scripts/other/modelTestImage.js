/**
 * Created by Bart on 13/12/14.
 */

function TestImage(testId, imageId){

    this.testId = testId;
    this.imageId = imageId;
    this.sentencePlay = false;
    this.wordPlay = false;
    this.recording = null;
    this.answered = true;
    this.hasMistake = null;
    this.newRecording = false;
    this.errorDataChanged = false;

    this.hasNewData = function(){
        return (this.newRecording || this.errorDataChanged);
    }

};