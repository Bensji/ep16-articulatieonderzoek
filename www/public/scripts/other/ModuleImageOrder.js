/**
 * Created by Bart on 24/03/15.
 */

function ModuleImageOrder(baseUrl){
    this.baseUrl = baseUrl;
    this.animationTime = 0;

    this.current = null;

    this.init = function(){
        var that = this;
        $.ajaxSetup({
            cache: false,
            //contentType: "application/json",
            dataType: 'json',
            timeout: 30 * 1000
        });
        that.bindReposition();
        that.bindOther();
        that.getImageData();
    };

    this.bindReposition = function(){
        var that = this;
        var ct = $('#images');
        $('body').on('mousedown', '.image', function(ev){
            $('body').disableTextSelect();
            var image = this;
            var hovering = $(image).clone();
            that.current = $(image).clone();
            var timer;
            //timer = setTimeout(function(){that.moveImages(ev)}, 600);
            $(ct).css({
                'width': $(ct).width(),
                'height': $(ct).height()
            });
            $(image).css('visibility', 'hidden').addClass('current').addClass('spacing');
            //that.detachAnimation(image);
            $(hovering)
                .addClass('hovering')
                .css({
                'float': 'none',
                'width': $(image).outerWidth(),
                'height': $(image).outerHeight(),
                'position': 'fixed',
                'margin': 0,
                'left': ev.clientX - $(image).outerWidth()/2,
                'top': ev.clientY - $(image).outerHeight()/2
            });
            $('body').append($(hovering));


            $('body').mousemove(function(ev){
                clearTimeout(timer);
                timer = setTimeout(function(){that.makeSpaceForInsert(ev)}, 600);
                $(hovering).css({
                    'left': ev.clientX - $(hovering).outerWidth()/2,
                    'top': ev.clientY - $(hovering).outerHeight()/2
                })
            });
            $('body').mouseup(function(){
                clearTimeout(timer);
                $('body').off('mousemove');
                $('body').off('mouseup');
                $(ct).css({
                    'width': 'auto',
                    'height': 'auto'
                });
                var spacing = $(ct).find('.spacing');
                if (spacing.length == 1){
                    $(spacing).after($(that.current));
                    $(image).detach();
                    $(spacing).detach();
                    $(hovering).detach();
                } else {
                    $(hovering).detach();
                    $(image).after($(that.current)).detach();
                }
                that.fillHiddenInput();
                $('body').enableTextSelect();
            });
        });
    };

    this.bindOther = function(){
        var that = this;
        $('#image-select').change(function(){
            var id = $(this).val();
            that.appendImage(id);
            $(this).val(0);
            that.renderImageSelect();
        });
        $('body').on('mousedown', '.remove', function(ev){
            ev.stopPropagation();
            $(this).mouseup(function(){
                var id = $(this).parent().attr('data-image-id');
                that.removeImage(id);
                $(this).off('mouseup');
            });
        })
    };

    this.appendImage = function(id){
        var that = this;
        var image = null;
        for (var i =0; i<that.imageData.length; i++){
            if (that.imageData[i].id == id){
                image = that.imageData[i];
            }
        }
        if (image != null){
            var ct = $('#images');
            var html =  '<div class="image" data-image-id="' + image.id + '" style="background-image: url(\'' + that.baseUrl + 'uploads/images/' + image.url + '\')">'+
                            '<img class="aspect-1-1" src="' + baseUrl + 'css/images/aspect-1-1.png' + '" alt="" />' +
                            '<div class="abs">'+
                                '<div class="image-name">' + image.word + '</div>'+
                            '</div>' +
                            '<div class="remove">'+
                                '<i class="fa fa-times"></i>'+
                            '</div>'+
                        '</div>';
            $(ct).append($(html));
        }
        that.fillHiddenInput();
    };

    this.removeImage = function(id){
        var that = this;
        var images = $('.image');
        for (var i = 0; i<images.length; i++){
            if ($(images[i]).attr('data-image-id') == id){
                $(images[i]).detach();
            }
        }
        that.renderImageSelect();
        that.fillHiddenInput();
    };

    this.makeSpaceForInsert = function(ev){
        var that = this;
        var coords = that.getTableCoordinates(ev);
        if (coords != null){
            that.shiftImages(coords);
        }
    };

    this.numberOfRows = function(){
        var that = this;
        var images = $('#images').find('.image').not('.current').add('.current.spacing');
        var r = Math.ceil(images.length/that.numberOfImagesInRow());
        return r;
    };

    this.numberOfImagesInRow = function(){
        var ct = $('#images');
        var images = $(ct).find('.image').not('.current').add('.current.spacing');
        var minimalOffsetX = $(images[0]).offset().left;
        /*var i = 1;
        while ($(images[i]).offset().left > minimalOffsetX && i<images.length){
            i++;
        }*/
        var ctWidth = $(ct).width();
        var imageWidth = $(images[0]).outerWidth(true);
        var i = Math.round(ctWidth/imageWidth);
        return i;
    };

    this.getTableCoordinates = function(ev){
        var that = this;
        var mouseX = ev.clientX;
        var mouseY = ev.clientY;
        var images = $('#images').find('.image').not('.current').add('.current.spacing');
        var rowCount = that.numberOfRows();
        var rowImageCount = that.numberOfImagesInRow();
        var rowIndex = null;
        for (var i=0; i<rowCount; i++){
            if ((mouseY > $(images[i*rowImageCount]).offset().top - $(document).scrollTop()) &&
                (mouseY < $(images[i*rowImageCount]).offset().top + $(images[i*rowImageCount]).outerHeight(true)  - $(document).scrollTop() )){
                rowIndex = i;
            }
        }
        var cellIndex = null;
        if (rowIndex != null){
            /*for (var i=0; i<rowImageCount; i++){
                if ((mouseX > $(images[i]).offset().left - $(images[i]).outerWidth(true)/2) &&
                    (mouseX < $(images[i]).offset().left + $(images[i]).outerWidth(true)/2)){
                    cellIndex = i;
                }
            }*/
            var i=0;
            while (cellIndex == null && i<rowImageCount){
                if (i < images.length){
                    if ((mouseX > $(images[i]).offset().left - $(images[i]).outerWidth(true)/2) &&
                        (mouseX < $(images[i]).offset().left + $(images[i]).outerWidth(true)/2)){
                        cellIndex = i;
                    }
                } else {
                    if ((mouseX > $(images[i - (images.length-i)-1]).offset().left + $(images[i - (images.length-i)-1]).outerWidth(true)/2) &&
                        (mouseX < $(images[i - (images.length-i)-1]).offset().left + 3 * ($(images[i - (images.length-i)-1]).outerWidth(true)/2))){
                        cellIndex = images.length;
                    }
                }
                i++;
            }
        }
        if (cellIndex != null && rowIndex != null){
            return {
                rIndex: rowIndex,
                cIndex: cellIndex
            };
        } else if (rowIndex == rowCount-1){
            if ((mouseX > $(images[rowImageCount-1]).offset().left + $(images[rowImageCount-1]).outerWidth(true)/2) &&
                (mouseX < $(images[rowImageCount-1]).offset().left + 3*($(images[rowImageCount-1]).outerWidth(true)/2))){
                return {
                    rIndex: rowIndex,
                    cIndex: rowImageCount
                };
            }
        } else {
            return null;
        }
    };

    this.shiftImages = function(coords){
        var that = this;
        var ct = $('#images');
        var images = $('#images').find('.image').not('.current').add('.current.spacing');
        var current = $('.spacing');

        var currentIndex = $(images).index($(current));
        var imagesInRow = that.numberOfImagesInRow();

        var rIndex = Math.floor(currentIndex/imagesInRow);
        var cIndex = currentIndex % imagesInRow;

        if (rIndex != coords.rIndex || cIndex != coords.cIndex){
            //if (rIndex == coords.rIndex){
            if (true){
                var newSpacing = jQuery('<div></div>');
                $(newSpacing)
                    .addClass('spacing')
                    .addClass('image')
                    .css({
                        'height': $(current).outerHeight(),
                        'opacity': 0,
                        'margin': 0,
                        'padding': 0,
                        'width': 0
                    });
                var width = $(current).outerWidth();
                var height = $(current).outerHeight();
                if (coords.cellIndex == -1){
                    $(images[images.length-1]).after($(newSpacing));
                } else {
                    if (rIndex == coords.rIndex){
                        if (coords.cIndex == images.length){
                            $(images[coords.rIndex*imagesInRow+coords.cIndex-1]).after($(newSpacing));
                        } else {
                            $(images[coords.rIndex*imagesInRow+coords.cIndex]).before($(newSpacing));
                        }
                    } else if (coords.rIndex > rIndex){
                        if (coords.cIndex != 0){
                            $(images[coords.rIndex*imagesInRow+coords.cIndex-1]).after($(newSpacing));
                        } else {
                            $(images[coords.rIndex*imagesInRow+coords.cIndex]).after($(newSpacing));
                        }
                        /*var flyingImages = [];
                        for (var i = currentIndex; i<images.length; i++){
                            if (i%imagesInRow == 0 && Math.floor(i/imagesInRow) != coords.rIndex + 1){
                                flyingImages.push(i);
                            }
                        }
                        console.log(flyingImages);
                        for (var i=0; i<flyingImages.length; i++){
                            var fi = images[flyingImages[i]];
                            var flying = $(fi).clone();
                            $(flying).css({
                                'float': 'none',
                                'width': $(fi).outerWidth(),
                                'height': $(fi).outerHeight(),
                                'position': 'fixed',
                                'left': $(fi).offset().left,
                                'top': $(fi).offset().top - $(document).scrollTop(),
                                'margin': 0
                            });
                            $('body').append($(flying));
                            //$(fi).css('opacity', 0);
                            var prev = images[flyingImages[i]-1];
                            var top = $(prev).offset().top-$(document).scrollTop();
                            var left = $(prev).offset().left;
                            $(flying).stop().animate({
                                'top': top,
                                'left': left
                            }, that.animationTime, function(){
                                //$('.temp-hidden').css('opacity', 1).removeClass('temp-hidden');
                                $(this).detach();
                            });
                            $(fi).stop().animate({
                                'width': 0
                            }, that.animationTime, function(){
                                $(this).css({
                                    'width': '14.666666%',
                                    'opacity': 1
                                })
                            });
                        }*/

                    } else {
                        $(images[coords.rIndex*imagesInRow+coords.cIndex]).before($(newSpacing));
                        /*var flyingImages = [];
                        for (var i = currentIndex; i<images.length; i++){
                            if (i%imagesInRow == imagesInRow-1){
                                flyingImages.push(i);
                            }
                        }
                        console.log(flyingImages);
                        for (var i=0; i<flyingImages.length; i++){
                            var fi = images[flyingImages[i]];
                            var flying = $(fi).clone();
                            $(flying).css({
                                'float': 'none',
                                'width': $(fi).outerWidth(),
                                'height': $(fi).outerHeight(),
                                'position': 'fixed',
                                'left': $(fi).offset().left,
                                'top': $(fi).offset().top - $(document).scrollTop(),
                                'margin': 0
                            });
                            $('body').append($(flying));
                            $(fi).css('opacity', 0);
                            var prev = images[flyingImages[i]+1];
                            var top = $(prev).offset().top-$(document).scrollTop();
                            var left = $(prev).offset().left;
                            $(flying).stop().animate({
                                'top': top,
                                'left': left
                            }, that.animationTime, function(){
                                //$('.temp-hidden').css('opacity', 1).removeClass('temp-hidden');
                                $(this).detach();
                            });
                            $(fi).stop().animate({
                                'width': 0
                            }, that.animationTime, function(){
                                $(this).css({
                                    'width': '14.666666%',
                                    'opacity': 1
                                })
                            });
                        }*/
                    }
                }

                $(current).stop().animate({
                    'width': 0,
                    'margin': 0,
                    'padding': 0
                }, that.animationTime, function(){
                    $(this)
                        .css({
                            'opacity': 1,
                            'display': 'none',
                            'width': 'auto'
                        })
                    ;
                    if (!$(this).hasClass('current')){
                        $(this).detach();
                    } else {
                        $(this).removeClass('spacing');
                    }
                });
                $(newSpacing).animate({
                    'width': width-1,
                    'height': height-1,
                    'margin': '1%'
                }, that.animationTime);

            } /*else {
                var newSpacing = jQuery('<div></div>');
                $(newSpacing)
                    .addClass('spacing')
                    .addClass('image')
                    .css({
                        'height': $(current).outerHeight(),
                        'opacity': 0,
                        'margin': 0,
                        'padding': 0,
                        'width': 0
                    });
                var width = $(current).outerWidth();
                var height = $(current).outerHeight();

                $(images[coords.rIndex*imagesInRow+coords.cIndex]).before($(newSpacing));

                $(current).stop().animate({
                    'width': 0,
                    'margin': 0,
                    'padding': 0
                }, that.animationTime, function(){
                    $(this)
                        .css({
                            'opacity': 1,
                            'display': 'none',
                            'width': 'auto'
                        })
                    ;
                    if (!$(this).hasClass('current')){
                        $(this).detach();
                    } else {
                        $(this).removeClass('spacing');
                    }
                });
                $(newSpacing).animate({
                    'width': width - 1,
                    'height': height - 1,
                    'margin': '1%'
                }, that.animationTime);
            }*/
        }
/*
        var flyingImages = [];
        for (var i = currentIndex; i<images.length; i++){
            if (i%imagesInRow == 0){
                flyingImages.push(i);
            }
        }
        for (var i=0; i<flyingImages.length; i++){
            var fi = images[flyingImages[i]];
            var flying = $(fi).clone();
            $(flying).css({
                'float': 'none',
                'width': $(fi).outerWidth(),
                'height': $(fi).outerHeight(),
                'position': 'fixed',
                'left': $(fi).offset().left,
                'top': $(fi).offset().top - $(document).scrollTop(),
                'margin': 0
            });
            $('body').append($(flying));
            $(fi).css('opacity', 0);
            var prev = images[flyingImages[i]-1];
            var top = $(prev).offset().top-$(document).scrollTop();
            var left = $(prev).offset().left;
            $(flying).stop().animate({
                'top': top,
                'left': left
            }, that.animationTime, function(){
                $(fi).css('opacity', 1);
                $(this).detach();
            });
        }

        $(current).css('opacity', '0');
        $(current).stop().animate({
            'width': 0
        }, that.animationTime, function(){
            $(this)
                .css({
                    'opacity': 1,
                    'width': 'auto'
                })
                .addClass('being-repositioned')
            ;
        });
        */
    };

    this.getImageData = function(){
        var that = this;
        $.ajax({
            type: 'get',
            url:  that.baseUrl + 'api/image/images',
            complete: function(){

            },
            success: function(response){
                that.imageData = response;
                that.renderImageSelect();
            },
            error: function(){

            }
        });
    };

    this.renderImageSelect = function(){
        var that = this;
        var images = $('.image');
        var ids = [];
        for (var i=0; i<images.length; i++){
            ids.push($(images[i]).attr('data-image-id'));
        }
        var html = '<option value="0">Figuren toevoegen</option>';
        for (var i=0; i<that.imageData.length; i++){
            var match = false;
            var j=0;
            while (!match && j<images.length){
                if (that.imageData[i].id == ids[j]){
                    match = true;
                }
                j++;
            }
            if (!match){
                html += '<option value="' + that.imageData[i].id + '">' + that.imageData[i].word + '</option>';
            }
        }
        var is = $('#image-select');
        $(is).html(html);
        var options = $(is).find('option');
        if ($(is).attr('disabled') != undefined){
            $(is).removeAttr('disabled');
        }
        if (options.length == 1){
            $(is).html('<option value="0">Alle figuren zijn reeds toegevoegd</option>');
            $(is).attr('disabled', 'disabled');
        }
    };

    this.fillHiddenInput = function(){
        var that = this;
        var images = $('.image');
        var idString = '';
        for (var i = 0; i<images.length; i++){
            if (i == 0){
                idString += $(images[i]).attr('data-image-id');
            } else{
                idString += '|' + $(images[i]).attr('data-image-id');
            }
        }
        $('#image-ids').val(idString);
    };

    this.init();
}

