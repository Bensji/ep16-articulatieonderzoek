/**
 * Created by Bart on 21/03/15.
 */

function InTestAnalysis(path, testId, imageId){

    this.path = path;

    this.currentImageData = null;
    this.currentErrorData = null;
    this.phoneticData = null;
    this.phonologicalErrorData = null;

    this.analysisDataLoaded = false;
    this.phoneticDataLoaded = false;
    this.phonologicalErrorDataLoaded = false;

    this.init = function(testId, imageId){
        var that = this;
        that.fetchImageData(imageId);
        that.fetchAnalysisData(testId, imageId);
        that.fetchPhoneticData();
        that.fetchPhonologicalErrorData();
        that.bindEvents();
    };

    this.bindEvents = function(){
        var that = this;
        $('body').on('change', '.phon-cats', function(ev){
            that.currentErrorData.updated = true;
            var catId = parseInt($(this).val());
            var errors = that.phonologicalErrorData[catId-1].errors;
            var errorSelect = $(this).parent().next().find('select');
            if (errors != null){
                var html = '<option value="0" selected>kies een proces</option>';
                for (var i=0; i<errors.length; i++){
                    var error = errors[i];
                    html += '<option value="' + error.id + '">' + error.name + '</option>';
                }
                $(errorSelect).html(html).removeAttr('disabled');
            } else {
                $(errorSelect).html('').attr('disabled', 'disabled');
            }
        });
        $('body').on('change', '.phon-errors', function(ev){
            that.currentErrorData.updated = true;
            var errorId = $(this).val();
            if (errorId > 0){
                var analyzeRow = this;
                while (!$(analyzeRow).hasClass('analyze-row')){
                    analyzeRow = $(analyzeRow).parent();
                }
                var labels = $(analyzeRow).find('.phon-error-list').children();
                var recurring = false;
                for (var i=0; i<labels.length; i++){
                    if ($(labels[i]).attr('data-phon-error-id') == errorId){
                        recurring = true;
                    }
                }
                if (!recurring){
                    var errorName = null;
                    for (var i=0; i<that.phonologicalErrorData.length; i++){
                        var errors = that.phonologicalErrorData[i]['errors'];
                        for (var j=0; j<errors.length; j++){
                            var error = errors[j];
                            if (error.id == errorId){
                                errorName = error.name;
                            }
                        }
                    }
                    var html = renderPhonologicalError(errorId, errorName);
                    $(analyzeRow).find('.phon-error-list').append(jQuery(html));
                }
            }
        });
        $('body').on('click', '.remove-phon-err', function(ev){
            that.currentErrorData.updated = true;
            $(this).parent().detach();
        });
        $('body').on('click', function(ev){
            //console.log('click');
            var phoneticInputs = $('.phonetic');
            console.log(phoneticInputs.length);
            that.currentErrorData.updated = true;
            for (var k=0; k<phoneticInputs.length; k++){
                var pi = phoneticInputs[k];
                //console.log(i);
                if ($(pi).parent().parent().hasClass('row-eval')){
                    /*if ($(this).parent().parent().find('.letter').attr('data-phonetic-id') == $(this).next().val()){
                        $(this).parent().next().find('select').show();
                    } else {
                        $(this).parent().next().find('select').val("0").hide();
                    }*/
                    var phonId = $(pi).parent().parent().find('.letter').attr('data-phonetic-id');
                    var transId = $(pi).next().val();
                    var phonIsVowel = false;
                    var transIsVowel = false;
                    for (var i=0; i<that.phoneticData.length; i++){
                        if (that.phoneticData[i]['id'] == phonId){
                            phonIsVowel = that.phoneticData[i]['vowel'];
                        }
                        if (that.phoneticData[i]['id'] == transId){
                            transIsVowel = that.phoneticData[i]['vowel'];
                        }
                    }
                    //console.log(phonIsVowel);
                    //console.log(transIsVowel);
                    if (!phonIsVowel && !transIsVowel){
                        $(pi).parent().next().find('select').show();
                    } else {
                        $(pi).parent().next().find('select').val("0").hide();
                    }
                }
            }
        });
        $('body').on('click', '.addrow', function(ev){
            that.currentErrorData.updated = true;
            var row = this;
            while (!$(row).hasClass('row')){
                row = $(row).parent();
            }
            var html = renderAddAddition();
            var date = new Date();
            var soundIndex = date.getTime();
            html+= renderAddition(soundIndex, soundIndex, '', '');
            $(row).before(html);
        });
        $('body').on('click', '.deleterow', function(ev){
            that.currentErrorData.updated = true;
            var row = ev.target;
            while (!$(row).hasClass('row')){
                row = $(row).parent();
            }
            $(row).prev().detach();
            $(row).detach();
        });
        $('body').on('click', 'h1', function(){
            that.updateErrorData();
        });
    };

    this.fetchAnalysisData = function(testId, imageId){
        var that = this;
        $.ajax({
            type: 'get',
            data: {
                'testId': testId,
                'imageId': imageId
            },
            url:  that.path + 'testapi/image-analysis-data',
            complete: function(){

            },
            success: function(response){
                that.currentErrorData = new ErrorData(response);
                that.analysisDataLoaded = true;
                //that.proceedIfLoaded();
            },
            error: function(){

            }
        });
    };

    this.fetchPhoneticData = function(){
        var that = this;
        $.ajax({
            type: 'get',
            url:  that.path + 'api/phonetics/phonetics',
            complete: function(){

            },
            success: function(response){
                that.phoneticData = response;
                that.phoneticDataLoaded = true;
                //that.proceedIfLoaded();
            },
            error: function(){

            }
        });
    };

    this.fetchPhonologicalErrorData = function(){
        var that = this;
        $.ajax({
            type: 'get',
            url:  that.path + 'testapi/phonological-error-data',
            complete: function(){

            },
            success: function(response){
                that.phonologicalErrorData = response;
                that.phonologicalErrorDataLoaded = true;
                //that.proceedIfLoaded();
            },
            error: function(){

            }
        });
    };

    this.fetchImageData = function(imageId){
        var that = this;
        $.ajax({
            type: 'get',
            data: {
                'imageId': imageId
            },
            url:  that.path + 'testapi/image-data',
            complete: function(){

            },
            success: function(response){
                that.currentImageData = response;
                that.proceedIfLoaded();
            },
            error: function(){

            }
        });
    };

    this.proceedIfLoaded = function(){
        var that = this;
        if (that.analysisDataLoaded && that.phoneticDataLoaded && that.phonologicalErrorDataLoaded){
            console.log(that.currentErrorData);
            that.rerenderSideBar();
        } else {
            setTimeout(function(){that.proceedIfLoaded();}, 300);
        }
    };

    this.rerenderSideBar = function(){
        var that = this;
        var html = renderSideBarContent(that.currentImageData, that.currentErrorData, that.phoneticData, that.phonologicalErrorData);
        $('#sidebar-content').html(html);
    };

    this.updateErrorData = function(){
        var that = this;
        var rootElt = $('.analyze-row');
        var rows = $(rootElt).find('.row-eval').add($(rootElt).find('.row-eval-addition'));
        var errorData = new ErrorData(null);
        errorData.testId = that.currentErrorData.testId;
        errorData.imageId = that.currentErrorData.imageId;

        var additions = [];
        var sound_index = 0;
        for (var i=0; i<rows.length; i++){
            var row = rows[i];
            if ($(row).hasClass('row-eval')){
                var distortionId = $(row).find('.mistakeselectbox').val();
                var sound_id = $(row).find('input[type="hidden"]').val();
                var transcription = new TranscriptionData(sound_id, sound_index, additions, distortionId);
                errorData.transcriptions.push(transcription);
                additions = [];
                sound_index++;
            } else {
                var input = $(row).find('input[type="hidden"]');
                if ($(input).val() != ""){
                    additions.push($(input).val());
                }
            }
        }
        errorData.trailingAdditions = additions;

        var errors = $(rootElt).find('.phon-error-list').children();
        for (var i=0; i< errors.length; i++){
            var error = errors[i];
            errorData.phonologicalErrors.push($(error).attr('data-phon-error-id'));
        }
        console.log(errorData);
        return errorData;

    };

    this.init(testId, imageId);

}