<?php

use App\Models\User;
use Faker\Provider\nl_BE as Faker;
use App\Models\Patient;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(User::class, function ($faker) {
    $faker->addProvider(new Faker\Address($faker));
    $faker->addProvider(new Faker\Internet($faker));
    $faker->addProvider(new Faker\Person($faker));

    return [
        'name' => $faker->firstname,
        'givenname' => $faker->lastname,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'street' => $faker->word(),
        'streetnumber' => $faker->numberBetween($min = 1, $max = 100),
        'city' => $faker->word(),
        'postalcode' => $faker->numberBetween($min = 1001, $max = 9999),
        'remember_token' => str_random(10),
        'role_id' => $faker->numberBetween($min = 1, $max = 4)
    ];
});

$factory->define(Patient::class, function ($faker) {
    $faker->addProvider(new Faker\Internet($faker));
    $faker->addProvider(new Faker\Person($faker));
    $faker->addProvider(new Faker\PhoneNumber($faker));

    return [
        'name' => $faker->firstname,
        'givenname' => $faker->lastname,
        'email' => $faker->safeEmail,
        'street' => $faker->word(),
        'streetnumber' => $faker->numberBetween($min = 1, $max = 100),
        'city' => $faker->word(),
        'postalcode' => $faker->numberBetween($min = 1001, $max = 9999),
        'sex' => 'm',
        'initials' => 'BL',
        'birthdate' => Carbon::now(),
        'user_id' => $faker->numberBetween($min= 1, $max= 6),
    ];


});
