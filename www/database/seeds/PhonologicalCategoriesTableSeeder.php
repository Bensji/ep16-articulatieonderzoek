<?php

use Illuminate\Database\Seeder;
use App\Models\PhonologicalCategory;

class PhonologicalCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PhonologicalCategory::create([
            'name' => 'syllabestructuurprocessen'
        ]);
        PhonologicalCategory::create([
            'name' => 'substitutieprocessen'
        ]);
        PhonologicalCategory::create([
            'name' => 'harmonieprocessen'
        ]);
        PhonologicalCategory::create([
            'name' => 'overige'
        ]);
    }
}
