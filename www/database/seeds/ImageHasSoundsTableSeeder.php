<?php

use Illuminate\Database\Seeder;
use App\Models\ImageHasSound;

class ImageHasSoundsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ImageHasSound::create(array(
            'image_id' => 1,
            'sound_id' => 1,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 1,
            'sound_id' => 22,
            'index' => 1
        ));
        ImageHasSound::create(array(
            'image_id' => 1,
            'sound_id' => 7,
            'index' => 2
        ));
        ImageHasSound::create(array(
            'image_id' => 1,
            'sound_id' => 34,
            'index' => 3
        ));
        ImageHasSound::create(array(
            'image_id' => 2,
            'sound_id' => 41,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 2,
            'sound_id' => 11,
            'index' => 1
        ));
        ImageHasSound::create(array(
            'image_id' => 2,
            'sound_id' => 1,
            'index' => 2
        ));
        ImageHasSound::create(array(
            'image_id' => 2,
            'sound_id' => 42,
            'index' => 3
        ));
        ImageHasSound::create(array(
            'image_id' => 2,
            'sound_id' => 13,
            'index' => 4
        ));
        ImageHasSound::create(array(
            'image_id' => 2,
            'sound_id' => 37,
            'index' => 5
        ));
        ImageHasSound::create(array(
            'image_id' => 2,
            'sound_id' => 6,
            'index' => 6
        ));
        ImageHasSound::create(array(
            'image_id' => 2,
            'sound_id' => 11,
            'index' => 8
        ));
        ImageHasSound::create(array(
            'image_id' => 2,
            'sound_id' => 37,
            'index' => 7
        ));
        ImageHasSound::create(array(
            'image_id' => 2,
            'sound_id' => 2,
            'index' => 8
        ));
        ImageHasSound::create(array(
            'image_id' => 2,
            'sound_id' => 28,
            'index' => 9
        ));
        ImageHasSound::create(array(
            'image_id' => 2,
            'sound_id' => 7,
            'index' => 10
        ));
        ImageHasSound::create(array(
            'image_id' => 2,
            'sound_id' => 42,
            'index' => 11
        ));
        ImageHasSound::create(array(
            'image_id' => 3,
            'sound_id' => 41,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 3,
            'sound_id' => 1,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 3,
            'sound_id' => 33,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 3,
            'sound_id' => 7,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 3,
            'sound_id' => 11,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 4,
            'sound_id' => 25,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 4,
            'sound_id' => 8,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 4,
            'sound_id' => 11,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 5,
            'sound_id' => 26,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 5,
            'sound_id' => 34,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 5,
            'sound_id' => 5,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 5,
            'sound_id' => 23,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 6,
            'sound_id' => 28,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 6,
            'sound_id' => 34,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 6,
            'sound_id' => 1,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 6,
            'sound_id' => 23,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 7,
            'sound_id' => 31,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 7,
            'sound_id' => 5,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 7,
            'sound_id' => 33,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 7,
            'sound_id' => 23,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 8,
            'sound_id' => 31,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 8,
            'sound_id' => 5,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 8,
            'sound_id' => 34,
            'index' => 0
        ));
        ImageHasSound::create(array(
            'image_id' => 8,
            'sound_id' => 35,
            'index' => 0
        ));
    }
}
