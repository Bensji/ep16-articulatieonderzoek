<?php

use Illuminate\Database\Seeder;
use App\Models\ModuleImage;

class ModulesHasImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('modules_has_images')->delete();

        // $Modules_has_images = array(
        //     'module_id' => 1,
        //     'image_id' => 1,
        //     'index' => 1
        // );
        // $Modules_has_images2 = array(
        //     'module_id' => 1,
        //     'image_id' => 2,
        //     'index' => 2
        // );
        // $Modules_has_images3 = array(
        //     'module_id' => 1,
        //     'image_id' => 3,
        //     'index' => 3
        // );

        // DB::table('modules_has_images')->insert($Modules_has_images);
        // DB::table('modules_has_images')->insert($Modules_has_images2);
        // DB::table('modules_has_images')->insert($Modules_has_images3);
        ModuleImage::create([
            'module_id' => 1,
            'image_id' => 1,
            'index' => 1
        ]);
        ModuleImage::create([
            'module_id' => 1,
            'image_id' => 2,
            'index' => 2
        ]);
        ModuleImage::create([
            'module_id' => 1,
            'image_id' => 3,
            'index' => 3
        ]);
    }
}
