<?php

use Illuminate\Database\Seeder;
use App\Models\Mistake;

class MistakeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* PARENTS */
        //1
        Mistake::create([
            'name' => 'fonetisch',
            'parent_id' => null,
            'listshow' => false
        ]);
        //2
        Mistake::create([
            'name' => 'fonologisch',
            'parent_id' => null,
            'listshow' => false
        ]);

        /*-----FONETISCHE-----*/
        //3
        Mistake::create([
            'name' => 'distorsie',
            'parent_id' => 1,
            'listshow' => false
        ]);
        //4
        Mistake::create([
            'name' => 'omissie',
            'parent_id' => 1,
            'listshow' => false
        ]);
        //5
        Mistake::create([
            'name' => 'substitutie',
            'parent_id' => 1,
            'listshow' => false
        ]);
        //6
        Mistake::create([
            'name' => 'additie',
            'parent_id' => 1,
            'listshow' => false
        ]);

        /*DISTORSIES*/
        //7
        Mistake::create([
            'name' => 'interdentaal',
            'parent_id' => 3,
            'listshow' => false
        ]);
        //8
        Mistake::create([
            'name' => 'addentaal',
            'parent_id' => 3,
            'listshow' => false
        ]);
        //9
        Mistake::create([
            'name' => 'sigm.simplex',
            'parent_id' => 3,
            'listshow' => false
        ]);
        //10
        Mistake::create([
            'name' => 'lateraal',
            'parent_id' => 3,
            'listshow' => false
        ]);
        //11
        Mistake::create([
            'name' => 'non sonans',
            'parent_id' => 3,
            'listshow' => false
        ]);
        //12
        Mistake::create([
            'name' => 'non vibrans',
            'parent_id' => 3,
            'listshow' => false
        ]);
        //13
        Mistake::create([
            'name' => 'stridens',
            'parent_id' => 3,
            'listshow' => false
        ]);
        //14
        Mistake::create([
            'name' => 'nasaal',
            'parent_id' => 3,
            'listshow' => false
        ]);
        //15
        Mistake::create([
            'name' => 'lateroflexus',
            'parent_id' => 3,
            'listshow' => false
        ]);
        //16
        Mistake::create([
            'name' => 'labiodentaal',
            'parent_id' => 3,
            'listshow' => false
        ]);
        //17
        Mistake::create([
            'name' => 'labiaal',
            'parent_id' => 3,
            'listshow' => false
        ]);
        //18
        Mistake::create([
            'name' => 'palataal',
            'parent_id' => 3,
            'listshow' => false
        ]);
        //19
        Mistake::create([
            'name' => 'faringaal',
            'parent_id' => 3,
            'listshow' => false
        ]);
        //20
        Mistake::create([
            'name' => 'velaar',
            'parent_id' => 3,
            'listshow' => false
        ]);


        /*-----FONOLOGISCHE-----*/
        //21
        Mistake::create([
            'name' => 'Syllabestructuurprocessen',
            'parent_id' => 2,
            'listshow' => false
        ]);
        //22
        Mistake::create([
            'name' => 'Substitutieprocessen',
            'parent_id' => 2,
            'listshow' => false
        ]);
        //23
        Mistake::create([
            'name' => 'Harmonieprocessen',
            'parent_id' => 2,
            'listshow' => false
        ]);
        //24
        Mistake::create([
            'name' => 'Overige',
            'parent_id' => 2,
            'listshow' => false
        ]);

        /*Syllabestructuurprocessen*/
        //25
        Mistake::create([
            'name' => 'weglating finale consonant',
            'parent_id' => 21,
            'listshow' => false
        ]);
        //26
        Mistake::create([
            'name' => 'weglating initiale consonant',
            'parent_id' => 21,
            'listshow' => false
        ]);
        //27
        Mistake::create([
            'name' => 'clusterreductie',
            'parent_id' => 21,
            'listshow' => false
        ]);
        //28
        Mistake::create([
            'name' => 'epenthesis',
            'parent_id' => 21,
            'listshow' => false
        ]);
        //29
        Mistake::create([
            'name' => 'weglating onbeklemtoonde syllabe',
            'parent_id' => 21,
            'listshow' => false
        ]);
        //30
        Mistake::create([
            'name' => 'syllabecreatie',
            'parent_id' => 21,
            'listshow' => false
        ]);
        //31
        Mistake::create([
            'name' => 'coalescentie',
            'parent_id' => 21,
            'listshow' => false
        ]);
        //32
        Mistake::create([
            'name' => 'clustercreatie',
            'parent_id' => 21,
            'listshow' => false
        ]);
        //33
        Mistake::create([
            'name' => 'weglating beklemtoonde syllabe',
            'parent_id' => 21,
            'listshow' => false
        ]);

        /*Substitutieprocessen*/
        //34
        Mistake::create([
            'name' => 'fronting',
            'parent_id' => 22,
            'listshow' => false
        ]);
        //35
        Mistake::create([
            'name' => 'backing',
            'parent_id' => 22,
            'listshow' => false
        ]);
        //36
        Mistake::create([
            'name' => 'stopping',
            'parent_id' => 22,
            'listshow' => false
        ]);
        //37
        Mistake::create([
            'name' => 'fricatie',
            'parent_id' => 22,
            'listshow' => false
        ]);
        //38
        Mistake::create([
            'name' => 'gliding',
            'parent_id' => 22,
            'listshow' => false
        ]);
        //39
        Mistake::create([
            'name' => 'gliding(van fricatieven)',
            'parent_id' => 22,
            'listshow' => false
        ]);
        //40
        Mistake::create([
            'name' => 'denasalisatie',
            'parent_id' => 22,
            'listshow' => false
        ]);
        //41
        Mistake::create([
            'name' => 'h-satie',
            'parent_id' => 22,
            'listshow' => false
        ]);
        //42
        Mistake::create([
            'name' => 'verstemlozing',
            'parent_id' => 22,
            'listshow' => false
        ]);
        //43
        Mistake::create([
            'name' => 'nasalisatie',
            'parent_id' => 22,
            'listshow' => false
        ]);
        //44
        Mistake::create([
            'name' => 'depalatalisatie',
            'parent_id' => 22,
            'listshow' => false
        ]);
        //45
        Mistake::create([
            'name' => 'palatalisatie',
            'parent_id' => 22,
            'listshow' => false
        ]);
        //46
        Mistake::create([
            'name' => 'dentalisatie',
            'parent_id' => 22,
            'listshow' => false
        ]);
        //47
        Mistake::create([
            'name' => 'labialisatie',
            'parent_id' => 22,
            'listshow' => false
        ]);
        //48
        Mistake::create([
            'name' => 'vocalisatie',
            'parent_id' => 22,
            'listshow' => false
        ]);

        /*Harmonieprocessen*/
        //49
        Mistake::create([
            'name' => 'assimilatie',
            'parent_id' => 23,
            'listshow' => false
        ]);
        //50
        Mistake::create([
            'name' => 'reduplicatie',
            'parent_id' => 23,
            'listshow' => false
        ]);
        /*Overige*/
        //51
        Mistake::create([
            'name' => 'metathesis',
            'parent_id' => 24,
            'listshow' => false
        ]);
        //52
        Mistake::create([
            'name' => 'idiosyncratische en ongewone processen',
            'parent_id' => 24,
            'listshow' => false
        ]);
    }
}
