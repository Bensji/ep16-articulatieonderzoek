<?php

use Illuminate\Database\Seeder;
use App\Models\Phonetic;

class PhoneticHasMistakeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('phonetics_has_mistakes')->delete();
        $phonetics = Phonetic::all();

        foreach ($phonetics as $phonetic){

            $phm1 = array(
                'phonetic_id' => $phonetic->id,
                'mistake_id' => 1,
                'index' => 1
            );
            $phm2 = array(
                'phonetic_id' => $phonetic->id,
                'mistake_id' => 2,
                'index' => 2
            );
            $phm3 = array(
                'phonetic_id' => $phonetic->id,
                'mistake_id' => 3,
                'index' => 3
            );
            $phm4 = array(
                'phonetic_id' => $phonetic->id,
                'mistake_id' => 4,
                'index' => 4
            );
            $phm5 = array(
                'phonetic_id' => $phonetic->id,
                'mistake_id' => 5,
                'index' => 5
            );

            DB::table('phonetics_has_mistakes')->insert($phm1);
            DB::table('phonetics_has_mistakes')->insert($phm2);
            DB::table('phonetics_has_mistakes')->insert($phm3);
            DB::table('phonetics_has_mistakes')->insert($phm4);
            DB::table('phonetics_has_mistakes')->insert($phm5);
        }
    }
}
