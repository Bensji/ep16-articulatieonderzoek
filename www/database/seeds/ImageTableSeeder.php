<?php

use Illuminate\Database\Seeder;
use App\Models\Image;

class ImageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Image::create([
            'word' => 'appel',
            'url' => 'appel.png',
            'wordurl' => 'appel.mp3',
            'sentenceurl' => 'appel.mp3'
        ]);
        Image::create([
            'word' => 'brandweerwagen',
            'url' => 'brandweerwagen.png',
            'wordurl' => 'brandweerwagen.mp3',
            'sentenceurl' => 'brandweerwagen.mp3'
        ]);
        Image::create([
            'word' => 'bakker',
            'url' => 'bakker.png',
            'wordurl' => 'bakker.mp3',
            'sentenceurl' => 'bakker.mp3'
        ]);
        Image::create([
            'word' => 'deur',
            'url' => 'deur.png',
            'wordurl' => 'deur.mp3',
            'sentenceurl' => 'deur.mp3'
        ]);
        Image::create([
            'word' => 'fles',
            'url' => 'fles.png',
            'wordurl' => 'fles.mp3',
            'sentenceurl' => 'fles.mp3'
        ]);
        Image::create([
            'word' => 'glas',
            'url' => 'glas.png',
            'wordurl' => 'glas.mp3',
            'sentenceurl' => 'glas.mp3'
        ]);
        Image::create([
            'word' => 'heks',
            'url' => 'heks.png',
            'wordurl' => 'heks.mp3',
            'sentenceurl' => 'heks.mp3'
        ]);
        Image::create([
            'word' => 'helm',
            'url' => 'helm.png',
            'wordurl' => 'helm.mp3',
            'sentenceurl' => 'helm.mp3'
        ]);
    }
}
