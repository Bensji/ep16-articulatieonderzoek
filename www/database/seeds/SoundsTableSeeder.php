<?php

use Illuminate\Database\Seeder;
use App\Models\Sound;

class SoundsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sound::create(array(
            'key' => 'A',
            'key_code' => 97,
            'phonetic' => 'ɑ',
            'vowel' => true,
            'sound_class_id' => null,
            'articulation_place_id' => null,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'A',
            'key_code' => 97,
            'phonetic' => 'a',
            'vowel' => true,
            'sound_class_id' => null,
            'articulation_place_id' => null,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'Z',
            'key_code' => 122,
            'phonetic' => 'z',
            'vowel' => false,
            'sound_class_id' => 2,
            'articulation_place_id' => 3,
            'voiced' => true,
        ));
        Sound::create(array(
            'key' => 'Z',
            'key_code' => 122,
            'phonetic' => 'ʒ',
            'vowel' => false,
            'sound_class_id' => 2,
            'articulation_place_id' => 4,
            'voiced' => true,
        ));
        Sound::create(array(
            'key' => 'E',
            'key_code' => 101,
            'phonetic' => 'ɛ',
            'vowel' => true,
            'sound_class_id' => null,
            'articulation_place_id' => null,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'E',
            'key_code' => 101,
            'phonetic' => 'e',
            'vowel' => true,
            'sound_class_id' => null,
            'articulation_place_id' => null,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'E',
            'key_code' => 101,
            'phonetic' => 'ə',
            'vowel' => true,
            'sound_class_id' => null,
            'articulation_place_id' => null,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'E',
            'key_code' => 101,
            'phonetic' => 'Ø',
            'vowel' => true,
            'sound_class_id' => null,
            'articulation_place_id' => null,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'E',
            'key_code' => 101,
            'phonetic' => 'ɶɪ',
            'vowel' => true,
            'sound_class_id' => null,
            'articulation_place_id' => null,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'E',
            'key_code' => 101,
            'phonetic' => 'ɛɪ',
            'vowel' => true,
            'sound_class_id' => null,
            'articulation_place_id' => null,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'R',
            'key_code' => 114,
            'phonetic' => 'r',
            'vowel' => false,
            'sound_class_id' => 3,
            'articulation_place_id' => 3,
            'voiced' => true,
        ));
        Sound::create(array(
            'key' => 'R',
            'key_code' => 114,
            'phonetic' => 'R',
            'vowel' => false,
            'sound_class_id' => 3,
            'articulation_place_id' => 7,
            'voiced' => true,
        ));
        Sound::create(array(
            'key' => 'T',
            'key_code' => 116,
            'phonetic' => 't',
            'vowel' => false,
            'sound_class_id' => 1,
            'articulation_place_id' => 3,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'Y',
            'key_code' => 121,
            'phonetic' => 'ʌ',
            'vowel' => true,
            'sound_class_id' => null,
            'articulation_place_id' => null,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'Y',
            'key_code' => 121,
            'phonetic' => 'y',
            'vowel' => true,
            'sound_class_id' => null,
            'articulation_place_id' => null,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'U',
            'key_code' => 117,
            'phonetic' => 'u',
            'vowel' => true,
            'sound_class_id' => null,
            'articulation_place_id' => null,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'I',
            'key_code' => 105,
            'phonetic' => 'ɪ',
            'vowel' => true,
            'sound_class_id' => null,
            'articulation_place_id' => null,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'I',
            'key_code' => 105,
            'phonetic' => 'i',
            'vowel' => true,
            'sound_class_id' => null,
            'articulation_place_id' => null,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'O',
            'key_code' => 111,
            'phonetic' => 'ɔ',
            'vowel' => true,
            'sound_class_id' => null,
            'articulation_place_id' => null,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'O',
            'key_code' => 111,
            'phonetic' => 'o',
            'vowel' => true,
            'sound_class_id' => null,
            'articulation_place_id' => null,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'O',
            'key_code' => 111,
            'phonetic' => 'ɑu',
            'vowel' => true,
            'sound_class_id' => null,
            'articulation_place_id' => null,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'P',
            'key_code' => 112,
            'phonetic' => 'p',
            'vowel' => false,
            'sound_class_id' => 1,
            'articulation_place_id' => 1,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'S',
            'key_code' => 115,
            'phonetic' => 's',
            'vowel' => false,
            'sound_class_id' => 2,
            'articulation_place_id' => 3,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'S',
            'key_code' => 115,
            'phonetic' => 'ʃ',
            'vowel' => false,
            'sound_class_id' => 2,
            'articulation_place_id' => 4,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'D',
            'key_code' => 100,
            'phonetic' => 'd',
            'vowel' => false,
            'sound_class_id' => 1,
            'articulation_place_id' => 3,
            'voiced' => true,
        ));
        Sound::create(array(
            'key' => 'F',
            'key_code' => 102,
            'phonetic' => 'f',
            'vowel' => false,
            'sound_class_id' => 2,
            'articulation_place_id' => 2,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'G',
            'key_code' => 103,
            'phonetic' => 'χ',
            'vowel' => false,
            'sound_class_id' => 2,
            'articulation_place_id' => 6,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'G',
            'key_code' => 103,
            'phonetic' => 'ɣ',
            'vowel' => false,
            'sound_class_id' => 2,
            'articulation_place_id' => 6,
            'voiced' => true,
        ));
        Sound::create(array(
            'key' => 'G',
            'key_code' => 103,
            'phonetic' => 'g',
            'vowel' => false,
            'sound_class_id' => 1,
            'articulation_place_id' => 6,
            'voiced' => true,
        ));
        Sound::create(array(
            'key' => 'G',
            'key_code' => 103,
            'phonetic' => 'ŋ',
            'vowel' => false,
            'sound_class_id' => 5,
            'articulation_place_id' => 6,
            'voiced' => true,
        ));
        Sound::create(array(
            'key' => 'H',
            'key_code' => 104,
            'phonetic' => 'h',
            'vowel' => false,
            'sound_class_id' => 2,
            'articulation_place_id' => 8,
            'voiced' => true,
        ));
        Sound::create(array(
            'key' => 'J',
            'key_code' => 106,
            'phonetic' => 'j',
            'vowel' => false,
            'sound_class_id' => 6,
            'articulation_place_id' => 5,
            'voiced' => true,
        ));
        Sound::create(array(
            'key' => 'K',
            'key_code' => 107,
            'phonetic' => 'k',
            'vowel' => false,
            'sound_class_id' => 1,
            'articulation_place_id' => 6,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'L',
            'key_code' => 108,
            'phonetic' => 'l',
            'vowel' => false,
            'sound_class_id' => 4,
            'articulation_place_id' => 3,
            'voiced' => true,
        ));
        Sound::create(array(
            'key' => 'M',
            'key_code' => 109,
            'phonetic' => 'm',
            'vowel' => false,
            'sound_class_id' => 5,
            'articulation_place_id' => 1,
            'voiced' => true,
        ));
        Sound::create(array(
            'key' => 'M',
            'key_code' => 109,
            'phonetic' => 'ɱ',
            'vowel' => false,
            'sound_class_id' => 5,
            'articulation_place_id' => 2,
            'voiced' => true,
        ));
        Sound::create(array(
            'key' => 'W',
            'key_code' => 119,
            'phonetic' => 'w',
            'vowel' => false,
            'sound_class_id' => 6,
            'articulation_place_id' => 1,
            'voiced' => true,
        ));
        Sound::create(array(
            'key' => 'W',
            'key_code' => 119,
            'phonetic' => 'ʊ',
            'vowel' => false,
            'sound_class_id' => 6,
            'articulation_place_id' => 2,
            'voiced' => true,
        ));
        Sound::create(array(
            'key' => 'C',
            'key_code' => 99,
            'phonetic' => 'c',
            'vowel' => false,
            'sound_class_id' => 1,
            'articulation_place_id' => 4,
            'voiced' => false,
        ));
        Sound::create(array(
            'key' => 'V',
            'key_code' => 118,
            'phonetic' => 'v',
            'vowel' => false,
            'sound_class_id' => 2,
            'articulation_place_id' => 2,
            'voiced' => true,
        ));
        Sound::create(array(
            'key' => 'B',
            'key_code' => 98,
            'phonetic' => 'b',
            'vowel' => false,
            'sound_class_id' => 1,
            'articulation_place_id' => 1,
            'voiced' => true,
        ));
        Sound::create(array(
            'key' => 'N',
            'key_code' => 110,
            'phonetic' => 'n',
            'vowel' => false,
            'sound_class_id' => 5,
            'articulation_place_id' => 3,
            'voiced' => true,
        ));
        Sound::create(array(
            'key' => 'N',
            'key_code' => 110,
            'phonetic' => 'ɲ',
            'vowel' => false,
            'sound_class_id' => 5,
            'articulation_place_id' => 4,
            'voiced' => true,
        ));
        Sound::create(array(
            'key' => '?',
            'key_code' => 191,
            'phonetic' => 'ʔ',
            'vowel' => false,
            'sound_class_id' => 1,
            'articulation_place_id' => 8,
            'voiced' => false,
        ));
    }
}
