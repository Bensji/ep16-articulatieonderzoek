<?php

use Illuminate\Database\Seeder;
use App\Models\Agegroup;

class AgegroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Agegroup::create([
            'name' => 'Groep 1(2j6m-2j11m)',
            'minage' => 30,
            'maxage' => 35
        ]);
        Agegroup::create([
            'name' => 'Groep 2(3j-3j5m)',
            'minage' => 36,
            'maxage' => 41
        ]);
        Agegroup::create([
            'name' => 'Groep 3(3j6m-3j11m)',
            'minage' => 42,
            'maxage' => 47
        ]);
        Agegroup::create([
            'name' => 'Groep 4(4j-4j5m)',
            'minage' => 48,
            'maxage' => 53
        ]);
        Agegroup::create([
            'name' => 'Groep 5(4j6m-5j5m)',
            'minage' => 54,
            'maxage' => 65
        ]);
        Agegroup::create([
            'name' => 'Groep 6(5j6m-7j11m)',
            'minage' => 66,
            'maxage' => 95
        ]);
    }
}
