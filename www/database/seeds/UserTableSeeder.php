<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Faker\Factory as Faker;

class UserTableSeeder extends Seeder
{

    /**
     * @var string
     */
    public static $fakerLocale = 'nl_BE';

    /**
     * Maximum amount of items to seed.
     *
     * @var int
     */
    public static $maxItems = 10;

    /**
     * @var Faker
     */
    protected $faker = null;

    public function __construct()
    {
        $this->faker = Faker::create(self::$fakerLocale);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'givenname' => 'Superuser',
            'email' => 'admin@ao-r.be',
            'password' => 'yamu-Paqe6re',
            'street' => '-none-',
            'streetnumber' => '-none-',
            'city' => '-none-',
            'postalcode' => '0000',
            'role_id' => '4'
        ]);
        User::create([
            'name' => 'Payed',
            'givenname' => 'Superuser',
            'email' => 'payed@ao-r.be',
            'password' => 'testtest',
            'street' => '-none-',
            'streetnumber' => '-none-',
            'city' => '-none-',
            'postalcode' => '0000',
            'role_id' => '3'
        ]);

        // Faker
        // -----
        factory(User::class, self::$maxItems)->create();

    }
}
