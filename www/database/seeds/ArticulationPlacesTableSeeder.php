<?php

use Illuminate\Database\Seeder;
use App\Models\ArticulationPlace;

class ArticulationPlacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ArticulationPlace::create(array(
            'name' => 'bilabiaal'
        ));
        ArticulationPlace::create(array(
            'name' => 'labiodentaal'
        ));
        ArticulationPlace::create(array(
            'name' => 'alveolair'
        ));
        ArticulationPlace::create(array(
            'name' => 'prepalataal'
        ));
        ArticulationPlace::create(array(
            'name' => 'palataal'
        ));
        ArticulationPlace::create(array(
            'name' => 'velair'
        ));
        ArticulationPlace::create(array(
            'name' => 'uvulair'
        ));
        ArticulationPlace::create(array(
            'name' => 'glottaal'
        ));
    }
}
