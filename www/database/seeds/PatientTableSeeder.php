<?php

use Illuminate\Database\Seeder;
use App\Models\Patient;
use Faker\Factory as Faker;

class PatientTableSeeder extends Seeder
{
    /**
     * @var string
     */
    public static $fakerLocale = 'nl_BE';

    /**
     * Maximum amount of items to seed.
     *
     * @var int
     */
    public static $maxItems = 10;

    /**
     * @var Faker
     */
    protected $faker = null;

    public function __construct()
    {
        $this->faker = Faker::create(self::$fakerLocale);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //SEEDING OF FINALIZED APP
        Patient::create([
            'name' => 'Peeters',
            'givenname' => 'Jonas',
            'email' => 'pj@mail.com',
            'street' => 'Schoolstraat',
            'streetnumber' => '11',
            'city' => 'Gent',
            'postalcode' => '9000',
            'birthdate' => '2010-12-02',
            'sex' => 'f',
            'initials' => 'JD',
            'user_id' => '1'
        ]);

        // Faker
        // -----
        factory(Patient::class, self::$maxItems)->create();

    }
}
