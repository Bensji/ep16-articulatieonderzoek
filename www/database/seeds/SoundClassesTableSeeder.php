<?php

use Illuminate\Database\Seeder;
use App\Models\SoundClass;

class SoundClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SoundClass::create(array(
            'name' => 'occlusief',
            'code' => 'o'
        ));
        SoundClass::create(array(
            'name' => 'fricatief',
            'code' => 'f'
        ));
        SoundClass::create(array(
            'name' => 'trilklank',
            'code' => 'r'
        ));
        SoundClass::create(array(
            'name' => 'lateraal',
            'code' => 'l'
        ));
        SoundClass::create(array(
            'name' => 'nasaal',
            'code' => 'n'
        ));
        SoundClass::create(array(
            'name' => 'semivocaal',
            'code' => 'sv'
        ));
    }
}
