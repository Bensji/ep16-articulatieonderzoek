<?php

use Illuminate\Database\Seeder;
use App\Models\PhonologicalError;

class PhonologicalErrorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PhonologicalError::create([
            'phonological_category_id' => 1,
            'name' => 'del.init.cons.'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 1,
            'name' => 'del.fin.cons.'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 1,
            'name' => 'del.onbekl.syl.'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 1,
            'name' => 'del.bekl.syl.'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 1,
            'name' => 'syllabecreatie'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 1,
            'name' => 'clusterreductie'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 1,
            'name' => 'clusterdeletie'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 1,
            'name' => 'clustercreatie'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 1,
            'name' => 'epenthesis'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 1,
            'name' => 'coalescentie'
        ]);



        PhonologicalError::create([
            'phonological_category_id' => 2,
            'name' => 'fronting'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 2,
            'name' => 'backing'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 2,
            'name' => 'stopping'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 2,
            'name' => 'fricatie'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 2,
            'name' => 'nasalisatie'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 2,
            'name' => 'denasalisatie'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 2,
            'name' => 'voicing'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 2,
            'name' => 'devoicing'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 2,
            'name' => 'gliding'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 2,
            'name' => 'lateralisatie'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 2,
            'name' => 'affricatie'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 2,
            'name' => 'H-satie'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 2,
            'name' => 'vocalisatie'
        ]);


        PhonologicalError::create([
            'phonological_category_id' => 3,
            'name' => 'assimilatieprocessen'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 3,
            'name' => 'reduplicatie'
        ]);


        PhonologicalError::create([
            'phonological_category_id' => 4,
            'name' => 'metathesis'
        ]);
        PhonologicalError::create([
            'phonological_category_id' => 4,
            'name' => 'idiosyncratische'
        ]);
    }
}
