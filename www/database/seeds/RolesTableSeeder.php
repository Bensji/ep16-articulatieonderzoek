<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'Registered',
            'description' => 'Een geregistreerde user die nog moet worden geaccepteeerd.'
        ]);
        Role::create([
            'name' => 'Free',
            'description' => 'Deze user heeft enkel toegang tot het gratis gedeelte van de website.'
        ]);
        Role::create([
            'name' => 'Payed',
            'description' => 'Deze user heeft volledige toegang tot de website.'
        ]);
        Role::create([
            'name' => 'Admin',
            'description' => 'Beheerder van de website.'
        ]);
    }
}
