<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $tables = [
            //User & Roles
            'Roles',
            'User',
            //Patients
            'Patient',
            'SoundClasses',
            'ArticulationPlaces',
            'Sounds',
            //Images
            'Image',
            'ImageHasSounds',
            //Moduletypes
            'Moduletype',
            //Modules
            'Module',
            'ModulesHasImages',
            //Phonetics
            //'Phonetics',
            //Mistake
            //'Mistake',
            //Phonetichasmistake
            //'PhoneticHasMistake',
            //Tests
            'Test',
            //'Testmistake',
            'Testrecordings',
            //Substitutions
            //'Subs',
            //Agegroup
            'Agegroup',
            'Distortion',
            'PhonologicalCategories',
            //Error
            'PhonologicalError',
            //'PhoneticError'
        ];

        $i = 0;
        foreach ($tables as $table) {
            $class = "${table}TableSeeder";
            $count = sprintf('%02d', ++$i);
            $this->command->getOutput()->writeln("<comment>Seed${count}:</comment> ${class}…");
            $this->call($class);
        }

        Model::reguard();
    }
}
