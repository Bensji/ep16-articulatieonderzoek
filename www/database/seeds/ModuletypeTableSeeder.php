<?php

use Illuminate\Database\Seeder;
use App\Models\Moduletype;

class ModuletypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Moduletype::create([
            'name' => 'Benoemtaak',
            'description' => 'Er wordt een figuur getoond die moet benoemd worden. Indien dat niet meteen lukt kan een aanvulzin of nazegwoord worden afgespeeld.'
        ]);
    }
}
