<?php

use Illuminate\Database\Seeder;
use App\Models\SoundClass;
use App\Models\PhoneticError;
use App\Models\Sound;

class PhoneticErrorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$klassen = ['occlusief', 'fricatief', 'nasaal', 'semivocaal'];
        $klassen = SoundClass::all();

        foreach ($klassen as $klasse){
            $klasseNaam = $klasse->name;

            $inKlasse = $klasse->sounds;
            $nietInKlasse = $klasse->soundsNotInThisClass()
            ;

            $velairenInKlasse = [];
            $nietVelairenInKlasse = [];
            foreach ($inKlasse as $sound){
                if ($sound->articulationPlace->name == 'velair'){
                    array_push($velairenInKlasse, $sound);
                } else {
                    array_push($nietVelairenInKlasse, $sound);
                }
            }

            foreach ($velairenInKlasse as $replacedSound){
                foreach ($inKlasse as $replacingSound){
                    if ($replacedSound->id != $replacingSound->id){
                        PhoneticError::create(array(
                            'replaced_sound_id' => $replacedSound->id,
                            'replacing_sound_id' => $replacingSound->id,
                            'name' => 'substitutie van een velaire ' . $klasseNaam . ' binnen dezelfde klankklasse',
                            'code' => 's_v'
                        ));
                    }
                }
                foreach ($nietInKlasse as $replacingSound){
                    PhoneticError::create(array(
                        'replaced_sound_id' => $replacedSound->id,
                        'replacing_sound_id' => $replacingSound->id,
                        'name' => 'substitutie van een velaire ' . $klasseNaam,
                        'code' => 's_v' . $klasse->code
                    ));
                }
                PhoneticError::create(array(
                    'replaced_sound_id' => $replacedSound->id,
                    'replacing_sound_id' => null,
                    'name' => 'omissie van een velaire ' . $klasseNaam,
                    'code' => 'o_v' . $klasse->code
                ));
            }

            foreach ($nietVelairenInKlasse as $replacedSound){
                foreach ($inKlasse as $replacingSound){
                    if ($replacedSound->id != $replacingSound->id){
                        PhoneticError::create(array(
                            'replaced_sound_id' => $replacedSound->id,
                            'replacing_sound_id' => $replacingSound->id,
                            'name' => 'substitutie van een (niet velaire) ' . $klasseNaam . ' binnen dezelfde klankklasse',
                            'code' => 's_'
                        ));
                    }
                }
                foreach ($nietInKlasse as $replacingSound){
                    PhoneticError::create(array(
                        'replaced_sound_id' => $replacedSound->id,
                        'replacing_sound_id' => $replacingSound->id,
                        'name' => 'substitutie van een (niet velaire) ' . $klasseNaam,
                        'code' => 's_' . $klasse->code
                    ));
                }
                PhoneticError::create(array(
                    'replaced_sound_id' => $replacedSound->id,
                    'replacing_sound_id' => null,
                    'name' => 'omissie van een (niet velaire) ' . $klasseNaam,
                    'code' => 'o_' . $klasse->code
                ));
            }
        }

        $s = Sound::where('phonetic', '=', 's')->first();
        $r = Sound::where('phonetic', '=', 'r')->first();
        $allOthers = Sound::whereNotIn('id', [$s->id, $r->id])
            ->get();

        $algemeneDistorsies = [
            [
                'code' => 'dlab',
                'name' => 'labialisatie'
            ],[
                'code' => 'ddlab',
                'name' => 'delabialisatie'
            ],[
                'code' => 'dint',
                'name' => 'interdentalisatie'
            ],[
                'code' => 'ddent',
                'name' => 'dentalisatie'
            ],[
                'code' => 'dalv',
                'name' => 'alveolarisatie'
            ],[
                'code' => 'dpal',
                'name' => 'palatalisatie'
            ],[
                'code' => 'dvel',
                'name' => 'velarisatie'
            ],[
                'code' => 'dlat',
                'name' => 'lateralisatie'
            ],[
                'code' => 'dnas',
                'name' => 'nasalisatie'
            ],[
                'code' => 'ddnas',
                'name' => 'denasalisatie'
            ],[
                'code' => 'dist',
                'name' => 'distorsie'
            ]
        ];

        foreach ($allOthers as $sound){
            for ($i=0; $i<count($algemeneDistorsies); $i++){
                PhoneticError::create(array(
                    'replaced_sound_id' => $sound->id,
                    'replacing_sound_id' => $sound->id,
                    'name' => $algemeneDistorsies[$i]['name'],
                    'code' => $algemeneDistorsies[$i]['code']
                ));
            }
        }

        $sDistorsies = [
            [
                'code' => 'dsint',
                'name' => 'sigmatismus interdentalis'
            ],[
                'code' => 'dsadd',
                'name' => 'sigmatismus addentalis'
            ],[
                'code' => 'dsstr',
                'name' => 'sigmatismus stridens'
            ],[
                'code' => 'dslat',
                'name' => 'sigmatismus lateralis'
            ],[
                'code' => 'dsltf',
                'name' => 'sigmatismus lateroflexus'
            ],[
                'code' => 'dsnas',
                'name' => 'sigmatismus nasalis'
            ],[
                'code' => 'dsand',
                'name' => 'sigmatismus labialis, labiodentalis, palatalis'
            ]
        ];

        for ($i=0; $i<count($sDistorsies); $i++){
            PhoneticError::create(array(
                'replaced_sound_id' => $s->id,
                'replacing_sound_id' => $s->id,
                'name' => $sDistorsies[$i]['name'],
                'code' => $sDistorsies[$i]['code']
            ));
        }

        $rDistorsies = [
            [
                'code' => 'drns',
                'name' => 'rhotacismus non sonans'
            ],[
                'code' => 'dRns',
                'name' => 'rhot uvul non sonans'
            ],[
                'code' => 'drnv',
                'name' => 'rhotacismus non vibrans'
            ],[
                'code' => 'dRnv',
                'name' => 'rhot uvul non vibrans'
            ],[
                'code' => 'drint',
                'name' => 'rhotacismus interdentalis'
            ],[
                'code' => 'drltf',
                'name' => 'rhotacismus lateroflexus'
            ],[
                'code' => 'drnas',
                'name' => 'rhotacismus nasalis'
            ],[
                'code' => 'dRnas',
                'name' => 'rhot uvul nasalis'
            ],[
                'code' => 'drand',
                'name' => 'ander rotacisme'
            ]
        ];

        for ($i=0; $i<count($rDistorsies); $i++){
            PhoneticError::create(array(
                'replaced_sound_id' => $r->id,
                'replacing_sound_id' => $r->id,
                'name' => $rDistorsies[$i]['name'],
                'code' => $rDistorsies[$i]['code']
            ));
        }

        $allSounds = Sound::all();

        foreach($allSounds as $sound){
            PhoneticError::create(array(
                'replaced_sound_id' => null,
                'replacing_sound_id' => $sound->id,
                'name' => 'additie',
                'code' => 'nvt'
            ));
        }
    }
}
