<?php

use Illuminate\Database\Seeder;
use App\Models\Phonetic;

class SubsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('phonetic_has_phonetics')->delete();
        $phonetics = Phonetic::all();

        foreach ($phonetics as $phonetic){

            $phm1 = array(
                'phonetic_id' => $phonetic->id,
                'phonetic_id_mistake' => 1,
                'index' => 1
            );
            $phm2 = array(
                'phonetic_id' => $phonetic->id,
                'phonetic_id_mistake' => 2,
                'index' => 2
            );
            $phm3 = array(
                'phonetic_id' => $phonetic->id,
                'phonetic_id_mistake' => 3,
                'index' => 3
            );
            $phm4 = array(
                'phonetic_id' => $phonetic->id,
                'phonetic_id_mistake' => 4,
                'index' => 4
            );

            DB::table('phonetic_has_phonetics')->insert($phm1);
            DB::table('phonetic_has_phonetics')->insert($phm2);
            DB::table('phonetic_has_phonetics')->insert($phm3);
            DB::table('phonetic_has_phonetics')->insert($phm4);
        }
    }
}
