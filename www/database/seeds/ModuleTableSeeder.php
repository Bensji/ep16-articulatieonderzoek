<?php

use Illuminate\Database\Seeder;
use App\Models\Module;

class ModuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Module::create([
        //     'name' => 'Standaardmodule',
        //     'description' => 'Dit is de standaardmodule',
        //     'moduletype_id' => '1'

        // ]);
        Module::create([
            'name' => 'Demo',
            'description' => 'Demonstration',
            'moduletype_id' => 1
        ]);
    }
}
