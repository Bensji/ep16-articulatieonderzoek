<?php

use Illuminate\Database\Seeder;
use App\Models\Phonetic;

class PhoneticsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* VOWELS */
        Phonetic::create([
            'sign' => 'a',
            'vowel' => TRUE
        ]);

        Phonetic::create([
            'sign' => 'e',
            'vowel' => TRUE
        ]);

        Phonetic::create([
            'sign' => 'u',
            'vowel' => TRUE
        ]);

        Phonetic::create([
            'sign' => 'i',
            'vowel' => TRUE
        ]);

        Phonetic::create([
            'sign' => 'o',
            'vowel' => TRUE
        ]);
        Phonetic::create([
            'sign' => 'ɘ',
            'vowel' => TRUE
        ]);
        Phonetic::create([
            'sign' => 'ɑ',
            'vowel' => TRUE
        ]);
        Phonetic::create([
            'sign' => 'ɛ',
            'vowel' => TRUE
        ]);
        Phonetic::create([
            'sign' => 'ɪ',
            'vowel' => TRUE
        ]);
        Phonetic::create([
            'sign' => 'ɔ',
            'vowel' => TRUE
        ]);
        Phonetic::create([
            'sign' => 'ʌ',
            'vowel' => TRUE
        ]);
        Phonetic::create([
            'sign' => 'ɸ',
            'vowel' => TRUE
        ]);
        Phonetic::create([
            'sign' => 'ɛɪ',
            'vowel' => TRUE
        ]);
        Phonetic::create([
            'sign' => 'ɔu',
            'vowel' => TRUE
        ]);
        Phonetic::create([
            'sign' => 'œy',
            'vowel' => TRUE
        ]);

        /*CONSONANTS*/

        Phonetic::create([
            'sign' => 'b'
        ]);
        Phonetic::create([
            'sign' => 'c'
        ]);
        Phonetic::create([
            'sign' => 'd'
        ]);
        Phonetic::create([
            'sign' => 'f'
        ]);
        Phonetic::create([
            'sign' => 'g'
        ]);
        Phonetic::create([
            'sign' => 'h'
        ]);
        Phonetic::create([
            'sign' => 'j'
        ]);
        Phonetic::create([
            'sign' => 'k'
        ]);
        Phonetic::create([
            'sign' => 'l'
        ]);
        Phonetic::create([
            'sign' => 'm'
        ]);
        Phonetic::create([
            'sign' => 'n'
        ]);
        Phonetic::create([
            'sign' => 'p'
        ]);
        Phonetic::create([
            'sign' => 'q'
        ]);
        Phonetic::create([
            'sign' => 'r'
        ]);
        Phonetic::create([
            'sign' => 's'
        ]);
        Phonetic::create([
            'sign' => 't'
        ]);
        Phonetic::create([
            'sign' => 'v'
        ]);
        Phonetic::create([
            'sign' => 'w'
        ]);
        Phonetic::create([
            'sign' => 'x'
        ]);
        Phonetic::create([
            'sign' => 'y'
        ]);
        Phonetic::create([
            'sign' => 'z'
        ]);
        Phonetic::create([
            'sign' => 'χ'
        ]);
        Phonetic::create([
            'sign' => 'ɣ'
        ]);
        Phonetic::create([
            'sign' => 'ŋ'
        ]);
        Phonetic::create([
            'sign' => 'ʃ'
        ]);
        Phonetic::create([
            'sign' => 'ʒ'
        ]);
    }
}
