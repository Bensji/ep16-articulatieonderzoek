<?php

use Illuminate\Database\Seeder;
use App\Models\Sound;
use App\Models\Distortion;

class DistortionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $s = Sound::where('phonetic', '=', 's')->first();
        $r = Sound::where('phonetic', '=', 'r')->first();
        $allOthers = Sound::whereNotIn('id', [$s->id, $r->id])
            ->get();

        $algemeneDistorsies = [
            [
                'code' => 'dlab',
                'name' => 'labialisatie'
            ],[
                'code' => 'ddlab',
                'name' => 'delabialisatie'
            ],[
                'code' => 'dint',
                'name' => 'interdentalisatie'
            ],[
                'code' => 'ddent',
                'name' => 'dentalisatie'
            ],[
                'code' => 'dalv',
                'name' => 'alveolarisatie'
            ],[
                'code' => 'dpal',
                'name' => 'palatalisatie'
            ],[
                'code' => 'dvel',
                'name' => 'velarisatie'
            ],[
                'code' => 'dlat',
                'name' => 'lateralisatie'
            ],[
                'code' => 'dnas',
                'name' => 'nasalisatie'
            ],[
                'code' => 'ddnas',
                'name' => 'denasalisatie'
            ],[
                'code' => 'dist',
                'name' => 'distorsie'
            ]
        ];

        foreach ($allOthers as $sound){
            for ($i=0; $i<count($algemeneDistorsies); $i++){
                Distortion::create(array(
                    'sound_id' => $sound->id,
                    'name' => $algemeneDistorsies[$i]['name'],
                    'code' => $algemeneDistorsies[$i]['code']
                ));
            }
        }

        $sDistorsies = [
            [
                'code' => 'dsint',
                'name' => 'sigmatismus interdentalis'
            ],[
                'code' => 'dsadd',
                'name' => 'sigmatismus addentalis'
            ],[
                'code' => 'dsstr',
                'name' => 'sigmatismus stridens'
            ],[
                'code' => 'dslat',
                'name' => 'sigmatismus lateralis'
            ],[
                'code' => 'dsltf',
                'name' => 'sigmatismus lateroflexus'
            ],[
                'code' => 'dsnas',
                'name' => 'sigmatismus nasalis'
            ],[
                'code' => 'dsand',
                'name' => 'sigmatismus labialis, labiodentalis, palatalis'
            ]
        ];

        for ($i=0; $i<count($sDistorsies); $i++){
            Distortion::create(array(
                'sound_id' => $s->id,
                'name' => $sDistorsies[$i]['name'],
                'code' => $sDistorsies[$i]['code']
            ));
        }

        $rDistorsies = [
            [
                'code' => 'drns',
                'name' => 'rhotacismus non sonans'
            ],[
                'code' => 'dRns',
                'name' => 'rhot uvul non sonans'
            ],[
                'code' => 'drnv',
                'name' => 'rhotacismus non vibrans'
            ],[
                'code' => 'dRnv',
                'name' => 'rhot uvul non vibrans'
            ],[
                'code' => 'drint',
                'name' => 'rhotacismus interdentalis'
            ],[
                'code' => 'drltf',
                'name' => 'rhotacismus lateroflexus'
            ],[
                'code' => 'drnas',
                'name' => 'rhotacismus nasalis'
            ],[
                'code' => 'dRnas',
                'name' => 'rhot uvul nasalis'
            ],[
                'code' => 'drand',
                'name' => 'ander rotacisme'
            ]
        ];

        for ($i=0; $i<count($rDistorsies); $i++){
            Distortion::create(array(
                'sound_id' => $r->id,
                'name' => $rDistorsies[$i]['name'],
                'code' => $rDistorsies[$i]['code']
            ));
        }
    }
}
