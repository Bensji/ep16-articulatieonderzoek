<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 255);
			$table->string('givenname', 255);
			$table->string('email', 255)->unique();
			$table->string('password', 60);
			$table->string('street', 255);
			$table->string('streetnumber', 255);
			$table->string('city', 255);
			$table->string('postalcode', 255);
			$table->timestamps();
			$table->softDeletes();
			$table->timestamp('locked')->nullable();
			$table->unsignedInteger('role_id')->default(1);
			$table->foreign('role_id')->references('id')->on('roles');
			$table->string('remember_token', 100)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
