<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tests', function(Blueprint $table)
		{
			$table->increments('id');
			$table->boolean('forstats')->default(false);
			$table->unsignedinteger('indexend')->nullable();
			$table->timestamp('ended')->nullable();
			$table->timestamp('analyzed')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->unsignedInteger('module_id');
            $table->foreign('module_id')->references('id')->on('modules');
            $table->unsignedInteger('patient_id');
            $table->foreign('patient_id')->references('id')->on('patients');
            $table->unsignedInteger('agegroup_id')->nullable();
            $table->foreign('agegroup_id')->references('id')->on('agegroups');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tests');
	}

}
