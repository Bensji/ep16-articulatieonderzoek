<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PatientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('patients', function(Blueprint $table)
		{

			//TABLE FOR FINALIZED APP
			$table->increments('id');
			$table->string('name', 255);
			$table->string('givenname', 255);
			$table->string('street', 255);
			$table->string('streetnumber', 255);
			$table->string('city', 255);
			$table->string('postalcode', 255);
			$table->string('email', 255)->nullable();
			$table->char('sex', 1);
			$table->date('birthdate');
			$table->char('initials', 3);
			$table->timestamps();
			$table->softDeletes();
			$table->unsignedInteger('user_id');
			$table->foreign('user_id')->references('id')->on('users');

		
			/*TABLE FOR TESTING WITH STUDENTS
			$table->increments('id');
			$table->string('name', 10);
			$table->char('sex', 1);
			$table->char('postalcode', 4);
			$table->char('initials', 3);
			$table->char('user_code',2);
			$table->date('birthdate');
			$table->timestamps();
			$table->softDeletes();
			$table->unsignedInteger('user_id');
			$table->foreign('user_id')->references('id')->on('users');*/
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('patients');
	}

}
