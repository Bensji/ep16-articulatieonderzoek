<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhonologicalErrors extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('phonological_errors', function($table){
            $table->increments('id');
            $table->string('name', 150);
            $table->unsignedInteger('phonological_category_id');
            $table->foreign('phonological_category_id')->references('id')->on('phonological_categories');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('phonological_errors', function($table){
            $table->dropForeign('phonological_errors_phonological_category_id_foreign');
        });
        Schema::drop('phonological_errors');
	}

}
