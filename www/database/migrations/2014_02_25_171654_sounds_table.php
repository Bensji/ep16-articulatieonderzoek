<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SoundsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sounds', function($table){
            $table->increments('id');
            $table->string('key', 1);
            $table->unsignedInteger('key_code');
            $table->string('phonetic', 3);
            $table->boolean('vowel');
            $table->unsignedInteger('sound_class_id')->nullable();
            $table->foreign('sound_class_id')->references('id')->on('sound_classes');
            $table->unsignedInteger('articulation_place_id')->nullable();
            $table->foreign('articulation_place_id')->references('id')->on('articulation_places');
            $table->boolean('voiced');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sounds', function($table){
            $table->dropForeign('sounds_sound_class_id_foreign');
            $table->dropForeign('sounds_articulation_place_id_foreign');
        });
        Schema::drop('sounds');
	}

}
