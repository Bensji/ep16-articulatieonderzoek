<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Distortions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('distortions', function($table){
            $table->increments('id');
            $table->unsignedInteger('sound_id');
            $table->foreign('sound_id')->references('id')->on('sounds');
            $table->string('name', 100);
            $table->string('code', 10);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('distortions', function($table){
            $table->dropForeign('distortions_sound_id_foreign');
        });
		Schema::drop('distortions');
	}

}
