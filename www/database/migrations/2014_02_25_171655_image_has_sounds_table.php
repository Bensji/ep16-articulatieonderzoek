<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImageHasSoundsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('image_has_sounds', function($table){
            $table->increments('id');
            $table->unsignedInteger('sound_id');
            $table->foreign('sound_id')->references('id')->on('sounds');
            $table->unsignedInteger('image_id');
            $table->foreign('image_id')->references('id')->on('images');
            $table->unsignedInteger('index');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('image_has_sounds', function($table){
            $table->dropForeign('image_has_sounds_sound_id_foreign');
            $table->dropForeign('image_has_sounds_image_id_foreign');
        });
        Schema::drop('image_has_sounds');
	}

}
