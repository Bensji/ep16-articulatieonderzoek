<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TestrecordingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('testrecordings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('filename', 255);
            $table->boolean('answered')->default(true);
			$table->boolean('hasMistake')->default(false);
			$table->boolean('wordplay')->default(false);
			$table->boolean('sentenceplay')->default(false);
			$table->timestamps();
			$table->softDeletes();
			$table->unsignedInteger('test_id');
            $table->foreign('test_id')->references('id')->on('tests');
            $table->unsignedInteger('image_id');
            $table->foreign('image_id')->references('id')->on('images');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('testrecordings');
	}

}
