<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhoneticAnalysisTranscriptions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('phonetic_analysis_transcriptions', function($table){
            $table->increments('id');
            $table->unsignedInteger('test_id');
            $table->foreign('test_id')->references('id')->on('tests');
            $table->unsignedInteger('image_id');
            $table->foreign('image_id')->references('id')->on('images');
            $table->unsignedInteger('sound_index');
            $table->unsignedInteger('sound_id')->nullable();
            $table->foreign('sound_id')->references('id')->on('sounds');
            $table->unsignedInteger('addition_order')->nullable();
            $table->unsignedInteger('distortion_id')->nullable();
            $table->foreign('distortion_id')->references('id')->on('distortions');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('phonetic_analysis_transcriptions', function($table){
            $table->dropForeign('phonetic_analysis_transcriptions_test_id_foreign');
            $table->dropForeign('phonetic_analysis_transcriptions_image_id_foreign');
            $table->dropForeign('phonetic_analysis_transcriptions_sound_id_foreign');
            $table->dropForeign('phonetic_analysis_transcriptions_distortion_id_foreign');
        });
        Schema::drop('phonetic_analysis_transcriptions');
	}

}
