<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TestrecordingHasPhonologicalError extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('testrecordings_phonological_errors', function($table){
            $table->increments('id');
            $table->unsignedInteger('testrecording_id');
            $table->foreign('testrecording_id')->references('id')->on('testrecordings');
            $table->unsignedInteger('phonological_error_id');
            $table->foreign('phonological_error_id')->references('id')->on('phonological_errors');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('testrecordings_phonological_errors', function($table){
            $table->dropForeign('testrecordings_phonological_errors_testrecording_id_foreign');
            $table->dropForeign('testrecordings_phonological_errors_phonological_error_id_foreign');
        });
        Schema::drop('testrecordings_phonological_errors');
	}

}
