<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => ['web']], function () {

    Debugbar::disable();

    // Standaard route
    Route::get('/', function () {
        return view('index');
    });

    //Route to login(post)
    Route::post('/login', [
        'as'   => 'login',
        'uses' => 'Auth\AccountController@auth'
    ]);
    Route::get('/check', [
        'as'   => 'check',
        'uses' => 'Auth\AccountController@check'
    ]);

    //Route to register(post)
    Route::post('/register', [
        'as' => 'register',
        'uses' => 'Auth\AccountController@Store'
    ]);

    //Route to logout
    Route::get('/logout', [
        'as' => 'logout',
        'uses' => 'Auth\AccountController@Logout'
    ]);

    //Route to register(post)
    Route::post('/reset', [
        'as' => 'reset',
        'uses' => 'Auth\AccountController@Resetpost'
    ]);

    //Route to reset
    Route::get('/mail', [
        'as' => 'mail',
        'uses' => 'Auth\AccountController@Mail'
    ]);

    Route::group([

        //'prefix' => 'api'

    ], function(){

        Route::group(['middleware' => 'auth'], function()
        {
            Route::get('/home', [
                'as' => 'home',
                'uses' => 'HomeController@Index'
            ]);

            Route::resource('child', 'ChildController');
            Route::resource('test', 'TestController');
            Route::get('/test/testing/{test}', [
                'as' => 'test.test',
                'uses' => 'TestController@Test'
            ]);

            Route::post('/test/testing/{test}', [
                'as' => 'test.test.store',
                'uses' => 'TestController@Teststore'
            ]);
            Route::post('/test/analyze/{test}', [
                'as' => 'test.analize.store',
                'uses' => 'TestController@Testanalyze'
            ]);
            Route::get('/test/analyzed/{test}', [
                'as' => 'test.analized.update',
                'uses' => 'TestController@Testanalyzed'
            ]);


            Route::get('/api/phonetics/phonetics', 'Api\PhoneticsController@getPhonetics');
            Route::get('/api/analysis/categories/', 'Api\AnalysisController@getCategories');
            Route::get('/api/analysis/phonetic-errors/', 'Api\AnalysisController@getPhoneticErrors');
            //Route::controller('/api/module', 'Admin\ApiModuleController');
            Route::get('/api/image/images/', 'Api\ImageController@getImages');
            Route::get('testapi/skipped-test-images/', 'Api\TestController@getSkippedTestImages');
            Route::get('testapi/test-images/', 'Api\TestController@getTestImages');
            Route::post('testapi/test-data/', 'Api\TestController@postTestData');
            Route::post('testapi/test-analysis/', 'Api\TestController@postTestAnalysis');
            Route::post('testapi/in-test-analysis/', 'Api\TestController@postInTestAnalysis');
            Route::get('testapi/image-analysis-data/', 'Api\TestController@getImageAnalysisData');
            Route::get('testapi/image-data/', 'Api\TestController@getImageData');
            Route::get('testapi/phonological-error-data/', 'Api\TestController@getPhonologicalErrorData');
            Route::post('testapi/in-test-analysis-data/', 'Api\TestController@postInTestAnalysisData');
            Route::get('testapi/unanswered/', 'Api\TestController@getUnanswered');


            Route::group(['middleware' => 'admin'], function()
            {
                Route::get('admin/check', [
                    'as'   => 'admin/check',
                    'uses' => 'Auth\AccountController@checkAdmin'
                ]);

                Route::get('/admin', [
                    'as' => 'admin',
                    'uses' => 'Admin\AdminController@Index'
                ]);

                Route::get('/admin/getroles', [
                    'as' => 'admin/getroles',
                    'uses' => 'Admin\UserController@getRoles'
                ]);

                Route::resource('admin/user', 'Admin\UserController');

                Route::put('admin/user/restore/{id}', [
                    'as'    => 'admin.user.restore',
                    'uses'  => 'Admin\UserController@restore'
                ]);

                Route::put('admin/user/lock/{id}', [
                    'as'    => 'admin.user.lock',
                    'uses'  => 'Admin\UserController@lock'
                ]);

                Route::put('admin/user/unlock/{id}', [
                    'as'    => 'admin.user.unlock',
                    'uses'  => 'Admin\UserController@unlock'
                ]);

                Route::resource('admin/child', 'Admin\ChildController');

                Route::put('admin/child/restore/{id}', [
                    'as'    => 'admin.child.restore',
                    'uses'  => 'Admin\ChildController@restore'
                ]);

                Route::resource('admin/image', 'Admin\ImageController');

                Route::put('admin/image/restore/{id}', [
                    'as'    => 'admin.image.restore',
                    'uses'  => 'Admin\ImageController@restore'
                ]);

                Route::resource('admin/module', 'Admin\ModuleController');

                Route::put('admin/module/restore/{id}', [
                    'as'    => 'admin.module.restore',
                    'uses'  => 'Admin\ModuleController@restore'
                ]);

                Route::resource('admin/mistake', 'Admin\MistakeController');

                // Possibility to ass most frequent mistakes. Not in final app(not usefull code still in views and controller if needed)
                // Route::resource('admin/phonetic', 'Admin_PhoneticController');

                Route::resource('admin/subs', 'Admin\SubsController');
            });

        });

    });

});
