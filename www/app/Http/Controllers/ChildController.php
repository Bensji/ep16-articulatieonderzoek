<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreChildRequest;
use App\Models\Patient;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Response;

class ChildController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = Auth::user();
		$children = Patient::where('user_id', '=', $user->id)->get();
        return response()->json(['user' => $user, 'children' => $children]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
     * @param  StoreChildRequest  $request
	 * @return Response
	 */
	public function store(StoreChildRequest $request)
	{
		$user = Auth::user();
        $request->merge(['user_id' => $user->id]);
        if($child = new Patient($request->all()))
        {
            $child->save();
            return response()->json(['succes' => 'Kind aangemaakt.']);
        }
        else
        {
            return response()
            ->json($request->getMessageBag()->toArray());
        }

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $user = Auth::user();
		$userId = Auth::id();
		$child = Patient::find($id);
		if($userId == $child->user_id)
		{
            return response()->json(['user' => $user, 'child'=> $child ]);
		}else
		{
            return response()->json(['error' => 'Je bent niet de logopedist van deze persoon.']);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = Auth::user();
        $userId = Auth::id();
		$editChild = Patient::find($id);
		if($userId == $editChild->user_id)
		{
			$editChild->birthdate = date("d-m-Y", strtotime($editChild->birthdate) );

            return response()->json(['user' => $user, 'child'=> $editChild ]);
		}else{
            return response()->json(['error' => 'Je bent niet de logopedist van deze persoon.']);
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  StoreChildRequest  $request int  $id
	 * @return Response
	 */
	public function update(StoreChildRequest $request, $id)
	{
        $user = Auth::user();
        $editChild = Patient::findOrFail($id);
        $request->merge(['user_id' => $user->id]);
        if($dataUpdate = $request->all())
        {
            $editChild->update($dataUpdate);
            return response()->json(['succes' => 'Kind aangepast.']);
        }
        else
        {
            return response()
                ->json($request->getMessageBag()->toArray());
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = Auth::user();
		$child = Patient::find($id);
		
        if($user->id == $child->user_id)
		{
	        $child->delete();
            return response()->json(['succes' => 'Kind verwijderd.']);
		}else
		{
            return response()->json(['error' => 'Kind niet verwijderd.']);
		}
	}

}