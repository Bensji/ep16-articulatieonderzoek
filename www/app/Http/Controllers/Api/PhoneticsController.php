<?php
/**
 * Created by PhpStorm.
 * User: BenjaminLierman
 * Date: 8/03/16
 * Time: 20:59
 */

namespace App\Http\Controllers\Api;

use App\Models\Sound;

use App\Http\Controllers\Controller as Controller;

class PhoneticsController extends Controller
{
    public function getPhonetics(){
        $phonetics = Sound::all();
        echo json_encode($phonetics);
    }
}