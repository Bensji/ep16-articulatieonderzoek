<?php
/**
 * Created by PhpStorm.
 * User: BenjaminLierman
 * Date: 8/03/16
 * Time: 20:57
 */

namespace App\Http\Controllers\Api;

use App\Models\PhonologicalCategory;

use App\Http\Controllers\Controller as Controller;

class AnalysisController extends Controller
{
    public function getCategories(){

    }

    public function getPhoneticErrors(){
        $data = PhonologicalCategory
            ::with(array('errors' => function($query){
                //$query->lists('name', 'id');
            }))
            ->get()
        ;
        return json_encode($data);
    }
}