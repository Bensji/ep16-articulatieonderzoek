<?php
namespace App\Http\Controllers\Api;

use App\Models\Test;
use App\Models\Module;
use App\Models\ModuleImage;
use App\Models\Testrecording;
use App\Models\PhoneticAnalysisTranscription;
use App\Models\TestrecordingHasPhonError;
use App\Models\Distortion;
use App\Models\PhonologicalCategory;
use App\Models\Image;
use Illuminate\Support\Facades\Input;
use Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller as Controller;

class TestController extends Controller {

    function getSkippedTestImages(){
        $testId = Input::get('testId');
        $unanswerdImages = Test::find($testId)->unansweredImages();
        return json_encode($unanswerdImages);
    }

    function getTestImages(){

        $testId = Input::get('testId');
        $imageIndex = Input::get('imageIndex');

        $user = Auth::user();
        $test = Test::find($testId);
        $module_id = $test->module_id;

        if($test->patient->user->id != $user->id)
        {
            return Redirect::route('test.index');
        }

        $images = [];

        if ($imageIndex - 1 > 0){
            $images['prev']['moduleImage'] = ModuleImage::where('module_id', '=', $module_id)->where('index', '=', $imageIndex-1)->with('image')->first();
            if ($images['prev']['moduleImage'] != null){
                $images['prev']['testrecording'] = Testrecording::where('test_id', '=', $testId)->where('image_id', '=', $images['prev']['moduleImage']['image']['id'])->first();
            } else {
                $images['prev']['testrecording'] = null;
            }
        } else {
            $images['prev'] = null;
        }

        if ($imageIndex > 0 && $imageIndex <= $test->module->moduleimages->count()){
            $images['curr']['moduleImage'] = ModuleImage::where('module_id', '=', $module_id)->where('index', '=', $imageIndex)->with('image')->first();
            if ($images['curr']['moduleImage'] != null){
                $images['curr']['testrecording'] = Testrecording::where('test_id', '=', $testId)->where('image_id', '=', $images['curr']['moduleImage']['image']['id'])->first();
            } else {
                $images['curr']['testrecording'] = null;
            }
        } else {
            $images['curr'] = null;
        }

        if ($imageIndex + 1 <= $test->module->moduleimages->count()){
            $images['next']['moduleImage'] = ModuleImage::where('module_id', '=', $module_id)->where('index', '=', $imageIndex+1)->with('image')->first();
            if ($images['next']['moduleImage'] != null){
                $images['next']['testrecording'] = Testrecording::where('test_id', '=', $testId)->where('image_id', '=', $images['next']['moduleImage']['image']['id'])->first();
            } else {
                $images['next']['testrecording'] = null;
            }
        } else {
            $images['next'] = null;
        }

        if ($imageIndex + 2 <= $test->module->moduleimages->count()){
            $images['nextnext']['moduleImage'] = ModuleImage::where('module_id', '=', $module_id)->where('index', '=', $imageIndex+2)->with('image')->first();
            if ($images['nextnext']['moduleImage'] != null){
                $images['nextnext']['testrecording'] = Testrecording::where('test_id', '=', $testId)->where('image_id', '=', $images['nextnext']['moduleImage']['image']['id'])->first();
            } else {
                $images['nextnext']['testrecording'] = null;
            }
        } else {
            $images['nextnext'] = null;
        }
        return json_encode($images);
    }

    function postTestData(){

        $testId = Input::get('testId');
        $imageId = Input::get('imageId');

        $test = Test::where('id', '=', $testId)->first();
        $testRecording = TestRecording::where('test_id', '=', $testId)->where('image_id', '=', $imageId)->first();
        $imageIndex = $test->imageIndex($imageId);

        if (!is_null($testRecording)){
            if (Input::get('hasNewRecording') === 'true'){
                $fileName = processRecording('recording');
                $testRecording->filename = $fileName;
            }
            if (Input::get('hasNewErrorData')){
                $testRecording->answered = Input::get('answered') === 'true'? true: false;
                if (Input::get('answered') === 'true'){
                    $testRecording->hasMistake = Input::get('hasMistake') === 'true'? true: false;
                } else {
                    $testRecording->hasMistake = false;
                }
                $testRecording->wordplay = Input::get('wordPlay') === 'true'? true: false;
                $testRecording->sentenceplay = Input::get('sentencePlay') === 'true'? true: false;
            }
        } else {
            $testRecording = new TestRecording();
            $fileName = processRecording('recording');
            $testRecording->filename = $fileName;
            $testRecording->test_id = Input::get('testId');
            $testRecording->image_id = Input::get('imageId');
            $testRecording->answered = Input::get('answered') === 'true'? true: false;
            if (Input::get('answered') === 'true'){
                $testRecording->hasMistake = Input::get('hasMistake') === 'true'? true: false;
            } else {
                $testRecording->hasMistake = false;
            }
            $testRecording->wordplay = Input::get('wordPlay') === 'true'? true: false;
            $testRecording->sentenceplay = Input::get('sentencePlay') === 'true'? true: false;

            $test->indexend = $imageIndex;
            $test->save();
        }

        $testRecording->save();

        $module = Module::where('id', '=', $test->module_id)->first();
        if ($module->imageCount() == $imageIndex){
            $skipped = TestRecording::where('test_id', '=', $testId)->where('answered', '=', false)->count();
            $response = [
                'imageIndex' => $imageIndex,
                'ended' => true,
                'skipped' => $skipped
            ];
            $test->ended = date('Y-m-d H:i:s');
            $test->save();
        } else {
            $response = [
                'ended' => false
            ];
        }

        // Mistakes TODO
        $errorData = json_decode(Input::get('errorData'));
        PhoneticAnalysisTranscription::where('test_id', '=', $testId)
            ->where('image_id', '=', $imageId)
            ->delete();
        TestrecordingHasPhonError::where('testrecording_id', '=', $testRecording->id)
            ->delete()
        ;
        if (Input::get('hasMistake') === 'true'){
            $transcriptions = $errorData->transcriptions;
            for ($i = 0; $i<count($transcriptions); $i++){
                $transcription = $transcriptions[$i];
                $ts = new PhoneticAnalysisTranscription();
                $ts->test_id = $testId;
                $ts->image_id = $imageId;
                $ts->sound_index = $i;
                if ($transcription->sound_id != ""){
                    $ts->sound_id = $transcription->sound_id;
                }
                if (intval($transcription->distortion_id) != 0){
                    $ts->distortion_id = $transcription->distortion_id;
                }
                $ts->save();
                for ($j=0; $j<count($transcription->additions); $j++){
                    $sound_id = $transcription->additions[$j];
                    if ($sound_id != ""){
                        $ts = new PhoneticAnalysisTranscription();
                        $ts->test_id = $testId;
                        $ts->image_id = $imageId;
                        $ts->sound_index = $i;
                        $ts->addition_order = $j;
                        $ts->sound_id = $sound_id;
                        $ts->save();
                    }
                }
            }
            $trailingAdditions = $errorData->trailingAdditions;
            for ($j=0; $j<count($trailingAdditions); $j++){
                $sound_id = $trailingAdditions[$j];
                if ($sound_id != ""){
                    $ts = new PhoneticAnalysisTranscription();
                    $ts->test_id = $testId;
                    $ts->image_id = $imageId;
                    $ts->sound_index = count($transcriptions);
                    $ts->addition_order = $j;
                    $ts->sound_id = $sound_id;
                    $ts->save();
                }
            }
            $phonologicalErrors = $errorData->phonologicalErrors;
            for ($i=0; $i<count($phonologicalErrors); $i++){
                $pe = new TestrecordingHasPhonError();
                $pe->testrecording_id = $testRecording->id;
                $pe->phonological_error_id = $phonologicalErrors[$i];
                $pe->save();
            }
        }

        return json_encode($response);

    }

    function postTestAnalysis(){
        $data = Input::get('data');
        $testId = $data["testId"];
        $imageId = $data["imageId"];
        PhoneticAnalysisTranscription::where('test_id', '=', $testId)
            ->where('image_id', '=', $imageId)
            ->delete();
        $testRecording = Testrecording::where('test_id', '=', $testId)
            ->where('image_id', '=', $imageId)
            ->first()
            ;
        TestrecordingHasPhonError::where('testrecording_id', '=', $testRecording->id)
            ->delete()
            ;
        $transcriptions = $data["transcriptions"];
        for ($i=0; $i<count($transcriptions); $i++){
            $transcription = new PhoneticAnalysisTranscription();
            $transcription->test_id = $testId;
            $transcription->image_id = $imageId;
            $transcription->sound_index = $i;
            if (isset($transcriptions[$i]["soundId"]) && $transcriptions[$i]["soundId"] != null){
                $transcription->sound_id = $transcriptions[$i]["soundId"];
            }
            if (isset($transcriptions[$i]['distortionId']) && $transcriptions[$i]['distortionId'] != null){
                $transcription->distortion_id = $transcriptions[$i]["distortionId"];
            }
            $transcription->save();
            if (isset($transcriptions[$i]["additions"])){
                $additions = $transcriptions[$i]["additions"];
                for ($j=0; $j<count($additions); $j++){
                    $transcription = new PhoneticAnalysisTranscription();
                    $transcription->test_id = $testId;
                    $transcription->image_id = $imageId;
                    $transcription->sound_index = $i;
                    $transcription->sound_id = $additions[$j];
                    $transcription->addition_order = $j;
                    $transcription->save();
                }
            }
        }
        $trailingAdditions = $data["trailingAdditions"];
        for ($i=0; $i<count($trailingAdditions); $i++){
            $addition = $trailingAdditions[$i];
            $transcription = new PhoneticAnalysisTranscription();
            $transcription->test_id = $testId;
            $transcription->image_id = $imageId;
            $transcription->sound_index = count($transcriptions);
            $transcription->sound_id = $addition;
            $transcription->addition_order = $i;
            $transcription->save();
        }
        if (isset($data['phonologicalErrors']) && count($data['phonologicalErrors']) > 0){
            $phonErrors = $data['phonologicalErrors'];
            for ($i=0; $i<count($phonErrors); $i++){
                $hasPhonError = new TestrecordingHasPhonError();
                $hasPhonError->testrecording_id = $data['testRecordingId'];
                $hasPhonError->phonological_error_id = intval($phonErrors[$i]);
                $hasPhonError->save();
            }
        }
    }

    function postInTestAnalysis(){

    }

    function getImageAnalysisData(){
        $testId = Input::get('testId');
        $imageId = Input::get('imageId');

        $test = Test::find($testId);
        $testRecording = Testrecording::where('test_id', '=', $testId)
            ->where('image_id', '=', $imageId)
            ->first();

        $image = Image::where('id', '=', $imageId)->first();
        $sounds = $image->phonetics;
        if (!is_null($testRecording)){

            $transcriptions = PhoneticAnalysisTranscription::where('test_id', '=', $test->id)
                ->where('image_id', '=', $imageId)
                ->orderBy('sound_index', 'asc')
                ->orderBy('addition_order', 'asc')
                ->get()
            ;
            if (count($transcriptions) == 0){
                $i=0;
                foreach ($sounds as $sound){
                    $transcription = new PhoneticAnalysisTranscription();
                    $transcription->sound_id = $sound->id;
                    $transcription->sound_index = $i;
                    $transcription->test_id = $testId;
                    $transcription->image_id = $imageId;
                    $transcription->save();

                    $i++;
                }
            }
            $transcriptions = PhoneticAnalysisTranscription::where('test_id', '=', $test->id)
                ->where('image_id', '=', $imageId)
                ->orderBy('sound_index', 'asc')
                ->orderBy('addition_order', 'asc')
                ->get()
            ;
            $i=0;
            $sounds = [];
            $additions = [];
            $prevTranscription = null;
            foreach ($transcriptions as $transcription){
                if (is_null($transcription->addition_order)){
                    $distortion = Distortion::where('id', '=', $transcription->distortion_id)->first();
                    if (!is_null($distortion)){
                        $transcription->distortion = $distortion->id;
                    } else {
                        $transcription->distortion = null;
                    }
                    if (!is_null($prevTranscription)){
                        $prevTranscription->additions = $additions;
                    }
                    array_push($sounds, $transcription);
                    $i++;
                    $additions = [];
                    $prevTranscription = $transcription;
                } else {
                    $additions[count($additions)] = $transcription->sound_id;
                }
            }
            $trailingAdditions = $additions;
            $phonologicalErrors = $testRecording->phonologicalErrors;

            $data = [
                'testId' => $testId,
                'imageId' => $imageId,
                //'word' => $image->word,
                'transcriptions' => $sounds,
                'phonologicalErrors' => $phonologicalErrors,
                'trailingAdditions' => $trailingAdditions
            ];

        } else {
            $transcriptions = [];
            $i=0;
            foreach ($sounds as $sound){
                $transcription = [];
                $transcription['sound_id'] = $sound->id;
                $transcription['sound_index'] = $i;
                $transcription['additions'] = [];
                $transcription['distortion_id'] = null;
                $transcription['distortion'] = null;
                $transcription['addition_order'] = null;
                array_push($transcriptions, $transcription);
                $i++;
            }
            $data = [
                'testId' => $testId,
                'imageId' => $imageId,
                //'word' => $image->word,
                'transcriptions' => $transcriptions,
                'phonologicalErrors' => [],
                'trailingAdditions' => []
            ];
        }

        return json_encode($data);
    }

    function getImageData(){
        $imageId = Input::get('imageId');

        $image = Image::where('id', '=', $imageId)
            ->with(array('phonetics' => function($query){
                    $query->with(array('distortions' => function($query){

                        }));
                }))
            ->first();
        return json_encode($image);
    }

    function getPhonologicalErrorData(){
        $phonErrors = PhonologicalCategory::with(array('errors' => function($query){

            }))
            ->get();
        echo json_encode($phonErrors);
    }

    function postInTestAnalysisData(){
        $testId = Input::get('testId');
        $imageId = Input::get('imageId');
        PhoneticAnalysisTranscription::where('test_id', '=', $testId)
            ->where('image_id', '=', $imageId)
            ->delete();
        $testRecording = Testrecording::where('test_id', '=', $testId)
            ->where('image_id', '=', $imageId)
            ->first()
        ;
        TestrecordingHasPhonError::where('testrecording_id', '=', $testRecording->id)
            ->delete()
        ;
        $transcriptions = Input::get('transcriptions');
        for ($i = 0; $i<count($transcriptions); $i++){
            $transcription = $transcriptions[$i];
            $ts = new PhoneticAnalysisTranscription();
            $ts->test_id = $testId;
            $ts->image_id = $imageId;
            $ts->sound_index = $i;
            if ($transcription->sound_id != ""){
                $ts->sound_id = $transcription->sound_id;
            }
            if ($transcription->distortion_id != 0){
                $ts->distortion_id = $transcription->distortion_id;
            }
            $ts->save();
            for ($j=0; $j<count($transcription['additions']); $j++){
                $sound_id = $transcription['additions'][$j];
                if ($sound_id != ""){
                    $ts = new PhoneticAnalysisTranscription();
                    $ts->test_id = $testId;
                    $ts->image_id = $imageId;
                    $ts->sound_index = $i;
                    $ts->addition_order = $j;
                    $ts->sound_id = $sound_id;
                    $ts->save();
                }
            }
        }
        $phonologicalErrors = Input::get('phonologicalErrors');
        for ($i=0; $i<count($phonologicalErrors); $i++){
            $pe = new TestrecordingHasPhonError();
            $pe->testrecording_id = $testRecording->id;
            $pe->phonological_error_id = $phonologicalErrors[$i];
            $pe->save();
        }
    }

    function getUnanswered(){
        $id = Input::get('testId');
        $test = Test::find($id);
        $module = Module::find($test->module_id);
        $testRecordings = Testrecording::where('test_id', '=', $id)
            ->where('answered', '=', false)
            ->orderBy('id', 'asc')->get();
        $images = [];
        foreach ($testRecordings as $tr){
            $moduleImage = ModuleImage::where('module_id', '=', $module->id)
                ->where('image_id', '=', $tr->image_id)
                ->with('image')
                ->first();
            array_push($images, $moduleImage);
        }
        echo json_encode($images);
    }

}

function processRecording($fieldname){
    $fileContent = file_get_contents($_FILES[$fieldname]['tmp_name']);
    $directory = "uploads/recordings/";

    // Determine filename by microseconds since Unix Epoch
    $split = explode(' ', microtime());
    $fileName = intval($split[1])*1000 . floatval($split[0])*1000000;

    // Check if file already exists. If so, pick another filename.
    while (file_exists(public_path() . $directory . $fileName . '.wav')){
        $split = explode(' ', microtime());
        $fileName = intval($split[1])*1000 . floatval($split[0])*1000000;
    }
    // Move file to permanent location
    file_put_contents(public_path() . $directory . $fileName . '.wav', $fileContent);
    return $fileName . '.wav';
}