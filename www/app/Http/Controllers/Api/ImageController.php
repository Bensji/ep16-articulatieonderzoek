<?php
/**
 * Created by PhpStorm.
 * User: BenjaminLierman
 * Date: 8/03/16
 * Time: 20:58
 */

namespace App\Http\Controllers\Api;

use App\Models\Image;

use App\Http\Controllers\Controller as Controller;

class ImageController extends Controller
{
    function getImages(){
        return json_encode(Image::all());
    }
}