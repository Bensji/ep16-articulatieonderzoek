<?php
/**
 * Created by PhpStorm.
 * User: BenjaminLierman
 * Date: 8/03/16
 * Time: 20:52
 */

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\User;
use Auth;
use Redirect;
use Carbon\Carbon;

use App\Http\Requests\EditUserRequest;
use App\Http\Requests\StoreUserRequest;

use App\Http\Controllers\Controller as Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $user = Auth::User();
        $users = User::with('role')->get();
        return response()->json(['users' => $users]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getRoles()
    {
        $roles = Role::all();
        return response()->json(['roles' => $roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $user = Auth::user();
        $roles = Role::All()->lists('name',  'id');
        return View::make('admin.user.create')->with('user', $user)->with('roles', $roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUserRequest $request
     * @return Response
     */
    public function store(StoreUserRequest $request)
    {
        if ( $user = new User( $request->all() )) {

            $user->save();
            return response()->json(['succes' => 'User aangemaakt.']);
        }
        else
        {
            return response()
                ->json($request->getMessageBag()->toArray());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $showUser = User::with('role')->find($id);
        $roles = Role::all();
        return response()->json(['user' => $showUser, 'role' => $roles]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $editUser = User::withTrashed()->find($id);
        $roles = Role::All()->lists('name',  'id');

        return View::make('admin.user.edit')->with('user', $user)->with('u', $editUser)->with('roles', $roles);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  EditUserRequest $request int  $id
     * @return Response
     */
    public function update(EditUserRequest $request, $id)
    {

        if ( $dataUserUpdate = $request->all() )
        {
            $editUser = User::withTrashed()->findOrFail($id);
            $editUser->update($request->all());

            return response()->json(['succes' => 'User aangepast.']);
        }else{
            return response()
                ->json($request->getMessageBag()->toArray());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if($id == 1){
            return response()->json(['error' => 'Deze user kan niet verwijderd worden.']);
        }
        $user = User::find($id);
        $user->delete();

        return response()->json(['succes' => 'User verwijderd.']);
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function restore($id)
    {
        $user = User::withTrashed()->find($id);
        $user->deleted_at = null;
        $user->update();

        return response()->json(['succes' => 'User restored.']);
    }

    /**
     * Lock the user
     *
     * @param  int $id
     * @return Response
     */
    public function lock($id)
    {
        if($id == 1){
            return response()->json(['error' => 'Deze user kan niet gelocked worden.']);
        }
        $user = User::withTrashed()->find($id);
        $user->locked = Carbon::now();
        $user->update();

        return response()->json(['succes' => 'User Locked.']);
    }

    /**
     * Lock the user
     *
     * @param  int $id
     * @return Response
     */
    public function unlock($id)
    {
        $user = User::withTrashed()->find($id);
        $user->locked = null;
        $user->update();

        return response()->json(['succes' => 'User Unlocked.']);
    }
}