<?php
/**
 * Created by PhpStorm.
 * User: BenjaminLierman
 * Date: 8/03/16
 * Time: 18:24
 */

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreImageRequest;
use App\Models\ImageHasSound;
use App\Models\Image;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;


use App\Http\Controllers\Controller as Controller;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $search = Input::get('search');

        if($search == null){
            $images = Image::withTrashed()->paginate(5);
        }else{
            $images = Image::withTrashed()->where('word', 'LIKE', '%'.$search.'%')->paginate(5);
        }

        $user = Auth::user();

        $all = Image::withTrashed()->lists('word');

        return View::make('admin.image.index')
            ->with('user', $user)
            ->with('images', $images)
            ->with('all', $all)
            ->with('search', $search);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $user = Auth::user();
        return View::make('admin.image.create')
            ->with('user', $user)
            ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validator = new StoreImageRequest(Input::all());

        if ( $validator->passes() ) {
            $destinationPathImages = 'uploads/images/';
            $destinationPathWord = 'uploads/words/';
            $destinationPathImagesSentences = 'uploads/sentences/';

            $fileUrl = Input::file('url');
            $fileWordUrl = Input::file('wordurl');
            $fileSentenceUrl = Input::file('sentenceurl');

            $extensionUrl = $fileUrl->getClientOriginalExtension();
            $extensionWordUrl = $fileWordUrl->getClientOriginalExtension();
            $extensionSentenceUrl = $fileSentenceUrl->getClientOriginalExtension();

            $filenameUrl = Input::get('word').'.'.$extensionUrl;
            $filenameWordUrl = Input::get('word').'.'.$extensionWordUrl;
            $filenameSentenceUrl = Input::get('word').'.'.$extensionSentenceUrl;

            $uploadSuccessUrl = $fileUrl->move($destinationPathImages, $filenameUrl);
            $uploadSuccessWordUrl = $fileWordUrl->move($destinationPathWord, $filenameWordUrl);
            $uploadSuccessSentencesUrl = $fileSentenceUrl->move($destinationPathImagesSentences, $filenameSentenceUrl);

            if( $uploadSuccessUrl && $uploadSuccessWordUrl  && $uploadSuccessSentencesUrl){

                $image = new Image();

                $image->word = Input::get('word');
                //$image->word = Input::get('phonetic-value');
                //$image->phonetic = Input::get('phonetic');
                $image->url = $filenameUrl;
                $image->wordurl = $filenameWordUrl;
                $image->sentenceurl = $filenameSentenceUrl;

                $image->save();

                $phoneticString = Input::get('phonetic-value');
                $split = explode('|', $phoneticString);

                for ($i=0; $i<count($split); $i++){
                    $hasSound = new ImageHasSound();
                    $hasSound->image_id = $image->id;
                    $hasSound->index = $i;
                    $hasSound->sound_id = intval($split[$i]);
                    $hasSound->save();
                }

                return Redirect::route('admin.image.index');
            }else{
                return Redirect::route('admin.image.create')
                    ->withInput();
            }
        }else{
            $validatorErrors = $validator->errors();
            return Redirect::route('admin.image.create')
                ->withInput()
                ->withErrors($validatorErrors);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $image = Image::withTrashed()
            ->where('id', '=', $id)
            ->first()
        ;

        return View::make('admin.image.show')
            ->with('user', $user)
            ->with('image', $image)
            ->with('phoneticString', $image->phoneticString())
            ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $image = Image::withTrashed()
            ->where('id', '=', $id)
            ->with(array('phonetics' => function($query){

            }))
            ->first()
        ;
        $sounds = $image->phonetics->toArray();
        $phoneticString = '';
        $phoneticIdString = '';
        if (count($sounds) > 0){
            $phoneticString .= $sounds[0]['phonetic'];
            $phoneticIdString = $sounds[0]['id'];
        }
        for ($i=1; $i<count($sounds); $i++){
            $phoneticString .= $sounds[$i]['phonetic'];
            $phoneticIdString .= '|' . $sounds[$i]['id'];
        }
        return View::make('admin.image.edit')
            ->with('user', $user)
            ->with('image', $image)
            ->with('phoneticIdString', $phoneticIdString)
            ->with('phoneticString', $phoneticString)
            ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $validator = StoreImageRequest::make(Input::all())
            ->addContext('edit')
            ->bindReplacement('word', ['id' => $id]);

        if ( $validator->passes() ) {
            $destinationPathImages = 'uploads/images/';
            $destinationPathWord = 'uploads/words/';
            $destinationPathImagesSentences = 'uploads/sentences/';

            $image = Image::withTrashed()->find($id);
            if( $image->word != Input::get('word') )
            {
                $ext = pathinfo('uploads/images/'.$image->url, PATHINFO_EXTENSION);
                rename( 'uploads/images/'.$image->url, 'uploads/images/'.Input::get('word').'.'.$ext );
                $image->url = Input::get('word').'.'.$ext;

                $ext = pathinfo('uploads/images/'.$image->wordurl, PATHINFO_EXTENSION);
                rename( 'uploads/words/'.$image->wordurl, 'uploads/words/'.Input::get('word').'.'.$ext );
                $image->wordurl = Input::get('word').'.'.$ext;

                $ext = pathinfo('uploads/images/'.$image->sentenceurl, PATHINFO_EXTENSION);
                rename( 'uploads/sentences/'.$image->sentenceurl, 'uploads/sentences/'.Input::get('word').'.'.$ext );
                $image->sentenceurl = Input::get('word').'.'.$ext;
            }
            $image->word = Input::get('word');
            //$image->phonetic = Input::get('phonetic');
            Image::find($id)->imageHasSounds()->delete();
            $phoneticString = Input::get('phonetic-value');
            $split = explode('|', $phoneticString);

            for ($i=0; $i<count($split); $i++){
                $hasSound = new ImageHasSound();
                $hasSound->image_id = $image->id;
                $hasSound->index = $i;
                $hasSound->sound_id = intval($split[$i]);
                $hasSound->save();
            }


            if (Input::hasFile('url'))
            {
                $fileUrl = Input::file('url');
                $extensionUrl = $fileUrl->getClientOriginalExtension();
                $filenameUrl = Input::get('word').'.'.$extensionUrl;
                $uploadSuccessUrl = $fileUrl->move($destinationPathImages, $filenameUrl);
                if($uploadSuccessUrl)
                {
                    $image->url = $filenameUrl;
                }else{
                    return Redirect::route('admin.image.edit', $id)
                        ->withInput();
                }
            }

            if (Input::hasFile('wordurl'))
            {
                $fileWordUrl = Input::file('wordurl');
                $extensionWordUrl = $fileWordUrl->getClientOriginalExtension();
                $filenameWordUrl = Input::get('word').'.'.$extensionWordUrl;
                $uploadSuccessWordUrl = $fileWordUrl->move($destinationPathWord, $filenameWordUrl);
                if($uploadSuccessWordUrl)
                {
                    $image->wordurl = $filenameWordUrl;
                }else{
                    return Redirect::route('admin.image.edit', $id)
                        ->withInput();
                }
            }

            if (Input::hasFile('sentenceurl'))
            {
                $fileSentenceUrl = Input::file('sentenceurl');
                $extensionSentenceUrl = $fileSentenceUrl->getClientOriginalExtension();
                $filenameSentenceUrl = Input::get('word').'.'.$extensionSentenceUrl;
                $uploadSuccessSentencesUrl = $fileSentenceUrl->move($destinationPathImagesSentences, $filenameSentenceUrl);
                if($uploadSuccessSentencesUrl)
                {
                    $image->sentenceurl = $filenameSentenceUrl;
                }else{
                    return Redirect::route('admin.image.edit', $id)
                        ->withInput();
                }
            }

            $image->update();

            return Redirect::route('admin.image.index');
        }else{
            $validatorErrors = $validator->errors();
            return Redirect::route('admin.image.edit', $id)
                ->withInput()
                ->withErrors($validatorErrors);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $image = Image::find($id);
        $image->delete();

        return Redirect::route('admin.image.index');
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function restore($id)
    {
        $image = Image::withTrashed()->find($id);
        $image->deleted_at = null;
        $image->update();

        return Redirect::route('admin.image.index');
    }
}