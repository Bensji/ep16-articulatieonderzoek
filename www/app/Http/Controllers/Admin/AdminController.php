<?php
/**
 * Created by PhpStorm.
 * User: BenjaminLierman
 * Date: 8/03/16
 * Time: 20:56
 */

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Auth;
use App\Models\Role;

use App\Http\Controllers\Controller as Controller;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $user = Auth::User();
        $users = User::with('role')->where('role_id', '=', '1')->get();
        return response()->json(['users' => $users]);
    }

}