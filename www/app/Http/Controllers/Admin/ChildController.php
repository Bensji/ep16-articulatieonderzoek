<?php
/**
 * Created by PhpStorm.
 * User: BenjaminLierman
 * Date: 8/03/16
 * Time: 18:18
 */

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreChildRequest;
use App\Http\Requests\StoreUserRequest;
use App\Models\Patient;
use Auth;
use App\Models\User;


use App\Http\Controllers\Controller as Controller;

class ChildController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $user = Auth::user();
        $children = Patient::all();
        return response()->json(['user' => $user, 'children' => $children]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param StoreChildRequest $request
     * @return Response
     */
    public function store(StoreChildRequest $request)
    {
        if($child = new Patient($request->all()))
        {
            $child->save();
            return response()->json(['succes' => 'Kind aangemaakt.']);
        }
        else
        {
            return response()
                ->json($request->getMessageBag()->toArray());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $child = Patient::with('user')->find($id);
        $users = User::all();
        return response()->json(['child'=> $child, 'users' => $users ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  StoreChildRequest  $request int  $id
     * @return Response
     */
    public function update(StoreChildRequest $request, $id)
    {
        $user = Auth::user();
        $editChild = Patient::findOrFail($id);
        if($dataUpdate = $request->all())
        {
            $editChild->update($dataUpdate);
            return response()->json(['succes' => 'Kind aangepast.']);
        }
        else
        {
            return response()
                ->json($request->getMessageBag()->toArray());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $child = Patient::find($id);

        if($child->delete())
        {
            return response()->json(['succes' => 'Kind verwijderd.']);
        }else
        {
            return response()->json(['error' => 'Kind niet verwijderd.']);
        }
    }

    public function restore($id)
    {
        $child = Patient::withTrashed()->find($id);
        $child->restore();

        return Redirect::route('admin.child.index');
    }
}