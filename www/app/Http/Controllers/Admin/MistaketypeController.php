<?php
/**
 * Created by PhpStorm.
 * User: BenjaminLierman
 * Date: 8/03/16
 * Time: 18:41
 */

namespace App\Http\Controllers\Admin;

use App\Models\Mistaketype;

use App\Http\Controllers\Controller as Controller;

class MistaketypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $search = Input::get('search');

        if($search == null){
            $mistaketypes = Mistaketype::withTrashed()->paginate(5);
        }else{
            $mistaketypes = Mistaketype::withTrashed()->where('name', 'LIKE', '%'.$search.'%')->paginate(5);
        }

        $user = Auth::user();

        $all = Mistaketype::withTrashed()->lists('name');

        return View::make('admin.mistaketype.index')
            ->with('user', $user)
            ->with('mistaketypes', $mistaketypes)
            ->with('all', $all)
            ->with('search', $search);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}