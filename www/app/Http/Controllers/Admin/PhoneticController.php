<?php
/**
 * Created by PhpStorm.
 * User: BenjaminLierman
 * Date: 8/03/16
 * Time: 20:45
 */

namespace App\Http\Controllers\Admin;

use App\Models\Module;
use App\Models\ModuleImage;
use App\Models\Mistake;
use App\Models\Phonetic;

use App\Http\Requests\StorePhoneticRequest;

use App\Http\Controllers\Controller as Controller;

class PhoneticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $search = Input::get('search');


        if($search == null){
            $phonetics = Phonetic::withTrashed()->paginate(5);
        }else{
            $phonetics = Phonetic::withTrashed()->where('sign', 'LIKE', '%'.$search.'%')->paginate(5);
        }

        $user = Auth::user();

        $all = Phonetic::withTrashed()->lists('sign');

        return View::make('admin.phonetic.index')
            ->with('user', $user)
            ->with('phonetics', $phonetics)
            ->with('all', $all)
            ->with('search', $search);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $editPhonetic = Phonetic::withTrashed()->find($id);
        $mistakes = Mistake::All()->lists('name',  'id');

        return View::make('admin.phonetic.edit')->with('user', $user)->with('phonetic', $editPhonetic)->with('mistakes', $mistakes);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $validator = StorePhoneticRequest::make(Input::all());

        if ( $validator->passes() )
        {
            $editPhonetic = Phonetic::withTrashed()->find($id);
            $i = 1;
            foreach ($editPhonetic->phoneticmistakes as $mistake) {
                $mistake->mistake_id = Input::get('mistake_id_'.$i);
                $mistake->update();
                $i++;
            }

            return Redirect::route('admin.phonetic.index');
        }else{
            $validatorErrors = $validator->errors();
            return Redirect::route('admin.phonetic.edit', $id)
                ->withInput()
                ->withErrors($validatorErrors);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
