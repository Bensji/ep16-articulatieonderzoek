<?php
/**
 * Created by PhpStorm.
 * User: BenjaminLierman
 * Date: 8/03/16
 * Time: 20:40
 */

namespace App\Http\Controllers\Admin;

use App\Models\Module;
use App\Models\ModuleImage;
use App\Models\Moduletype;
use App\Models\Image;

use App\Http\Requests\StoreModuleRequest;

use App\Http\Controllers\Controller as Controller;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $search = Input::get('search');

        if($search == null){
            $modules = Module::withTrashed()->paginate(5);
        }else{
            $modules = Module::withTrashed()->where('name', 'LIKE', '%'.$search.'%')->paginate(5);
        }

        $user = Auth::user();

        $all = Module::withTrashed()->lists('name');

        return View::make('admin.module.index')
            ->with('user', $user)
            ->with('modules', $modules)
            ->with('all', $all)
            ->with('search', $search);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $user = Auth::user();
        $moduletypes = ["" => ""] + Moduletype::All()->lists('name', 'id');
        $images = Image::all();

        $selectedimages = [];

        return View::make('admin.module.create')
            ->with('user', $user)
            ->with('moduletypes', $moduletypes)
            ->with('images', $images)
            ->with('selectedimages', $selectedimages)
            ->with('baseUrl', '../../')
            ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validator = new StoreModuleRequest(Input::all());

        if ( $validator->passes() ) {
            $module = new Module();
            $module->name = Input::get('name');
            $module->description = Input::get('description');
            $module->moduletype_id = Input::get('moduletype_id');
            $module->save();

            $idString = Input::get('image-ids');
            $ids = explode('|', $idString);

            $i=1;
            for ($i = 0; $i<count($ids); $i++){
                $mi = new ModuleImage();
                $mi->module_id = $module->id;
                $mi->image_id = $ids[$i];
                $mi->index = $i+1;
                $mi->save();
            }

            return Redirect::route('admin.module.index');
        }else{

            if( Input::get('images') )
            {
                foreach (Input::get('images') as $image)
                {
                    $selectedimages[] = $image;
                }
            }
            else
            {
                $selectedimages = [];
            }

            $validatorErrors = $validator->errors();
            return Redirect::route('admin.module.create')
                ->withInput()
                ->with('selectedimages', $selectedimages )
                ->withErrors($validatorErrors);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $module = Module::find($id);

        return View::make('admin.module.show')
            ->with('user', $user)
            ->with('module', $module)
            ->with('baseUrl', '../../')
            ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $module = Module::find($id);
        $moduletypes = ["" => ""] + Moduletype::All()->lists('name', 'id');
        $images = Image::all();
        $moduleimages = (array) $module->moduleimages;

        usort($moduleimages, "cmp");
        $moduleimages = $moduleimages[0];

        $selectedimages = [];
        for ($i=0; $i<count($moduleimages); $i++){
            $image = Image::where('id', '=', $moduleimages[$i]["image_id"])->first();
            array_push($selectedimages, $image);
        }

        return View::make('admin.module.edit')
            ->with('user', $user)
            ->with('moduletypes', $moduletypes)
            ->with('images', $images)
            ->with('selectedimages', $selectedimages)
            ->with('module', $module)
            ->with('baseUrl', '../../../')
            ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $validator = StoreModuleRequest::make(Input::all())
            ->addContext('edit')
            ->bindReplacement('name', ['id' => $id])
        ;

        if ( $validator->passes() ) {
            $module = Module::find($id);
            $module->name = Input::get('name');
            $module->description = Input::get('description');
            $module->moduletype_id = Input::get('moduletype_id');
            $module->save();

            $idString = Input::get('image-ids');
            $ids = explode('|', $idString);

            $i=1;
            $moduleimages = ModuleImage::where('module_id','=',$module->id)->get();
            $stillPresent = [];
            for ($i = 0; $i<count($ids); $i++){
                $mi = ModuleImage::where('module_id', '=', $module->id)
                    ->where('image_id', '=', $ids[$i])
                    ->first();
                if (!is_null($mi)){
                    array_push($stillPresent, $mi->id);
                    $mi->index = $i+1;
                    $mi->save();
                } else {
                    $mi = new ModuleImage();
                    $mi->module_id = $module->id;
                    $mi->image_id = $ids[$i];
                    $mi->index = $i+1;
                    $mi->save();
                }
            }
            foreach ($moduleimages as $mi){
                $match = false;
                $i=0;
                while (!$match && $i<count($stillPresent)){
                    if ($mi->id == $stillPresent[$i]){
                        $match = true;
                    }
                    $i++;
                }
                if (!$match){
                    $mi->delete();
                }
            }
            /*foreach (Input::get('images') as $image) {
                $mhi = new ModuleImage();
                $mhi->module_id = $module->id;
                $mhi->image_id = $image;
                $mhi->index = $i;
                $mhi->save();
                $i++;
            }*/

            return Redirect::route('admin.module.index');
        }else{
            if( Input::get('images') )
            {
                foreach (Input::get('images') as $image)
                {
                    $selectedimages[] = $image;
                }
            }
            else
            {
                $selectedimages = [];
            }

            $validatorErrors = $validator->errors();
            return Redirect::route('admin.module.edit', ['id' => $id])
                ->withInput()
                ->with('selectedimages', $selectedimages )
                ->withErrors($validatorErrors);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $module = Module::find($id);
        $module->delete();

        return Redirect::route('admin.module.index');
    }

    public function restore($id)
    {
        $module = Module::withTrashed()->find($id);
        $module->restore();

        return Redirect::route('admin.module.index');
    }
}

//TODO Wat doet dit?
function cmp($a, $b)
{
    return strcmp($a->index, $b->index);
}