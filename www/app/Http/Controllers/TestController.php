<?php

namespace App\Http\Controllers;

use App\Models\Test;
use App\Models\Testrecording;
use App\Models\Patient;
use App\Models\Module;
use App\Models\ModuleImage;
use App\Models\PhoneticAnalysisTranscription;
use App\Models\PhonologicalCategory;
use App\Models\Sound;
use App\Models\Distortion;
use App\Models\Testmistake;
use App\Models\Agegroup;
use Auth;
use View;
use Log;
use Redirect;
use Illuminate\Support\Facades\Input;

use App\Http\Requests\StoreTestRequest;

class TestController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = Auth::user();
		$alltests = Test::with('module')->get();
        foreach ($alltests as $test){
            $testrecordings = Testrecording::where('test_id', '=', $test->id)->get();
            $hasError = false;
            $i=0;
            while (!$hasError && $i<count($testrecordings)){
                if ($testrecordings[$i]->hasMistake == true){
                    $hasError = true;
                }
                $i++;
            }
            if (!$hasError && !is_null($test->ended)){
                $test->analyzed = date('Y-m-d H:i:s');
                $test->save();
            }
        }
        //$alltests = Test::whereNull('deleted_at');

		$tests = [];
		$children = [];
		foreach ($alltests as $test)
		{
            if( $test->patient->user->id == $user->id )
            {
                $tests[] = $test;
                $children[] = $test->patient->name;
            }
		}

        return response()
            ->json(['user' => $user, 'tests' => $tests, 'children' => $children]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$user = Auth::user();
        $userId = Auth::id();
        $patient = Patient::where('user_id', '=', $userId);
        $module = Module::all();
		$children = $patient->lists('name', 'id');
		$modules = $module->lists('name', 'id');
        Log::info($children);
		return View::make('test.create')
			->with('user', $user)
			->with('children', $children)
			->with('modules', $modules);
	}

	/**
	 * Store a newly created resource in storage.
	 *
     * @param  StoreTestRequest  $request
	 * @return Response
	 */
	public function store(StoreTestRequest $request)
	{
		if ($request->all()) {
			$test = new Test( $request->all() );
            $test->indexend = 0;
			$test->save();

			return Redirect::route('test.test', [$test->id]);
		}else{
				return Redirect::route('test.create')
            		->withInput()
            		->withErrors($request->getMessageBag()->toArray());
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function test($id)
	{
		$user = Auth::user();
		$test = Test::find($id);
		if($test->patient->user->id != $user->id)
		{
            return redirect('/#/test');
		}
        $moduleImagesCount = $test->module->moduleimages->count();
		if( $test->indexend < $moduleImagesCount )
		{
			$index = $test->indexend + 1;
			$module_id = $test->module->id;
			$moduleImage = ModuleImage::where('module_id', '=', $module_id)->where('index', '=', $index)->first();
			$image = $moduleImage->image;

			//$phonetics = Phonetic::all();
			//$mistakes = Mistake::where('parent_id', 3)->lists('name', 'id');
			return View::make('test/test')->with('test',$test)
                ->with('moduleImage', $moduleImage)
				->with('image', $image)
				//->with('phonetics',$phonetics)
				//->with('mistakes', $mistakes)
                ->with('totalImages', $moduleImagesCount);

		}
		elseif ( !$test->analyzed )
		{
			$user = Auth::user();
			$testRecordings = TestRecording::where('test_id','=',$id)->where('hasMistake', '=', true)->get();
            foreach ($testRecordings as $recording){
                $image = $recording->image;
                $phonetics = $image->phonetics;
                if (count($recording->transcriptions()) == 0){
                    $i=0;
                    foreach ($phonetics as $phonetic){
                        $transcription = new PhoneticAnalysisTranscription();
                        $transcription->test_id = $recording->test_id;
                        $transcription->image_id = $recording->image_id;
                        $transcription->sound_id = $phonetic->id;
                        $transcription->sound_index = $i;
                        $transcription->save();
                        $i++;
                    }
                }
            }
            $testRecordings = TestRecording::where('test_id','=',$id)
                ->where('hasMistake', '=', true)
                ->with(array('phonologicalErrors' => function($query){

                    }))
                ->get()
                //->paginate(5)
                ;
            $phonologicalCategories = PhonologicalCategory::all()->lists('name', 'id');
            foreach ($testRecordings as $recording){
                $transcriptions = $recording->transcriptions();
                foreach ($transcriptions as $transcription){
                    $transcription->sound = Sound::where('id', '=', $transcription->sound_id)
                        ->first();
                    $transcription->distortion = Distortion::where('id', '=', $transcription->distortion_id)
                        ->first();
                }
                $recording->transcription = $transcriptions;
            }
			return View::make('test/analyze')
				->with('user', $user)
				->with('testRecordings', $testRecordings)
                ->with('phonCats', $phonologicalCategories)
				//->with('phonetics',$phonetics)
				//->with('mistakes', $mistakes)
            ;
		}
		elseif ( $test->analyzed)
		{
			$user = Auth::user();
            $faultyRecordings = Testrecording::where('test_id', '=', $test->id)
                ->where('hasMistake', '=', true)
                ->with('image')
                ->get()
            ;
            //$first = null;
            $omissions = [];
            $omissions["sound_class"] = [];
            $omissions["articulation_place"] = [];
            $omissions["voiced_voiceless"] = [];
            $substitutions = [];
            $substitutions["sound_class"] = [];
            $substitutions["articulation_place"] = [];
            $substitutions["voiced_voiceless"] = [];
            $distortions = [];
            $additions = 0;
            $phonologicalErrors = [];
            foreach ($faultyRecordings as $fr){
                $pats = PhoneticAnalysisTranscription::where('test_id', '=', $test->id)
                    ->where('image_id', '=', $fr->image->id)
                    ->get();
                $imageSounds = $fr->image->phonetics;
                $count = 0;
                foreach ($imageSounds as $is){
                    $count++;
                }
                //$first = $imageSounds->get(0);
                foreach ($pats as $pat){
                    if (is_null($pat->addition_order)){
                        $sound = $imageSounds->get($pat->sound_index);
                        if (!$sound->vowel){
                            $soundClass = $sound->soundClass->name;
                            if ($pat->sound_id == null){
                                if (array_key_exists($soundClass, $omissions["sound_class"])){
                                    $omissions["sound_class"][$soundClass]++;
                                } else {
                                    $omissions["sound_class"][$soundClass] = 1;
                                }
                            } else if ($pat->sound_id != $sound->id){
                                $faultysc = $pat->sound->soundClass->name;
                                $key = $soundClass . '_' . $faultysc;
                                if (array_key_exists($key, $substitutions["sound_class"])){
                                    $substitutions["sound_class"][$key]++;
                                } else {
                                    $substitutions["sound_class"][$key] = 1;
                                }
                            }
                            $articulationPlace = $sound->articulationPlace->name;
                            if ($pat->sound_id == null){
                                if (array_key_exists($articulationPlace, $omissions["articulation_place"])){
                                    $omissions["articulation_place"][$articulationPlace]++;
                                } else {
                                    $omissions["articulation_place"][$articulationPlace] = 1;
                                }
                            } else if ($pat->sound_id != $sound->id){
                                $faultyap = $pat->sound->articulationPlace->name;
                                $key = $articulationPlace . '_' . $faultyap;
                                if (array_key_exists($key, $substitutions["articulation_place"])){
                                    $substitutions["articulation_place"][$key]++;
                                } else {
                                    $substitutions["articulation_place"][$key] = 1;
                                }
                            }
                            $voiced = $sound->voiced;
                            if ($pat->sound_id == null){
                                if ($voiced){
                                    if (array_key_exists("stemhebbend", $omissions["voiced_voiceless"])){
                                        $omissions["voiced_voiceless"]["stemhebbend"]++;
                                    } else {
                                        $omissions["voiced_voiceless"]["stemhebbend"] = 1;
                                    }
                                } else {
                                    if (array_key_exists("stemloos", $omissions["voiced_voiceless"])){
                                        $omissions["voiced_voiceless"]["stemloos"]++;
                                    } else {
                                        $omissions["voiced_voiceless"]["stemloos"] = 1;
                                    }
                                }
                            } else if ($pat->sound_id != $sound->id){
                                $faultyVoiced = $pat->sound->voiced;
                                $key = null;
                                if ($voiced && $faultyVoiced){
                                    $key = 'stemhebbend_stemhebbend';
                                } else if (!$voiced && $faultyVoiced){
                                    $key = 'stemloos_stemhebbend';
                                } else if ($voiced && !$faultyVoiced){
                                    $key = 'stemhebbend_stemloos';
                                } else if (!$voiced && !$faultyVoiced){
                                    $key = 'stemloos_stemloos';
                                }
                                if (array_key_exists($key, $substitutions["voiced_voiceless"])){
                                    $substitutions["voiced_voiceless"][$key]++;
                                } else {
                                    $substitutions["voiced_voiceless"][$key] = 1;
                                }
                            }
                        }
                        if ($pat->distortion_id != null){
                            if (array_key_exists($pat->distortion->name, $distortions)){
                                $distortions[$pat->distortion->name]++;
                            } else {
                                $distortions[$pat->distortion->name] = 1;
                            }
                        }
                    } else {
                        $additions++;
                    }
                }
                //$categories = PhonologicalCategory::all()->orderBy('id');

                $errors = $fr->phonologicalErrors;
                $counter = 0;
                foreach ($errors as $err){
                    $counter++;
                    $cat = $err->category;
                    if (array_key_exists($cat->name, $phonologicalErrors)){
                        if (array_key_exists($err->name, $phonologicalErrors[$cat->name])){
                            $phonologicalErrors[$cat->name][$err->name]++;
                        } else {
                            $phonologicalErrors[$cat->name][$err->name] = 1;
                        }
                            //$phonologicalErrors[$cat->name]
                    } else {
                        $phonologicalErrors[$cat->name] = [];
                        $phonologicalErrors[$cat->name][$err->name] = 1;
                    }
                }
            }
            arsort($omissions['sound_class']);
            arsort($omissions['articulation_place']);
            arsort($omissions['voiced_voiceless']);
            arsort($substitutions['sound_class']);
            arsort($substitutions['articulation_place']);
            arsort($substitutions['voiced_voiceless']);
            ksort($phonologicalErrors);
            foreach ($phonologicalErrors as $cat){
                arsort($cat);
            }
			return View::make('test/result')
				->with('user', $user)
				->with('test', $test)
                ->with('omissions', $omissions)
                ->with('substitutions', $substitutions)
                ->with('distortions', $distortions)
                ->with('additions', $additions)
                ->with('phonological', $phonologicalErrors)
                ->with('totalCount', $test->module->imageCount())
                ->with('correctCount', Testrecording
                        ::where('test_id', '=', $test->id)
                        ->where('hasMistake', '=', false)
                        ->where('answered', '=', true)
                        ->count()
                )
                ->with('wrongCount', Testrecording
                        ::where('test_id', '=', $test->id)
                        ->where('hasMistake', '=', true)
                        ->count()
                )
                //->with('debug', $phonologicalErrors)
                ;
		}
		else
		{
            return redirect('/#/test');
		}

	}

	public function teststore($id)
	{
		foreach(array('audio') as $type) {
		    if (Input::file("${type}-blob")) {

		    	$file = Input::file("${type}-blob");
		        $fileName = Input::get("${type}-filename");
		        $imageId = Input::get('imageId');
		        $hasMistake = Input::get('hasMistake');
		        $wordplay = Input::get('wordplay');
		        $sentenceplay = Input::get('sentenceplay');
		        $directory = "uploads/recordings/";
		        $uploadDirectory = "uploads/recordings/$fileName";

		        $file->move($directory, $fileName);

		        $testRecording = new TestRecording();
		        $testRecording->filename = $fileName;
		        $testRecording->test_id = $id;
		        $testRecording->image_id = $imageId;
		        $testRecording->hasMistake = $hasMistake;
		        $testRecording->wordplay = $wordplay;
		        $testRecording->sentenceplay = $sentenceplay;

		        $testRecording->save();

		        //MISTAKES
		        $mistakes = json_decode(Input::get('mistakes'));
		        foreach ($mistakes as $mistake) {
		        	$testmistake = new Testmistake();
		        	$testmistake->test_id = $id;
		        	$testmistake->image_id = $mistake->imageId;
		        	$testmistake->mistake_id = $mistake->mistakeId;
		        	$testmistake->letterindex = $mistake->letterindex;
		        	$testmistake->beforeindex = $mistake->beforeindex;
		        	$testmistake->additionindex = $mistake->additionindex;
		        	$testmistake->substitution = $mistake->substitution;
		        	$testmistake->testrecording_id = $testRecording->id;
		        	$testmistake->save();
		        }

		        //echo($uploadDirectory);

		        $test = Test::find($id);
		        $test->indexend = $test->indexend + 1;
		        if( $test->indexend >= $test->module->moduleimages->count() )
				{
					$test->ended = new DateTime;


					$birthday = new DateTime($test->patient->birthdate);
					$diff = $birthday->diff(new DateTime());
					$months = $diff->format('%m') + 12 * $diff->format('%y');

					$agegroups = Agegroup::All();

					foreach ($agegroups as $agegroup) {
						if($agegroup->minage <= $months && $agegroup->maxage >= $months)
						{
							$test->agegroup_id = $agegroup->id;
						}
					}
				}
		        $test->update();
		    }
		}

		// return Redirect::route('test.test', [ 'id' => $id ] );
	}

	public function testanalyze($id)
	{
		$image_id = Input::get('imageId');
		$testrecording_id = Input::get('testrecordingId');
		$mistakes = json_decode(Input::get('mistakes'));

		$testmistakesDelete = Testmistake::where("testrecording_id", "=", $testrecording_id);
		$testmistakesDelete->Delete();

		foreach ($mistakes as $mistake) {
        	$testmistake = new Testmistake();
        	$testmistake->test_id = $id;
        	$testmistake->image_id = $mistake->imageId;
        	$testmistake->mistake_id = $mistake->mistakeId;
        	$testmistake->letterindex = $mistake->letterindex;
        	$testmistake->beforeindex = $mistake->beforeindex;
        	$testmistake->additionindex = $mistake->additionindex;
        	$testmistake->substitution = $mistake->substitution;
        	$testmistake->testrecording_id = $mistake->testrecordingId;
        	$testmistake->save();
		}

	}

	public function testanalyzed ($id)
	{
		$test = Test::find($id);
		$test->analyzed = new DateTime;
		$test->update();
		return Redirect::route('test.test', [$id]);
	}

}