<?php
/**
 * Created by PhpStorm.
 * User: BenjaminLierman
 * Date: 8/03/16
 * Time: 21:29
 */

namespace App\Http\Controllers\Auth;

use App\Http\Requests\StoreUserRequest;
use Illuminate\Support\Facades\View;
use Auth;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Support\Facades\Redirect;
use Mail;
use Illuminate\Support\Facades\Hash;

use App\Http\Controllers\Controller as Controller;

class AccountController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreUserRequest  $request
     * @return Response
     */
    public function store(StoreUserRequest $request)
    {
        $request->merge(['role_id' => 1]);
        if ( $user = new User( $request->all() )) {

            $user->save();
            return response()->json(['succes' => 'User aangemaakt.']);
        }
        else
        {
            return response()
                ->json($request->toArray());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function auth(){

        $rules = [
            'email'    => 'required|email',
            'password' => 'required',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {
            $credentials = [
                'email'      => Input::get('email'),
                'password'   => Input::get('password'),
                'deleted_at' => null, // Extra voorwaarde
            ];
            $remember = Input::get('switchAuth') == 'remember'; // onthoud authenticatie

            if (Auth::attempt($credentials, $remember)) {
                return response()->json(['succes' => 'User ingelogd.']);
            } else {
                return response()
                    ->json(['error' => 'Verkeerde inloggegevens.'])
                    ->setStatusCode(Response::HTTP_BAD_REQUEST);
            }
        } else {
            return response()
                ->json(['error' => 'Verkeerde inloggegevens.'])
                ->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
    }

    public function logout(){

        Auth::logout();
        return response()->json(['succes' => 'User uitgelogd.']);
    }

    public function reset()
    {
        /**
         * Show resetpage
         */
        return View::make('account/reset');
    }

    public function resetpost()
    {
        $userEdit = User::where('email', '=', Input::get('email') )->first();

        if( !empty($userEdit) )
        {
            $pass = substr(sha1(time()),0,8);

            $userEdit->password = Hash::make($pass);
            $userEdit->save();


            $user = array(
                'email'=>$userEdit->email,
                'name'=>$userEdit->name
            );

            $data = array(
                'pass' => $pass
            );

            Mail::send('account.mail', $data, function($message) use ($user)
            {
                $message->from('no-reply@ao-r.be', 'ao-r');
                $message->to($user['email'], $user['name'])->subject('Uw passwoord is gereset');
            });

            return response()
                ->json(['succes' => 'Als er een account verbonden is aan dit emailadres ontvangt u een mail met het nieuwe wachtwoord.']);
        }
        else
        {
            return response()
                ->json(['error' => 'User is niet ingelogd.'])
                ->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Check if the user is logged in.
     *
     * @return Response
     */

    public function check()
    {
        if (Auth::check()) {
            $user = Auth::user();
            return $user;
        }
        else
        {
            return response()
                ->json(['error' => 'User is niet ingelogd.'])
                ->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Check if the user is Admin.
     *
     * @return Response
     */

    public function checkAdmin()
    {
        if (Auth::check()) {
            $user = Auth::user();
            if($user->isRole('Admin'))
            {
                return $user;
            }
            else
            {
                return response()
                    ->json(['error' => 'User is geen admin.'])
                    ->setStatusCode(Response::HTTP_BAD_REQUEST);
            }

        }
        else
        {
            return response()
                ->json(['error' => 'User is niet ingelogd.'])
                ->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
    }
}