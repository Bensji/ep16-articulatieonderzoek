<?php

namespace App\Http\Controllers;

use App\Models\Test;
use Auth;
use Illuminate\Support\Facades\View;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
		$user = Auth::User();
		$alltests = Test::with('module')->get();
		$tests = [];
		foreach ($alltests as $test)
		{
			if( $test->patient->user->id == $user->id )
			{
				$tests[] = $test;
			}
		}

		$ended = [];
		$analyzed =  [];
		$unfinished = [];
		if($tests != null)
		{
			foreach ($tests as $test) {
				// $module = $test->module;
				// $imageCount = $module->moduleimages->count();
				// if($test->indexend == null)
				if( $test->analyzed != null)
				{
					$analyzed[] = $test;
				}elseif( $test->ended != null )
				{
					$ended[] = $test;
				}else
				{
					$unfinished[] = $test;
				}
			}
			if($ended)
			$ended = array_slice($ended, 0, 4);
			$analyzed = array_slice($analyzed, 0, 4);
			$unfinished = array_slice($unfinished, 0, 4);
		}

        return response()->json(['user' => $user, 'ended' => $ended, 'analyzed' => $analyzed, 'unfinished' => $unfinished]);

	}

}