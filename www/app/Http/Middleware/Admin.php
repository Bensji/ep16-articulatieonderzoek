<?php
/**
 * Created by PhpStorm.
 * User: BenjaminLierman
 * Date: 8/03/16
 * Time: 21:59
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{

    public function handle($request, Closure $next)
    {
        if (Auth::User()->isRole('Admin')) {
            return $next($request);
        }

        if ($request->ajax() || $request->wantsJson()) {
            return redirect('/#/home');
        } else {
            return redirect('/#/home');
        }


    }

}