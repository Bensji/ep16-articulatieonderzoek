<?php
/**
 * Created by PhpStorm.
 * User: BenjaminLierman
 * Date: 8/03/16
 * Time: 20:53
 */

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreUserRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return
            [
            'name' => 'required|max:255',
            'givenname' => 'required|max:255',
            'street' => 'required|max:255',
            'streetnumber' => 'required|max:255',
            'city' => 'required|max:255',
            'postalcode' => 'required|digits:4',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|max:255',
            'password_repeat' => 'required|same:password',
            'role_id' => 'required|digits:1'
        ];
    }

    public function messages()
    {
        return
        [
        'password_repeat.same' => 'De paswoorden zijn niet gelijk.',
        'name.required' => 'Gelieve u naam in te vullen.',
        'givenname.required' => 'Gelieve u naam in te vullen.',
        'email.required' => 'Gelieve u email in te vullen.',
        'email.email' => 'Gelieve een geldig emailadres in te vullen',
        'street.required' => 'Gelieve u straat in te vullen',
        'streetnumber.required' => 'Gelieve u huisnummer in te vullen',
        'city.required' => 'Gelieve u stad in te vullen',
        'postalcode.required' => 'Gelieve u postcode in te vullen',
        'role_id.required' => 'Gelieve een rol in te vullen'
        ];
    }
}