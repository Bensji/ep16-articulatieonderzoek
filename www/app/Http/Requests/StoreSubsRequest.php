<?php
/**
 * Created by PhpStorm.
 * User: BenjaminLierman
 * Date: 8/03/16
 * Time: 20:50
 */

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreSubsRequest extends Request
{
    protected $rules = [
        'default' => [
            'sub_id_1'		=> 'required|numeric',
            'sub_id_2'		=> 'required|numeric',
            'sub_id_3'		=> 'required|numeric',
            'sub_id_4'		=> 'required|numeric',
        ]

    ];

    protected $messages = [

    ];
}