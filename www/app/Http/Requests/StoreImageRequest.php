<?php
/**
 * Created by PhpStorm.
 * User: BenjaminLierman
 * Date: 8/03/16
 * Time: 18:25
 */

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreImageRequest extends Request
{
    protected $rules = [
        'default' => [
            'word'			=> 'required|max:255|unique:images',
            'phonetic'		=> 'required|max:255',
            'url'			=> 'required|mimes:jpeg,png,jpg,gif',
            'wordurl'		=> 'required|mimes:mpga,wav',
            'sentenceurl'	=> 'required|mimes:mpga,wav',
        ],
        'edit' => [
            'word'          => 'required|max:255|unique:images,word,@id',
            'url'           => 'mimes:jpeg,png,jpg,gif',
            'wordurl'       => 'mimes:mpga,wav',
            'sentenceurl'   => 'mimes:mpga,wav',
        ]

    ];

    protected $messages = [

        'url.mimes'         => 'Het bestand is niet het juiste type.(jpeg, png, of jpg)',
        'wordurl.mimes'     => 'Het bestand is niet het juiste type.(mp3 of wav)',
        'sentenceurl.mimes' => 'Het bestand is niet het juiste type.(mp3 of wav)',
    ];
}