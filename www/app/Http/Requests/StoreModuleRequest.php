<?php
/**
 * Created by PhpStorm.
 * User: BenjaminLierman
 * Date: 8/03/16
 * Time: 20:43
 */

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreModuleRequest extends Request
{
    protected $rules = [
        'default' => [
            'name'			=> 'required|max:255|unique:modules',
            'description'	=> 'required|max:255',
            'moduletype_id'	=> 'required',
            'images'		=> '',
        ],
        'edit' => [
            'name'          => 'required|max:255|unique:modules,name,@id',
        ]
    ];

    protected $messages = [
        '' => ''
    ];
}