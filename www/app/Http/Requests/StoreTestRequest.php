<?php
/**
 * Created by PhpStorm.
 * User: BenjaminLierman
 * Date: 8/03/16
 * Time: 16:12
 */

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreTestRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */


    public function rules()
    {
        return [

            'patient_id'		=> 'required',
            'module_id'			=> 'required',
        ];
    }

    public function messages()
    {
        return [

        '' => ''
        ];
    }
}