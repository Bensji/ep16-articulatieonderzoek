<?php
/**
 * Created by PhpStorm.
 * User: BenjaminLierman
 * Date: 8/03/16
 * Time: 20:53
 */

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditUserRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return
            [
                'name' => 'required|max:255',
                'givenname' => 'required|max:255',
                'street' => 'required|max:255',
                'streetnumber' => 'required|max:255',
                'city' => 'required|max:255',
                'postalcode' => 'required|digits:4',
                'email' => 'required|email',
                'user_id' => 'required'
            ];
    }

    public function messages()
    {
        return
            [
                'password_repeat.same' => 'De paswoorden zijn niet gelijk.'
            ];
    }
}