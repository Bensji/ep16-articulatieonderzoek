<?php
/**
 * Created by PhpStorm.
 * User: BenjaminLierman
 * Date: 8/03/16
 * Time: 15:48
 */

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreChildRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
                'name' => 'required|max:255',
                'givenname' => 'required|max:255',
                'email' => 'required|email',
                'street' => 'required|max:255',
                'streetnumber' => 'required|max:255',
                'city' => 'required|max:255',
                'postalcode' => 'required|digits:4',
                'birthdate' => 'required|date',
                'sex' => 'required|max:1',
                'initials' => 'required|max:3'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Gelieve u naam in te vullen.',
            'givenname.required' => 'Gelieve u naam in te vullen.',
            'email.required' => 'Gelieve u email in te vullen.',
            'email.email' => 'Gelieve een geldig emailadres in te vullen',
            'street.required' => 'Gelieve u straat in te vullen',
            'streetnumber.required' => 'Gelieve u huisnummer in te vullen',
            'city.required' => 'Gelieve u stad in te vullen',
            'postalcode.required' => 'Gelieve u postcode in te vullen',
            'birthdate.date' => 'Gelieve een geldige tijd en datum in te geven.',
            'birthdate.required' => 'Gelieve een geboortedatum in te geven.',
            'sex.required' => 'Gelieve u geslacht te specifieren.',
            'initials.required' => 'Gelieve u initialen in te geven.'
        ];
    }
}
