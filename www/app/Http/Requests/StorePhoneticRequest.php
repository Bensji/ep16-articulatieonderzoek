<?php
/**
 * Created by PhpStorm.
 * User: BenjaminLierman
 * Date: 8/03/16
 * Time: 20:46
 */

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StorePhoneticRequest extends Request
{
    protected $rules = [
        'default' => [
            'mistake_id_1'		=> 'required|numeric',
            'mistake_id_2'		=> 'required|numeric',
            'mistake_id_3'		=> 'required|numeric',
            'mistake_id_4'		=> 'required|numeric',
            'mistake_id_5'		=> 'required|numeric',
        ]

    ];

    protected $messages = [

    ];
}