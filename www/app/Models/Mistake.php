<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mistake extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mistakes';

    public $timestamps = false;

    //Relations

    /**
     * Relation with mistaketype table
     */
    public function parent(){
        return $this->belongsTo(Mistake::class, 'parent_id');
    }

    public function children(){
        return $this->hasMany(Mistake::class, 'parent_id');
    }

    public function phonetics(){
        return $this->belongsToMany(Phonetic::class);
    }

    public function testmistakes(){
        return $this->belongsToMany(Testmistake::class);
    }
}
