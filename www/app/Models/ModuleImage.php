<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModuleImage extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'modules_has_images';

    public $timestamps = false;

    //Relations
    //
    public function module(){
        return $this->belongsTo(Module::class);
    }
    public function image(){
        return $this->hasOne(Image::class, 'id', 'image_id');
    }
}
