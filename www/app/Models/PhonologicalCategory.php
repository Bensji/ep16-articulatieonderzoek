<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhonologicalCategory extends Model
{
    protected $table = 'phonological_categories';

    public function errors(){
        return $this->hasMany(PhonologicalError::class);
    }
}
