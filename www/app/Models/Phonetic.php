<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phonetic extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'phonetics';

    public $timestamps = false;

    //Relations
    //
    public function phoneticmistakes(){
        return $this->hasMany(PhoneticMistake::class);
    }
    public function subs(){
        return $this->hasMany(Subs::class);
    }
    public function distortions(){
        return $this->hasMany(Distortion::class);
    }
}
