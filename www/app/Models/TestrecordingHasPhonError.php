<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TestrecordingHasPhonError extends Model
{
    protected $table = 'testrecordings_phonological_errors';
}
