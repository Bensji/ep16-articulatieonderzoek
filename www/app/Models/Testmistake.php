<?php

namespace App\Models;

use Faker\Provider\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Testmistake extends Model
{
    use SoftDeletes;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'testmistakes';
    protected $dates = ['deleted_at'];

    //protected $softDelete = false;

    //Relations

    /**
     * Relation with test table
     */
    public function test()
    {
        return $this->belongsTo(Test::class);
    }
    /**
     * Relation with image table
     */
    public function image()
    {
        return $this->belongsTo(Image::class);
    }
    /**
     * Relation with mistake table
     */
    public function mistake()
    {
        return $this->belongsTo(Mistake::class);
    }
    /**
     * Relation with mistake table
     */
    public function testmistake()
    {
        return $this->belongsTo(Testrecording::class);
    }
}
