<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Moduletype extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'moduletypes';


    //Relations

    /**
     * Relation with patients table
     */
    public function modules()
    {
        return $this->hasMany(Module::class);
    }
}
