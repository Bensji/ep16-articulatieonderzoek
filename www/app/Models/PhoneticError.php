<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhoneticError extends Model
{
    protected $table = 'phonetic_errors';
}
