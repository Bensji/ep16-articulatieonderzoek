<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SoundClass extends Model
{
    protected $table = 'sound_classes';


    public function sounds(){
        return $this->hasMany(Sound::class, 'sound_class_id');
    }

    public function soundsNotInThisClass(){
        $sounds = Sound::where('sound_class_id', '!=', $this->id)->get();
        return $sounds;
    }
}
