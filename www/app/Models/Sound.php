<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sound extends Model
{
    protected $table = 'sounds';

    public function soundClass(){
        return $this->belongsTo(SoundClass::class);
    }
    public function articulationPlace(){
        return $this->belongsTo(ArticulationPlace::class, 'articulation_place_id');
    }
    public function distortions(){
        return $this->hasMany(Distortion::class);
    }
}
