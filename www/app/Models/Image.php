<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'images';
    protected $dates = ['deleted_at'];

    /**
     * Enable soft deleting
     */
    //protected $softDelete = true;


    //Relations
    public function modules(){
        return $this->belongsToMany(Module::class);
    }

    public function testmistakes(){
        return $this->belongsToMany(Testmistake::class);
    }

    public function testrecordings(){
        return $this->belongsToMany(Testrecording::class);
    }
    public function moduleimages(){
        return $this->hasMany(ModuleImage::class);
    }
    public function phonetics(){
        return $this->belongsToMany(Sound::class, 'image_has_sounds', 'image_id', 'sound_id')->orderBy('index', 'asc');
    }
    public function imageHasSounds(){
        return $this->hasMany(ImageHasSound::class);
    }
    public function phoneticString(){
        //$phonetics = Image::where('id', '=', $this->id)->phonetics()->get();
        $phonetics = $this->phonetics()->get();
        $string = '';
        foreach ($phonetics as $phonetic){
            $string .= $phonetic->phonetic;
        }
        return $string;
    }
}
