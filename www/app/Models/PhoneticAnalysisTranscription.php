<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhoneticAnalysisTranscription extends Model
{
    protected $table = 'phonetic_analysis_transcriptions';

    public function phonetic_error(){
        //$phonetics = Image::find($this->image_id)->phonetics;
        //$phoneticError = PhoneticError::where('replaced_id', '=')
    }

    public function sound(){
        return $this->belongsTo(Sound::class);
    }

    public function distortion(){
        return $this->belongsTo(Distortion::class);
    }
}
