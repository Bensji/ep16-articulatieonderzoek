<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subs extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'phonetic_has_phonetics';

    public $timestamps = false;

    //Relations
    //
    public function phonetic(){
        return $this->belongsTo(Phonetic::class, "phonetic_id_mistake");
    }
}
