<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agegroup extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'agegroups';


    protected $fillable = [
        'name',
        'minage',
        'maxage'
    ];

    //Relations

    /**
     * Relation with users table
     */
    public function users()
    {
        return $this->belongsToMany(Test::class);
    }
}
