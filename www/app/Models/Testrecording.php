<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Testrecording extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'testrecordings';
    protected $dates = ['deleted_at'];

    //protected $softDelete = true;

    //Relations

    /**
     * Relation with test table
     */
    public function test()
    {
        return $this->belongsTo(Test::class);
    }
    /**
     * Relation with image table
     */
    public function image()
    {
        return $this->belongsTo(Image::class);
    }
    /**
     * Relation with image table
     */
    public function testmistakes()
    {
        return $this->hasMany(Testmistake::class);
    }

    public function transcriptions()
    {
        $transcriptions = PhoneticAnalysisTranscription::where('test_id', '=', $this->test_id)
            ->where('image_id', '=', $this->image_id)
            ->orderBy('sound_index', 'asc')
            ->orderBy('addition_order', 'asc')
            ->with(array('sound' => function($query){
                $query->with(array('distortions' => function($query){

                }));
            }))
            ->get()
        ;
        return $transcriptions;
    }
    public function phonologicalErrors(){
        return $this->belongsToMany(PhonologicalError::class, 'testrecordings_phonological_errors',
            'testrecording_id', 'phonological_error_id');
    }
}
