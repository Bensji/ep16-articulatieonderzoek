<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TestrecordingHasSound extends Model
{
    protected $table = 'testrecording_has_sound';
}
