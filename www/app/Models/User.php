<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use SoftDeletes;
    use Authenticatable;
    use CanResetPassword;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    protected $dates = ['deleted_at'];

    /**
     * Enable soft deleting
     */
    //protected $softDelete = true;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password');

    /**
     * Attributes where mass assignment is allowed
     * @var array
     */
    protected $fillable = [
        'name',
        'givenname',
        'email',
        'password',
        'street',
        'streetnumber',
        'city',
        'postalcode',
        'role_id'
    ];

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    public static function boot()
    {
        parent::boot();

        /**
         * Hook die aangesproken wordt als het model voor de eerste keer naar de database weggeschreven wordt.
         * Zie: http://laravel.com/docs/eloquent#model-events
         */
        self::creating(function ($user) {
            $user->password = bcrypt($user->password);
        });

        // self::updating(function ($user) {
        //     $user->password = Hash::make($user->password);
        // });
    }


    //Relationships

    /**
     * Relation with roles table
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * Relation with patients table
     */
    public function patients()
    {
        return $this->hasMany(Patient::class);
    }


    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    /*
     * Check if user is $roleName
     */
    public function isRole($roleName)
    {
        foreach ($this->role()->get() as $role)
        {
            if ($role->name == $roleName)
            {
                return true;
            }
        }

        return false;
    }
}
