<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Module extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'modules';
    protected $dates = ['deleted_at'];

    /**
     * Enable soft deleting
     */
    //protected $softDelete = true;


    //Relations

    /**
     * Relation with moduletypes table
     */
    public function moduletype()
    {
        return $this->belongsTo(Moduletype::class);
    }

    public function moduleimages(){
        return $this->hasMany(ModuleImage::class)->orderBy('index', 'asc');
    }

    public function imageCount(){
        return ModuleImage::where('module_id', '=', $this->id)->count();
    }

    public function tests(){
        return $this->hasMany(Test::class);
    }

    public static function boot()
    {
        parent::boot();
        //static::deleting(function($module){
        //
        //});
        static::deleted(function($module){
            $id = $module->id;
            Test::where('module_id', '=', $id)->delete();
        });
        static::restored(function($module){
            $id = $module->id;
            //$tests =
            Test::onlyTrashed()
                ->where('module_id', $id)->restore();
            //->where('deleted_at', '=', new DateTime($module->deleted_at))
            //->restore();
            /*foreach ($tests as $test){
                $test->deleted_at = null;
                $test->save();
            }*/
        });
    }
}
