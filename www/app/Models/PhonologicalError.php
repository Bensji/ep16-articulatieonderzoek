<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhonologicalError extends Model
{
    protected $table = 'phonological_errors';

    public function category(){
        return $this->belongsTo(PhonologicalCategory::class, 'phonological_category_id');
    }
}
