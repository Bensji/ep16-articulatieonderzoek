<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhoneticMistake extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'phonetics_has_mistakes';

    public $timestamps = false;

    //Relations
    //
    public function mistake(){
        return $this->belongsTo(Mistake::class);
    }
    public function phonetics(){
        return $this->belongsToMany(Phonetic::class);
    }
}
