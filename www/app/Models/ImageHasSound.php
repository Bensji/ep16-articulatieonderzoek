<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImageHasSound extends Model
{
    protected $table = 'image_has_sounds';
}
