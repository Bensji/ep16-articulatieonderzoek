<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Patient extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'patients';
    protected $dates = ['deleted_at'];
    /**
     * Enable soft deleting
     */
    //protected $softDelete = true;

    /**
     * Attributes where mass assignment is allowed
     * @var array
     */

    //FILLABLE FOR FINALIZED APP
    protected $fillable = [
        'name',
        'givenname',
        'email',
        'street',
        'streetnumber',
        'city',
        'postalcode',
        'birthdate',
        'sex',
        'initials',
        'user_id'
    ];

    //FILLABLE FOR TEST WITH STUDENTS
    /*protected $fillable = [
        'name',
        'postalcode',
        'sex',
        'user_code',
        'initials',
        'user_id'
    ];*/


    //Relations

    /**
     * Relation with users table
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Relation with test table
     */
    public function tests()
    {
        return $this->belongsToMany(Test::class);
    }

    public static function boot()
    {
        parent::boot();

        static::deleted(function($patient){
            $id = $patient->id;
            Test::where('patient_id', '=', $id)->delete();
        });
        static::restored(function($patient){
            $id = $patient->id;
            Test::onlyTrashed()
                ->where('patient_id', '=', $id)->restore();
        });
    }
}
