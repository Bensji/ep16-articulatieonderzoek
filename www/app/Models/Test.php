<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Test extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tests';
    protected $dates = ['deleted_at'];

    //protected $softDelete = true;

    /**
     * Attributes where mass assignment is allowed
     * @var array
     */
    protected $fillable = [
        'forstats',
        'patient_id',
        'module_id',
        'agegroup_id'
    ];

    //Relations

    /**
     * Relation with moduletypes table
     */
    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }

    public function module(){
        return $this->belongsTo(Module::class);
    }

    public function testmistakes(){
        return $this->hasMany(Testmistake::class);
    }

    public function testrecordings(){
        return $this->belongsToMany(Testrecording::class);
    }

    public function agegroup(){
        return $this->hasOne(Agegroup::class);
    }

    public function imageIndex($imageId){
        $module = Module::where('id', '=', $this->module_id)->first();
        $moduleImage = ModuleImage::where('module_id', '=', $module->id)
            ->where('image_id', '=', $imageId)
            ->first()
        ;
        return $moduleImage->index;
        //return 1;
    }

    public function unansweredImages(){
        $testrecordings = TestRecording::where('test_id', '=', $this->id)->get();
        $images = [];
        foreach ($testrecordings as $recording){
            $image = Image::where('id', '=', $recording->image_id)->first();
            array_push($images, $image);
        }
        return $images;
    }
}
