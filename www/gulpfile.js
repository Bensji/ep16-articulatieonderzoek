/*

    Auteur: Benjamin Lierman
    Datum: 29/02/2016
    Contact: benjamin@keidigitaal.be

 */

var elixir = require('laravel-elixir');
var elixirTypscript = require('elixir-typescript');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.css.outputFolder = 'styles/css'; // Override default 'css'
elixir.config.js.outputFolder  = 'scripts/js'; // Override default 'js'

var path = {
        base : './',
        npm: 'node_modules',
        css  : elixir.config.publicPath + '/' + elixir.config.css.outputFolder,
        js   : elixir.config.publicPath + '/' + elixir.config.js.outputFolder
        },
        npmFiles = {
            lodash: path.npm + '/lodash/lodash.min.js',
            jquerydt: path.npm + '/definitely-typed-jquery/jquery.d.ts',
            uirouterdt: path.npm + '/definitely-typed-angular-ui-router/angular-ui-router.d.ts'
        },
        angularFiles = {
            angulardt: path.npm + '/definitely-typed-angular/angular.d.ts',
            routedt: path.npm + '/definitely-typed-angular/angular-route.d.ts',
            angular: path.npm + '/angular/angular.js',
            route: path.npm + '/angular-route/angular-route.js',
            resource: path.npm + '/angular-resource/angular-resource.js',
            aria: path.npm + '/angular-aria/angular-aria.js',
            messages: path.npm + '/angular-messages/angular-messages.js',
            sanitize: path.npm + '/angular-sanitize/angular-sanitize.js',
            uirouter: path.npm + '/angular-ui-router/release/angular-ui-router.js',
            pagination: path.npm + '/angular-utils-pagination/dirPagination.js'
        };


elixir(function(mix) {

    mix

        .copy(
            angularFiles.angulardt,
            elixir.config.assetsPath + '/typescript' + '/definitely-typed-angular/'
        )
        .copy(
            angularFiles.routedt,
            elixir.config.assetsPath + '/typescript' + '/definitely-typed-angular/'
        )
        .copy(
            npmFiles.uirouterdt,
            elixir.config.assetsPath + '/typescript' + '/definitely-typed-angular/'
        )

        .copy(
            npmFiles.jquerydt,
            elixir.config.assetsPath + '/typescript' + '/definitely-typed-jquery/'
        )

        .scripts([
            angularFiles.angular,
            angularFiles.route,
            angularFiles.resource,
            angularFiles.aria,
            angularFiles.messages,
            angularFiles.sanitize,
            angularFiles.uirouter,
            angularFiles.pagination
        ],
            elixir.config.publicPath + '/scripts/lib/angular.js',
            path.base
        )

        .copy(
            npmFiles.lodash,
            elixir.config.publicPath + '/scripts/lib/lodash.min.js'
        )
        /*
        *
        * Eerst Alle scss compilen
        *
        */
        .sass(
            '**/*.scss',
            path.css + '/app.css'
        )

        /*
         *
         * concat + minify + copy
         *
         */

       /* .stylesIn(
            elixir.config.assetsPath + '/' + elixir.config.css.folder + '/',
            path.css + '/app.css'
        )*/

        /*
         *
         * Eerst Alle ts compilen
         *
         */

        .typescript(
            '**/*.ts',
            path.js + '/app.js'
        )

        /*
         *
         * concat + minify + copy
         *
         */

        /*.scriptsIn(
            elixir.config.assetsPath + '/typescript/',
            path.js + '/app.js'
        )*/

/*        .scriptsIn(
            elixir.config.assetsPath + '/typescript/definitely-typed-angular/',
            path.js + '/libs/angular.js'
        )
        .scriptsIn(
            elixir.config.assetsPath + '/typescript/definitely-typed-jquery/',
            path.js + '/libs/jquery.js'
        )*/

        .copy(
            elixir.config.assetsPath + '/images',
            elixir.config.publicPath + '/images/'
        )

        .copy(
            elixir.config.assetsPath + '/html/templates',
            elixir.config.publicPath + '/templates/'
        )
    ;
});
