// <reference path="../_referencepaths.ts"/>

module aoControllers
{
    'use strict';

    import Services = aoServices;

    export class AdminController {

        errorMessage: any;
        succesMessage: string;

        newUsersData: any;

        currentPage: number;
        numPerPage: number;
        pageSize: any;

        static $inject = [
            'adminUserService',
            '$location'
        ];

        loggedInUserData: any;

        constructor(private adminUserService: Services.adminUserService, private $location: ng.ILocationService) {

            this.indexAdminUserCtrl();

        }

        indexAdminUserCtrl()
        {
            this.adminUserService.indexUserDataServ()
                .then((data) => {
                    if(data["error"] != null)
                    {
                        this.errorMessage = data["error"];
                    }
                    else
                    {
                        console.log(data);
                        this.newUsersData = data;
                    }
                });
        }

        deleteUserCtrl(id)
        {
            this.adminUserService.deleteUserData(id)
                .then((data) => {
                    if(data["data"]["error"] != null)
                    {
                        console.log("neuwp");
                        this.errorMessage = data["data"]["error"];
                    }
                    else
                    {
                        console.log(data);
                        this.succesMessage = data["data"]["succes"];
                        this.indexAdminUserCtrl();
                    }
                })
        }

        lockUserCtrl(id)
        {
            this.adminUserService.lockUserData(id)
                .then((data) => {
                    if(data["data"]["error"] != null)
                    {
                        this.errorMessage = data["data"]["error"];
                    }
                    else
                    {
                        console.log(data);
                        this.indexAdminUserCtrl();
                        this.succesMessage = data["data"]["succes"];
                    }
                })
        }

        unlockUserCtrl(id)
        {
            this.adminUserService.unlockUserData(id)
                .then((data) => {
                    if(data["data"]["error"] != null)
                    {
                        this.errorMessage = data["data"]["error"];
                    }
                    else
                    {
                        this.indexAdminUserCtrl();
                        this.succesMessage = data["data"]["succes"];
                    }
                })
        }




    }

    angular.module('aoControllers')
        .controller('AdminController', AdminController);
}