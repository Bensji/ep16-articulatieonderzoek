// <reference path="../_referencepaths.ts"/>

module aoServices {

    'use strict';

    export class AdminService {

        public static $inject = [
            '$http'
        ];

        constructor(private $http: angular.IHttpService) {
        }

        getNewUsersServ()
        {
            return this.$http.get('/admin/');
        }



    }

    angular.module('aoServices')
        .service('AdminService', AdminService);

}