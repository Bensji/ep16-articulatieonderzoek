// <reference path="../../_referencepaths.ts"/>

module aoControllers
{
    'use strict';

    import Services = aoServices;

    export class showChildAdminController {

        static $inject = [
            'adminChildService',
            '$location',
            '$stateParams'
        ];

        singleChildData: any;
        errorMessage: string;

        constructor(private adminChildService: Services.adminChildService, private $location: ng.ILocationService, private $stateParams: angular.ui.IStateParamsService) {
            this.showChildCtrl($stateParams["id"]);
        }

        showChildCtrl(id)
        {
            this.adminChildService.showChildServ(id)
                .then((data) => {
                    if(data["error"] != null)
                    {
                        this.errorMessage = data["error"];
                    }
                    else
                    {
                        this.singleChildData = data;
                    }
                })
        }
    }

    angular.module('aoControllers')
        .controller('showChildAdminController', showChildAdminController);
}