// <reference path="../../_referencepaths.ts"/>

module aoControllers
{
    'use strict';

    import Services = aoServices;

    export class createChildAdminController {

        static $inject = [
            'adminChildService',
            '$location'
        ];

        createChildData: any;

        name: string;
        givenname: string;
        street: string;
        streetnumber: number;
        city: string;
        postalcode: number;
        initials: string;
        email: string;
        sex: string;
        birthdate: any;

        errorMessage: any;
        succesMessage: string;

        users: any;
        defaultUser: any;

        constructor(private adminChildService: Services.adminChildService, private $location: ng.ILocationService) {
            this.getUsersCtrl();
        }

        getUsersCtrl()
        {
            this.adminChildService.getUsers()
                .then((data) => {
                    console.log(data);
                    this.users = data["data"]["users"];
                    this.defaultUser = {id: data["data"]["users"][0]["id"], value: data["data"]["users"][0]["name"]};
                });
        }

        createChildCtrl()
        {
            this.createChildData = {
                name: this.name,
                givenname: this.givenname,
                email: this.email,
                street: this.street,
                streetnumber: this.streetnumber,
                city: this.city,
                postalcode: this.postalcode,
                initials: this.initials,
                birthdate: this.birthdate,
                sex: this.sex,
                user_id: this.defaultUser.id
            };

            this.adminChildService.createChildServ(this.createChildData)
                .success((data) => {
                    if(data["succes"] != null)
                    {
                        this.succesMessage = data["succes"];
                        this.$location.path('/admin/child');
                    }
                    else
                    {
                        this.errorMessage = data;
                    }
                })
                .error((data) => {
                    this.errorMessage = data;
                    console.log(data);
                });
        }
    }

    angular.module('aoControllers')
        .controller('createChildAdminController', createChildAdminController);
}