// <reference path="../_referencepaths.ts"/>

module aoServices {

    'use strict';

    export class adminChildService {

        public static $inject = [
            '$http'
        ];

        constructor(private $http: angular.IHttpService) {
        }


        getUsers()
        {
            return this.$http.get('/admin/user/');
        }

        getChildDataServ()
        {
            return this.$http.get('/admin/child/');
        }

        showChildServ(id)
        {
            return this.$http.get('/admin/child/' + id);
        }

        editChildDataServ(id, editChildData)
        {
            console.log(editChildData);
            return this.$http({
                method: 'PATCH',
                url: '/admin/child/' + id,
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $.param(editChildData)
            });

        }

        deleteChildServ(id)
        {
            return this.$http.delete('/admin/child/' + id);
        }



        createChildServ(createChildData)
        {
            console.log(createChildData);
            return this.$http({
                method: 'POST',
                url: '/admin/child/',
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $.param(createChildData)
            });
        }
    }

    angular.module('aoServices')
        .service('adminChildService', adminChildService);

}