// <reference path="../../_referencepaths.ts"/>

module aoControllers
{
    'use strict';

    import Services = aoServices;

    export class editChildAdminController {

        static $inject = [
            'adminChildService',
            '$location',
            '$stateParams'
        ];

        childData: any;
        createChildData: any;
        editChildData: any;
        singleChildData: any;

        name: string;
        givenname: string;
        street: string;
        streetnumber: number;
        city: string;
        postalcode: number;
        initials: string;
        email: string;
        sex: string;
        birthdate: any;

        dropdownuser: any;
        thisuser: any;

        errorMessage: any;
        succesMessage: string;
        createChildForm: any;

        constructor(private adminChildService: Services.adminChildService, private $location: ng.ILocationService, private $stateParams: angular.ui.IStateParamsService) {
            this.bindChildCtrl($stateParams["id"]);
        }

        bindChildCtrl(id)
        {
            this.adminChildService.showChildServ(id)
                .success((data) => {
                    console.log(data);
                        this.name = data["child"]["name"];
                        this.givenname = data["child"]["givenname"];
                        this.email = data["child"]["email"];
                        this.street = data["child"]["street"];
                        this.streetnumber = parseFloat(data["child"]["streetnumber"]);
                        this.city =  data["child"]["city"];
                        this.postalcode =  parseFloat(data["child"]["postalcode"]);
                        this.initials =  data["child"]["initials"];
                        this.birthdate =  data["child"]["birthdate"];
                        this.sex =  data["child"]["sex"];

                        this.dropdownuser = data["users"];
                        this.thisuser = { id: data["child"]["user"]["id"], value: data["child"]["user"]["givenname"] };

                })
                .error((data) => {
                    this.errorMessage = data;
                });
        }

        editChildCtrl(id)
        {
            this.editChildData = {
                name: this.name,
                givenname: this.givenname,
                email: this.email,
                street: this.street,
                streetnumber: this.streetnumber,
                city: this.city,
                postalcode: this.postalcode,
                initials: this.initials,
                birthdate: this.birthdate,
                sex: this.sex,
                user_id: this.thisuser.id
            };

            this.adminChildService.editChildDataServ(this.$stateParams["id"], this.editChildData)
                .success((data) => {
                    console.log(data);
                    this.succesMessage = data["succes"];
                    this.$location.path('/admin/child');
                })
                .error((data) => {
                    console.log(data);
                    console.log("error");
                    this.errorMessage = data;
                });
        }
    }

    angular.module('aoControllers')
        .controller('editChildAdminController', editChildAdminController);
}