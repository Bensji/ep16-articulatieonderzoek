// <reference path="../../_referencepaths.ts"/>

module aoControllers
{
    'use strict';

    import Services = aoServices;

    export class editUserAdminController {

        static $inject = [
            'adminUserService',
            '$location',
            '$stateParams'
        ];

        childData: any;
        createChildData: any;
        editUserData: any;
        singleChildData: any;

        name: string;
        givenname: string;
        street: string;
        streetnumber: number;
        city: string;
        postalcode: number;
        initials: string;
        email: string;
        sex: string;
        birthdate: any;
        userrole: any;

        dropdown: any;

        errorMessage: any;
        succesMessage: string;
        createChildForm: any;

        constructor(private adminUserService: Services.adminUserService, private $location: ng.ILocationService, private $stateParams: angular.ui.IStateParamsService) {
            this.bindUserCtrl($stateParams["id"]);
        }

        bindUserCtrl(id)
        {
            this.adminUserService.showUserDataServ(id)
                .success((data) => {
                    console.log(data);
                        this.name = data["user"]["name"];
                        this.givenname = data["user"]["givenname"];
                        this.email = data["user"]["email"];
                        this.street = data["user"]["street"];
                        this.streetnumber = parseFloat(data["user"]["streetnumber"]);
                        this.city =  data["user"]["city"];
                        this.postalcode =  parseFloat(data["user"]["postalcode"]);
                        this.userrole = { id: data["user"]["role"]["id"], name: data["user"]["role"]["name"]};

                        this.dropdown = data["role"];
                })
                .error((data) => {
                    this.errorMessage = data;
                });
        }

        editUserCtrl(id)
        {
            this.editUserData = {
                name: this.name,
                givenname: this.givenname,
                email: this.email,
                street: this.street,
                streetnumber: this.streetnumber,
                city: this.city,
                postalcode: this.postalcode,
                role_id: this.userrole.id
            };

            this.adminUserService.editUserDataServ(this.$stateParams["id"], this.editUserData)
                .success((data) => {
                    this.succesMessage = data["succes"];
                    this.$location.path('/admin');
                })
                .error((data) => {
                    this.errorMessage = data;
                });
        }
    }

    angular.module('aoControllers')
        .controller('editUserAdminController', editUserAdminController);
}