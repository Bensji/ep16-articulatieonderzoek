// <reference path="../../_referencepaths.ts"/>

module aoControllers
{
    'use strict';

    import Services = aoServices;

    export class createUserAdminController {

        static $inject = [
            'adminUserService',
            '$location'
        ];

        createUserData: any;
        userRoles: any;
        defaultUserRole: any;

        name: string;
        givenname: string;
        street: string;
        streetnumber: number;
        city: string;
        postalcode: number;
        initials: string;
        email: string;
        sex: string;
        birthdate: any;
        userrole: any;
        password: string;
        password_repeat: string;
        role_id: number;

        errorMessage: any;
        succesMessage: string;

        constructor(private adminUserService: Services.adminUserService, private $location: ng.ILocationService) {
            this.getUserRoles();
        }

        getUserRoles()
        {
            this.adminUserService.getUserRoles()
                .then((data) => {
                    this.userRoles = data["data"]["roles"];
                    this.defaultUserRole = {id: data["data"]["roles"][0]["id"], value: data["data"]["roles"][0]["name"]};
                });
        }

        createUserCtrl()
        {
            this.createUserData = {
                name: this.name,
                givenname: this.givenname,
                email: this.email,
                street: this.street,
                streetnumber: this.streetnumber,
                city: this.city,
                postalcode: this.postalcode,
                password: this.password,
                password_repeat: this.password_repeat,
                role_id: this.defaultUserRole.id
            };

            this.adminUserService.createUserDataServ(this.createUserData)
                .success((data) => {
                    if(data["succes"] != null)
                    {
                        this.succesMessage = data["succes"];
                        this.$location.path('/admin/user');
                    }
                    else
                    {
                        this.errorMessage = data;
                    }
                })
                .error((data) => {
                    this.errorMessage = data;
                });
        }
    }

    angular.module('aoControllers')
        .controller('createUserAdminController', createUserAdminController);
}