// <reference path="../../_referencepaths.ts"/>

module aoControllers
{
    'use strict';

    import Services = aoServices;

    export class indexUserAdminController {

        static $inject = [
            'adminUserService',
            '$location',
            '$stateParams'
        ];

        newUsersData: any;

        currentPage: number;
        numPerPage: number;
        pageSize: any;

        singleChildData: any;
        succesMessage: string;
        errorMessage: string;

        constructor(private adminUserService: Services.adminUserService, private $location: ng.ILocationService, private $stateParams: angular.ui.IStateParamsService) {
            this.currentPage = 1;
            this.pageSize = 10;

            this.indexAdminUserCtrl();
        }

        indexAdminUserCtrl()
        {
            this.adminUserService.indexAllUserDataServ()
                .then((data) => {
                    if(data["data"]["error"] != null)
                    {
                        this.errorMessage = data["data"]["error"];
                    }
                    else
                    {
                        console.log(data);
                        this.newUsersData = data;
                    }
                });
        }

        deleteUserCtrl(id)
        {
            this.adminUserService.deleteUserData(id)
                .then((data) => {
                    if(data["data"]["error"] != null)
                    {
                        console.log("neuwp");
                        this.errorMessage = data["data"]["error"];
                    }
                    else
                    {
                        console.log(data);
                        this.succesMessage = data["data"]["succes"];
                        this.indexAdminUserCtrl();
                    }
                })
        }

        lockUserCtrl(id)
        {
            this.adminUserService.lockUserData(id)
                .then((data) => {
                    if(data["data"]["error"] != null)
                    {
                        this.errorMessage = data["data"]["error"];
                    }
                    else
                    {
                        console.log(data);
                        this.indexAdminUserCtrl();
                        this.succesMessage = data["data"]["succes"];
                    }
                })
        }

        unlockUserCtrl(id)
        {
            this.adminUserService.unlockUserData(id)
                .then((data) => {
                    if(data["data"]["error"] != null)
                    {
                        this.errorMessage = data["data"]["error"];
                    }
                    else
                    {
                        this.indexAdminUserCtrl();
                        this.succesMessage = data["data"]["succes"];
                    }
                })
        }
    }

    angular.module('aoControllers')
        .controller('indexUserAdminController', indexUserAdminController);
}