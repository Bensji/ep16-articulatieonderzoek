// <reference path="../_referencepaths.ts"/>

module aoServices {

    'use strict';

    export class adminUserService {

        public static $inject = [
            '$http'
        ];

        constructor(private $http: angular.IHttpService) {
        }

        indexUserDataServ()
        {
            return this.$http.get('/admin/');
        }

        indexAllUserDataServ()
        {
            return this.$http.get('/admin/user/');
        }

        showUserDataServ(id)
        {
            return this.$http.get('/admin/user/' + id);
        }

        createUserDataServ(userCreateData)
        {
            console.log(userCreateData);
            return this.$http({
                method: 'POST',
                url: '/admin/user/',
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $.param(userCreateData)
            });

        }

        editUserDataServ(id, userEditData)
        {
            return this.$http({
                method: 'PATCH',
                url: '/admin/user/' + id,
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $.param(userEditData)
            });

        }

        deleteUserData(id)
        {
            return this.$http.delete('/admin/user/' + id);
        }

        lockUserData(id)
        {
            return this.$http({
                method: 'PUT',
                url: '/admin/user/lock/' + id,
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' }
            });
            //return this.$http.put('/admin/user/lock/' + id);
        }

        unlockUserData(id)
        {
            return this.$http({
                method: 'PUT',
                url: '/admin/user/unlock/' + id,
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' }
            });
        }

        getUserRoles()
        {
            return this.$http.get('/admin/getroles');
        }
    }

    angular.module('aoServices')
        .service('adminUserService', adminUserService);

}