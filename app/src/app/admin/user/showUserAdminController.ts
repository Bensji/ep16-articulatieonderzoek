// <reference path="../../_referencepaths.ts"/>

module aoControllers
{
    'use strict';

    import Services = aoServices;

    export class showUserAdminController {

        static $inject = [
            'adminUserService',
            '$location',
            '$stateParams'
        ];

        singleUserData: any;
        errorMessage: string;

        constructor(private adminUserService: Services.adminUserService, private $location: ng.ILocationService, private $stateParams: angular.ui.IStateParamsService) {
            this.showAdminUserCtrl($stateParams["id"]);
        }

        showAdminUserCtrl(id)
        {
            this.adminUserService.showUserDataServ(id)
                .then((data) => {
                    if(data["error"] != null)
                    {
                        this.errorMessage = data["error"];
                    }
                    else
                    {
                        console.log(data);
                        this.singleUserData = data;
                    }
                })
        }
    }

    angular.module('aoControllers')
        .controller('showUserAdminController', showUserAdminController);
}