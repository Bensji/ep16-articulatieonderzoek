/*
*
*   Auteur: Benjamin Lierman
*   Datum: 29/02/2026
*   contact: benjamin@keidigitaal.be
*
*/

// <reference path="_referencepaths.ts"/>

((): void => {

    "use strict";

        angular.module("aoApp",
            [
                "ui.router",
                "ngRoute",
                "ngSanitize",
                "ngResource",
                "aoControllers",
                "aoServices",
                'angularUtils.directives.dirPagination'
            ]);

        angular.module("aoControllers", []);
        angular.module("aoServices", []);

})();