// <reference path="../_referencepaths.ts"/>

module aoServices {

    'use strict';

    export class HomeService {

        public static $inject = [
            '$http'
        ];

        constructor(private $http: angular.IHttpService) {
        }

        getHomeDataServ()
        {
            return this.$http.get('/home/');
        }



    }

    angular.module('aoServices')
        .service('HomeService', HomeService);

}