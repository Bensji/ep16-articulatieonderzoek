// <reference path="../_referencepaths.ts"/>

module aoControllers
{
    'use strict';

    import Services = aoServices;

    export class HomeController {

        static $inject = [
            'HomeService'
        ];

        homeData: any;

        constructor(private HomeService: Services.HomeService) {
            this.getHomeDataCtrl();
        }

        getHomeDataCtrl()
        {
            this.HomeService.getHomeDataServ()
            .then((data) => {
                this.homeData = data;
                console.log(this.homeData);
            });
        }


    }

angular.module('aoControllers')
    .controller('HomeController', HomeController);
}