// <reference path="../_referencepaths.ts"/>

module aoServices {

    'use strict';

    export class AuthService {

        public static $inject = [
            '$http',
            '$q'
        ];

        constructor(private $http: angular.IHttpService, private $q) {
        }

        loginUser(userData)
        {
            return this.$http({
                method: 'POST',
                url: '/login/',
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $.param(userData)
            });

            //return this.$http.get("http://jsonplaceholder.typicode.com/posts/1");
        }

        registerUser(registerData)
        {
            return this.$http({
                method: 'POST',
                url: '/register/',
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $.param(registerData)
            });
        }

        resetUser(resetData)
        {
            return this.$http({
                method: 'POST',
                url: '/reset/',
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $.param(resetData)
            });
        }

        logoutUser()
        {
            return this.$http.get('/logout/');
        }

        authenticate()
        {
            return this.$http.get('/check/');
        }

        authenticateAdmin()
        {
            return this.$http.get('/admin/check/');
        }

    }

    angular.module('aoServices')
        .service('AuthService', AuthService);

}