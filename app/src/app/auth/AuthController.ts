// <reference path="../_referencepaths.ts"/>

module aoControllers {

    'use strict';

    import Services = aoServices;

    export class AuthController {

        static $inject = [
            'AuthService',
            '$scope',
            '$location'
        ];

        data: any;
        userData: any;
        registerData: any;
        resetData: any;


        email: string;
        password: string;
        switchAuth: boolean;

        name: string;
        givenname: string;
        street: string;
        streetnumber: number;
        city: string;
        postalcode: number;
        password_repeat: string;

        errorMessage: any;
        succesMessage: string;

        constructor(private AuthService: Services.AuthService, public $scope: angular.IScope, private $location: ng.ILocationService)
        {
        }

        // function to handle submitting the form
        login()
        {
            this.userData = {
                email: this.email,
                password: this.password,
                switchAuth: this.switchAuth
            };

            this.AuthService.loginUser(this.userData)
                .success((data) => {
                    if(data["succes"] != null)
                    {
                        this.succesMessage = data["succes"];
                        this.$location.path('/home');
                    }
                    else
                    {
                        this.errorMessage = data["error"];
                    }
                })
                .error(() => {
                    this.errorMessage = "Er is iets misgegaan, probeer het opnieuw.";
                });
        }

        registerUser()
        {
            this.registerData = {
                name: this.name,
                givenname: this.givenname,
                street: this.street,
                streetnumber: this.streetnumber,
                postalcode: this.postalcode,
                city: this.city,
                email: this.email,
                password: this.password,
                password_repeat: this.password_repeat,
                role_id: 1
            };

            this.AuthService.registerUser(this.registerData)
                .success((data) => {
                    if(data["succes"] != null)
                    {
                        this.succesMessage = data["succes"];
                        this.$location.path('/auth');
                    }
                    else
                    {
                        console.log("Failed");
                        this.errorMessage = data;
                    }
                })
                .error((data) => {
                    console.log("Error");
                    this.errorMessage = data;
                    console.log(data);
                });
        }

        reset()
        {
            this.resetData = {
                email: this.email
            };

            this.AuthService.resetUser(this.resetData)
                .success((data) => {
                    if(data["succes"] != null)
                    {
                        this.succesMessage = data["succes"];
                        this.$location.path('/auth');
                    }
                    else
                    {
                        this.errorMessage = data;
                    }
                })
                .error((data) => {
                    console.log("Error");
                    this.errorMessage = data;
                    console.log(data);
                });
        }

    }

    angular.module('aoControllers')
        .controller('AuthController', AuthController);
}