// <reference path="../_referencepaths.ts"/>

module aoControllers
{
    'use strict';

    import Services = aoServices;

    export class AppController {


        static $inject = [
            'AuthService',
            '$location'
        ];

        loggedInUserData: any;

        constructor(private AuthService: Services.AuthService, private $location: ng.ILocationService) {
            this.loggedInUser();
        }

        loggedInUser()
        {
            this.AuthService.authenticate()
                .then((data) => {
                    this.loggedInUserData = data;
                    console.log(data);
                });
        }

        logout()
        {
            this.AuthService.logoutUser()
                .success((data) => {
                    if(data["succes"] != null)
                    {
                        this.$location.path('/auth');
                    }
                    else
                    {
                        console.log(data);
                    }
                });
        }



    }

    angular.module('aoControllers')
        .controller('AppController', AppController);
}