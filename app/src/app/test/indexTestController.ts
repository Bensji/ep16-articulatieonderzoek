// <reference path="../../_referencepaths.ts"/>

module aoControllers
{
    'use strict';

    import Services = aoServices;

    export class indexTestController {

        static $inject = [
            'TestService',
            '$location'
        ];

        testData: any;

        currentPage: number;
        numPerPage: number;
        pageSize: any;

        errorMessage: any;
        succesMessage: string;

        constructor(private TestService: Services.TestService, private $location: ng.ILocationService) {
            this.currentPage = 1;
            this.pageSize = 10;

            this.getTestDataCtrl();
        }

        getTestDataCtrl()
        {
            this.TestService.getTestDataServ()
                .then((data) => {
                    if(data["error"] != null)
                    {
                        console.log(data);
                        this.errorMessage = data["error"];
                    }
                    else
                    {
                        console.log(data);
                        this.testData = data;
                    }
                });
        }

        deleteTestCtrl(id)
        {
            this.TestService.deleteTestServ(id)
                .then((data) => {
                    if(data["error"] != null)
                    {
                        this.errorMessage = "Test is niet verwijderd.";
                    }
                    else
                    {
                        this.succesMessage = "Test is verwijderd.";
                        this.getTestDataCtrl();
                    }
                })
        }

        pageChangeHandler(number)
        {
            console.log(number);
        }
    }

    angular.module('aoControllers')
        .controller('indexTestController', indexTestController);
}