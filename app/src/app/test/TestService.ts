// <reference path="../_referencepaths.ts"/>

module aoServices {

    'use strict';

    export class TestService {

        public static $inject = [
            '$http'
        ];

        constructor(private $http: angular.IHttpService) {
        }

        getTestDataServ()
        {
            return this.$http.get('/test/');
        }

        createTestServ(childData)
        {
            return this.$http({
                method: 'POST',
                url: '/test/',
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $.param(childData)
            });
        }

        editTestServ(id, childEditData)
        {
            return this.$http({
                method: 'PATCH',
                url: '/test/' + id,
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $.param(childEditData)
            });
        }

        showTestServ(id)
        {
            return this.$http.get('/test/' + id);
        }

        deleteTestServ(id)
        {
            return this.$http.delete('/test/' + id);
        }
    }

    angular.module('aoServices')
        .service('TestService', TestService);

}