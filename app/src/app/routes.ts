/*
 *
 *   Auteur: Benjamin Lierman
 *   Datum: 29/02/2026
 *   contact: benjamin@keidigitaal.be
 *
 */

// <reference path="_referencepaths.ts"/>

((): void => {

    angular
        .module('aoApp')
        .config(configureMainRoutes);

        configureMainRoutes.$inject = [
            '$stateProvider',
            '$urlRouterProvider'
        ];

    /* @ngInject */
    function configureMainRoutes(
        $stateProvider: angular.ui.IStateProvider,
        $urlRouterProvider: angular.ui.IUrlRouterProvider): void {
        // When nothing is specified, go to the login url
        $urlRouterProvider.when('', '/auth');
        // When trying to go to a route that does not exists show a 404
        // Why is this going crazy?!
        //$urlRouterProvider.otherwise('/404');

        // Public routes
        $stateProvider
            //General (Login, Register, 404, ...)
            .state('auth', {
                url: '/auth',
                templateUrl: '../../templates/auth/auth.view.html',
                controller: 'AuthController as auth'
            })
            .state('register', {
                url: '/register',
                templateUrl: '../../templates/auth/register.view.html',
                controller: 'AuthController as auth'
            })
            .state('reset', {
                url: '/reset',
                templateUrl: '../../templates/auth/reset.view.html',
                controller: 'AuthController as auth'
            })
            .state('404', {
                url: '/404',
                templateUrl: '../../templates/common/404.view.html',
                controller: 'AuthController as auth'
            })

            // Home (dashboard logged in)
            .state('home', {
                url: '/home',
                templateUrl: '../../templates/home/home.view.html',
                controller: 'HomeController as home',
                resolve: {
                    'auth' : function(AuthService){
                        return AuthService.authenticate();
                    }
                }
            })
            .state('test', {
                url: '/test',
                templateUrl: '../../templates/test/test-index.view.html',
                controller: 'indexTestController as test',
                resolve: {
                    'auth' : function(AuthService){
                        return AuthService.authenticate();
                    }
                }
            })

            .state('test/create', {
                url: '/test/create',
                templateUrl: '../../templates/test/test-create.view.html',
                controller: 'indexTestController as test',
                resolve: {
                    'auth' : function(AuthService){
                        return AuthService.authenticate();
                    }
                }
            })

            // Child pages (Edit, create, show)
            .state('child', {
                url: '/child',
                templateUrl: '../../templates/child/child-index.view.html',
                controller: 'indexChildController as child',
                resolve: {
                    'auth' : function(AuthService){
                        return AuthService.authenticate();
                    }
                }
            })
            .state('child/create', {
                url: '/child/create',
                templateUrl: '../../templates/child/child-create.view.html',
                controller: 'createChildController as child',
                resolve: {
                    'auth' : function(AuthService){
                        return AuthService.authenticate();
                    }
                }
            })
            .state('child/edit', {
                url: '/child/edit/{id}',
                templateUrl: '../../templates/child/child-edit.view.html',
                controller: 'editChildController as child',
                resolve: {
                    'auth' : function(AuthService){
                        return AuthService.authenticate();
                    }
                }
            })
            .state('child/show', {
                url: '/child/show/{id}',
                templateUrl: '../../templates/child/child-show.view.html',
                controller: 'showChildController as child',
                resolve: {
                    'auth' : function(AuthService){
                        return AuthService.authenticate();
                    }
                }
            })
            // Admin Pages
            .state('admin', {
                url: '/admin',
                templateUrl: '../../templates/admin/admin.view.html',
                controller: 'AdminController as admin',
                resolve: {
                    'admin' : function(AuthService){
                        return AuthService.authenticateAdmin();
                    }
                }
            })
            // Admin User Pages
            .state('admin/user/show', {
                url: '/admin/user/show/{id}',
                templateUrl: '../../templates/admin/user/user-show.view.html',
                controller: 'showUserAdminController as admin',
                resolve: {
                    'admin' : function(AuthService){
                        return AuthService.authenticateAdmin();
                    }
                }
            })
            .state('admin/user/edit', {
                url: '/admin/user/edit/{id}',
                templateUrl: '../../templates/admin/user/user-edit.view.html',
                controller: 'editUserAdminController as admin',
                resolve: {
                    'admin' : function(AuthService){
                        return AuthService.authenticateAdmin();
                    }
                }
            })
            .state('admin/user', {
                url: '/admin/user',
                templateUrl: '../../templates/admin/user/user-index.view.html',
                controller: 'indexUserAdminController as admin',
                resolve: {
                    'admin' : function(AuthService){
                        return AuthService.authenticateAdmin();
                    }
                }
            })
            .state('admin/user/create', {
                url: '/admin/user/create',
                templateUrl: '../../templates/admin/user/user-create.view.html',
                controller: 'createUserAdminController as admin',
                resolve: {
                    'admin' : function(AuthService){
                        return AuthService.authenticateAdmin();
                    }
                }
            })

            // Admin Child Pages
            .state('admin/child', {
                url: '/admin/child',
                templateUrl: '../../templates/admin/child/child-index.view.html',
                controller: 'indexChildAdminController as admin',
                resolve: {
                    'admin' : function(AuthService){
                        return AuthService.authenticateAdmin();
                    }
                }
            })
            .state('admin/child/show', {
                url: '/admin/child/show/{id}',
                templateUrl: '../../templates/admin/child/child-show.view.html',
                controller: 'showChildAdminController as admin',
                resolve: {
                    'admin' : function(AuthService){
                        return AuthService.authenticateAdmin();
                    }
                }
            })
            .state('admin/child/edit', {
                url: '/admin/child/edit/{id}',
                templateUrl: '../../templates/admin/child/child-edit.view.html',
                controller: 'editChildAdminController as admin',
                resolve: {
                    'admin' : function(AuthService){
                        return AuthService.authenticateAdmin();
                    }
                }
            })
            .state('admin/child/create', {
                url: '/admin/child/create',
                templateUrl: '../../templates/admin/child/child-create.view.html',
                controller: 'createChildAdminController as admin',
                resolve: {
                    'admin' : function(AuthService){
                        return AuthService.authenticateAdmin();
                    }
                }
            });
    }

})();