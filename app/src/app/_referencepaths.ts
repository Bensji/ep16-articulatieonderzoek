//Libs
/// <reference path='../typescript/definitely-typed-angular/angular.d.ts' />
/// <reference path='../typescript/definitely-typed-angular/angular-route.d.ts' />
/// <reference path='../typescript/definitely-typed-angular/angular-ui-router.d.ts' />
/// <reference path='../typescript/definitely-typed-jquery/jquery.d.ts' />

//Application
/// <reference path='app.ts' />
/// <reference path='routes.ts' />

//Admin
/// <reference path='admin/AdminController.ts' />
/// <reference path='admin/AdminService.ts' />
//Admin/Child
/// <reference path='admin/child/adminChildService.ts' />
/// <reference path='admin/child/createChildAdminController.ts' />
/// <reference path='admin/child/editChildAdminController.ts' />
/// <reference path='admin/child/indexChildAdminController.ts' />
/// <reference path='admin/child/showChildAdminController.ts' />
//Admin/User
/// <reference path='admin/user/adminUserService.ts' />
/// <reference path='admin/user/createUserAdminController.ts' />
/// <reference path='admin/user/editUserAdminController.ts' />
/// <reference path='admin/user/indexUserAdminController.ts' />
/// <reference path='admin/user/showUserAdminController.ts' />

//Child
/// <reference path='child/ChildService.ts' />
/// <reference path='child/createChildController.ts' />
/// <reference path='child/editChildController.ts' />
/// <reference path='child/indexChildController.ts' />
/// <reference path='child/showChildController.ts' />

//Common
/// <reference path='common/AppController.ts' />

//Home
/// <reference path='home/HomeService.ts' />
/// <reference path='home/HomeController.ts' />

//Test
/// <reference path='test/TestService.ts' />
/// <reference path='test/IndexTestController.ts' />
