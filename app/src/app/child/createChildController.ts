// <reference path="../../_referencepaths.ts"/>

module aoControllers
{
    'use strict';

    import Services = aoServices;

    export class createChildController {

        static $inject = [
            'ChildService',
            '$location'
        ];

        createChildData: any;

        name: string;
        givenname: string;
        street: string;
        streetnumber: number;
        city: string;
        postalcode: number;
        initials: string;
        email: string;
        sex: string;
        birthdate: any;

        errorMessage: any;
        succesMessage: string;

        constructor(private ChildService: Services.ChildService, private $location: ng.ILocationService) {
        }


        createChildCtrl()
        {
            this.createChildData = {
                name: this.name,
                givenname: this.givenname,
                email: this.email,
                street: this.street,
                streetnumber: this.streetnumber,
                city: this.city,
                postalcode: this.postalcode,
                initials: this.initials,
                birthdate: this.birthdate,
                sex: this.sex
            };

            this.ChildService.createChildServ(this.createChildData)
                .success((data) => {
                    if(data["succes"] != null)
                    {
                        this.succesMessage = data["succes"];
                        this.$location.path('/child');
                    }
                    else
                    {
                        this.errorMessage = data;
                    }
                })
                .error((data) => {
                    this.errorMessage = data;
                    console.log(data);
                });
        }
    }

    angular.module('aoControllers')
        .controller('createChildController', createChildController);
}