// <reference path="../../_referencepaths.ts"/>

module aoControllers
{
    'use strict';

    import Services = aoServices;

    export class indexChildController {

        static $inject = [
            'ChildService',
            '$location'
        ];

        childData: any;

        currentPage: number;
        numPerPage: number;
        pageSize: any;

        errorMessage: any;
        succesMessage: string;

        constructor(private ChildService: Services.ChildService, private $location: ng.ILocationService) {
            this.currentPage = 1;
            this.pageSize = 10;

            this.getChildDataCtrl();
        }

        getChildDataCtrl()
        {
            this.ChildService.getChildDataServ()
                .then((data) => {
                    if(data["error"] != null)
                    {
                        this.errorMessage = data["error"];
                    }
                    else
                    {
                        this.childData = data;
                    }
                });
        }

        deleteChildCtrl(id)
        {
            this.ChildService.deleteChildServ(id)
                .then((data) => {
                    if(data["error"] != null)
                    {
                        this.errorMessage = "Kind is niet verwijderd.";
                    }
                    else
                    {
                        this.succesMessage = "Kind is verwijderd.";
                        this.getChildDataCtrl();
                    }
                })
        }
    }

    angular.module('aoControllers')
        .controller('indexChildController', indexChildController);
}