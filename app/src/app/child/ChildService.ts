// <reference path="../_referencepaths.ts"/>

module aoServices {

    'use strict';

    export class ChildService {

        public static $inject = [
            '$http'
        ];

        constructor(private $http: angular.IHttpService) {
        }

        getChildDataServ()
        {
            return this.$http.get('/child/');
        }

        createChildServ(childData)
        {
            return this.$http({
                method: 'POST',
                url: '/child/',
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $.param(childData)
            });
        }

        editChildServ(id, childEditData)
        {
            return this.$http({
                method: 'PATCH',
                url: '/child/' + id,
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $.param(childEditData)
            });
        }

        showChildServ(id)
        {
            return this.$http.get('/child/' + id);
        }

        deleteChildServ(id)
        {
            return this.$http.delete('/child/' + id);
        }
    }

    angular.module('aoServices')
        .service('ChildService', ChildService);

}