// <reference path="../../_referencepaths.ts"/>

module aoControllers
{
    'use strict';

    import Services = aoServices;

    export class editChildController {

        static $inject = [
            'ChildService',
            '$location',
            '$stateParams'
        ];

        childData: any;
        createChildData: any;
        editChildData: any;
        singleChildData: any;

        name: string;
        givenname: string;
        street: string;
        streetnumber: number;
        city: string;
        postalcode: number;
        initials: string;
        email: string;
        sex: string;
        birthdate: any;

        errorMessage: any;
        succesMessage: string;
        createChildForm: any;

        constructor(private ChildService: Services.ChildService, private $location: ng.ILocationService, private $stateParams: angular.ui.IStateParamsService) {
            this.bindChildCtrl($stateParams["id"]);
        }

        bindChildCtrl(id)
        {
            this.ChildService.showChildServ(id)
                .success((data) => {

                        this.name = data["child"]["name"];
                        this.givenname = data["child"]["givenname"];
                        this.email = data["child"]["email"];
                        this.street = data["child"]["street"];
                        this.streetnumber = parseFloat(data["child"]["streetnumber"]);
                        this.city =  data["child"]["city"];
                        this.postalcode =  parseFloat(data["child"]["postalcode"]);
                        this.initials =  data["child"]["initials"];
                        this.birthdate =  data["child"]["birthdate"];
                        this.sex =  data["child"]["sex"];

                })
                .error((data) => {
                    this.errorMessage = data;
                });
        }

        editChildCtrl(id)
        {
            this.editChildData = {
                name: this.name,
                givenname: this.givenname,
                email: this.email,
                street: this.street,
                streetnumber: this.streetnumber,
                city: this.city,
                postalcode: this.postalcode,
                initials: this.initials,
                birthdate: this.birthdate,
                sex: this.sex
            };

            this.ChildService.editChildServ(this.$stateParams["id"], this.editChildData)
                .success((data) => {
                    this.succesMessage = data["succes"];
                    this.$location.path('/child');
                })
                .error((data) => {
                    this.errorMessage = data;
                });
        }
    }

    angular.module('aoControllers')
        .controller('editChildController', editChildController);
}