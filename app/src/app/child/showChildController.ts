// <reference path="../../_referencepaths.ts"/>

module aoControllers
{
    'use strict';

    import Services = aoServices;

    export class showChildController {

        static $inject = [
            'ChildService',
            '$location',
            '$stateParams'
        ];

        singleChildData: any;
        errorMessage: string;

        constructor(private ChildService: Services.ChildService, private $location: ng.ILocationService, private $stateParams: angular.ui.IStateParamsService) {
            this.showChildCtrl($stateParams["id"]);
        }

        showChildCtrl(id)
        {
            this.ChildService.showChildServ(id)
                .then((data) => {
                    if(data["error"] != null)
                    {
                        this.errorMessage = data["error"];
                    }
                    else
                    {
                        this.singleChildData = data;
                    }
                })
        }
    }

    angular.module('aoControllers')
        .controller('showChildController', showChildController);
}