/**
 * Created by BenjaminLierman on 4/04/16.
 */

(() => {
    'use strict';

    const CONFIG = require('../config.json');

    let gulp   = require('gulp'),
        concat = require('gulp-concat'),
        util   = require('gulp-util');

    gulp.task('vendor', [
        'vendor:angular',
        'vendor:lodash',
        'vendor:jquery',
        'vendor:typescript'
    ]);

    gulp.task('vendor:angular', () => {
        return gulp // JavaScript
            .src([
                `${CONFIG.dir.node}angular/angular.js`,
                `${CONFIG.dir.node}angular-route/angular-route.js`,
                `${CONFIG.dir.node}angular-resource/angular-resource.js`,
                `${CONFIG.dir.node}angular-aria/angular-aria.js`,
                `${CONFIG.dir.node}angular-messages/angular-messages.js`,
                `${CONFIG.dir.node}angular-sanitize/angular-sanitize.js`,
                `${CONFIG.dir.node}angular-ui-router/release/angular-ui-router.js`,
                `${CONFIG.dir.node}angular-utils-pagination/dirPagination.js`,
            ])
            .pipe(concat('angular.js').on('error', util.log))
            .pipe(gulp.dest(`${CONFIG.dir.vendor}angular/`));
    });

    gulp.task('vendor:lodash', () => {
        return gulp // JavaScript
            .src(`${CONFIG.dir.node}lodash/lodash.min.js`)
            .pipe(gulp.dest(`${CONFIG.dir.vendor}lodash/`).on('error', util.log));
    });

    gulp.task('vendor:jquery', () => {
        gulp // JavaScript
            .src(`${CONFIG.dir.node}jquery/dist/jquery.min.js`)
            .pipe(gulp.dest(`${CONFIG.dir.vendor}jquery/js/`).on('error', util.log));
        gulp // CSS
            .src(`${CONFIG.dir.node}jquery/dist/jquery.min.css`)
            .pipe(gulp.dest(`${CONFIG.dir.vendor}jquery/css/`).on('error', util.log));

        return gulp;
    });

    gulp.task('vendor:typescript', () => {
        gulp
            .src(`${CONFIG.dir.node}definitely-typed-angular/angular.d.ts`)
            .pipe(gulp.dest(`${CONFIG.dir.src}typescript/definitely-typed-angular/`).on('error', util.log));
        gulp
            .src(`${CONFIG.dir.node}definitely-typed-angular/angular-route.d.ts`)
            .pipe(gulp.dest(`${CONFIG.dir.src}typescript/definitely-typed-angular/`).on('error', util.log));
        gulp
            .src(`${CONFIG.dir.node}definitely-typed-angular-ui-router/angular-ui-router.d.ts`)
            .pipe(gulp.dest(`${CONFIG.dir.src}typescript/definitely-typed-angular/`).on('error', util.log));
        gulp
            .src(`${CONFIG.dir.node}definitely-typed-jquery/jquery.d.ts`)
            .pipe(gulp.dest(`${CONFIG.dir.src}typescript/definitely-typed-jquery/`).on('error', util.log));
    });

})();