/**
 * Created by BenjaminLierman on 4/04/16.
 */

(() => {
    'use strict';

    const CONFIG = require('../config.json');

    let gulp = require('gulp'),
        sass = require('gulp-sass');

    gulp.task('styles', [
        'styles:app'
    ]);

    gulp.task('styles:app', () => {
        gulp.src(`${CONFIG.dir.src}css/**/*.scss`)
            .pipe(sass(CONFIG.sass).on('error', sass.logError))
            .pipe(gulp.dest(`${CONFIG.dir.dest}styles/css/`));
    });

})();