/**
 * Created by BenjaminLierman on 4/04/16.
 */

(() => {
    'use strict';

    const CONFIG = require('../config.json');

    let elixir   = require('laravel-elixir'),
        elixirTypscript =  require('elixir-typescript'),
        gulp = require('gulp'),
        util   = require('gulp-util');


    elixir(function(mix) {
            mix.typescript(
                `${CONFIG.dir.src}**/*.ts`,
                `${CONFIG.dir.dest}scripts/js/app.js`
            );
        });

})();