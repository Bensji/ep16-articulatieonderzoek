/**
 * Created by BenjaminLierman on 4/04/16.
 */

(() => {
    'use strict';

    const CONFIG = require('../config.json');

    let gulp  = require('gulp');

    gulp.task('watch', [
        'watch:templates',
        'watch:styles'
    ]);

    gulp.task('watch:templates', () => {
        gulp.watch(CONFIG.dir.templates.src, ['templates:copy']);
    });

    gulp.task('watch:styles', () => {
       gulp.watch(`${CONFIG.dir.src}css/**/*.scss`, ['styles:app'])
    });

})();