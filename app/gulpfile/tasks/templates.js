/**
 * Created by BenjaminLierman on 4/04/16.
 */

(() => {
    'use strict';

    const CONFIG = require('../config.json');

    let gulp = require('gulp');

    gulp.task('templates', [
        'templates:copy'
    ]);

    gulp.task('templates:copy', () => {
        gulp.src(CONFIG.dir.templates.src)
            .pipe(gulp.dest(CONFIG.dir.templates.dest));
    });

})();