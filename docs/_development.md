#Develoment Articulatieonderzoek

**You will need**

- Server (pref. Vagrant Box (Artestead, Laravel Homestead, Vagrant, ...))
- NPM
- Composer
- Bower
- Git

**Walkthrough**

1. Clone the project from: 
https://bitbucket.org/Bensji/ep16-articulatieonderzoek

**I have made the repo public. Also, you will find the old version of the application in the _docs**

2. Composer Install
`cd ep16.articulatieonderzoek.local/www && composer install`
3. Edit DB-credentials (prob. not necessary)`
4. Migrations: `php artisan migrate`
5. Seeding of the DB: `php artisan db:seed`
6. `cd ep16.articulatieonderzoek.local/app && npm install`
7. `gulp && gulp watch

Before you can use the Recorder. You will prob. have to enable a secure enviroment with SSL.
This because google wants is. All hail The Google Botnet!

How you can do so you can find here:
https://www.digitalocean.com/community/tutorials/how-to-create-a-ssl-certificate-on-nginx-for-ubuntu-12-04

8. Go tho your application

Credentials are (for Admin user):
admin@ao-r.be
yamu-Paqe6re

The other credentials can be found in the seeders.