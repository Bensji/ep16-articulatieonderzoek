#Veranderingen

**2015/2016**
Benjamin Lierman <benjaminlierman@hotmail.com>

- Omzetten naar Laravel 5.2 LTS
- Introduceren Typescript
- Introduceren SASS
- Introduceren Gulp
- Introduceren ide_helper
- Visual Update
- Updating all dependencies
- Updating recording Audio

Note:
The Application now talks to the server through an API.
To run the application, it's best you're on HTTPS.

**2016/2017**