#API

The application works with an API.
You can check the API out in `ep16.articulatieonderzoek.local/www/app/Http/`

Be sure to check all the routes `php artisan route:list` to see what's possible.